# Summary

Date : 2020-02-04 18:17:52

Directory c:\Users\asus\Documents\FlutterProjects\kutupsozluk_app

Total : 161 files,  12009 codes, 171 comments, 1047 blanks, all 13227 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Dart | 142 | 11,600 | 148 | 993 | 12,741 |
| JSON | 2 | 145 | 0 | 2 | 147 |
| XML | 7 | 104 | 19 | 8 | 131 |
| Groovy | 3 | 92 | 3 | 21 | 116 |
| YAML | 1 | 30 | 0 | 11 | 41 |
| Markdown | 2 | 16 | 0 | 8 | 24 |
| Swift | 1 | 12 | 0 | 2 | 14 |
| Properties | 2 | 9 | 1 | 2 | 12 |
| C++ | 1 | 1 | 0 | 0 | 1 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 161 | 12,009 | 171 | 1,047 | 13,227 |
| android | 10 | 144 | 21 | 29 | 194 |
| android\app | 6 | 96 | 20 | 18 | 134 |
| android\app\src | 5 | 43 | 17 | 6 | 66 |
| android\app\src\debug | 1 | 4 | 3 | 1 | 8 |
| android\app\src\main | 3 | 35 | 11 | 4 | 50 |
| android\app\src\main\res | 2 | 10 | 9 | 3 | 22 |
| android\app\src\main\res\drawable | 1 | 4 | 7 | 2 | 13 |
| android\app\src\main\res\values | 1 | 6 | 2 | 1 | 9 |
| android\app\src\profile | 1 | 4 | 3 | 1 | 8 |
| android\gradle | 1 | 5 | 1 | 1 | 7 |
| android\gradle\wrapper | 1 | 5 | 1 | 1 | 7 |
| ios | 7 | 222 | 2 | 8 | 232 |
| ios\Runner | 7 | 222 | 2 | 8 | 232 |
| ios\Runner\Assets.xcassets | 3 | 148 | 0 | 4 | 152 |
| ios\Runner\Assets.xcassets\AppIcon.appiconset | 1 | 122 | 0 | 1 | 123 |
| ios\Runner\Assets.xcassets\LaunchImage.imageset | 2 | 26 | 0 | 3 | 29 |
| ios\Runner\Base.lproj | 2 | 61 | 2 | 2 | 65 |
| lib | 141 | 11,586 | 138 | 986 | 12,710 |
| lib\authentication_bloc | 4 | 106 | 0 | 23 | 129 |
| lib\data | 6 | 272 | 0 | 28 | 300 |
| lib\data\model | 6 | 272 | 0 | 28 | 300 |
| lib\home | 51 | 4,015 | 11 | 341 | 4,367 |
| lib\home\homescreen | 47 | 3,415 | 11 | 301 | 3,727 |
| lib\home\homescreen\pages | 38 | 2,973 | 11 | 245 | 3,229 |
| lib\home\homescreen\pages\events_and_states | 2 | 58 | 0 | 27 | 85 |
| lib\home\homescreen\pages\last_month | 4 | 204 | 1 | 18 | 223 |
| lib\home\homescreen\pages\main | 6 | 278 | 7 | 32 | 317 |
| lib\home\homescreen\pages\main\bloc | 2 | 42 | 0 | 8 | 50 |
| lib\home\homescreen\pages\onceuptime | 4 | 327 | 0 | 29 | 356 |
| lib\home\homescreen\pages\random | 2 | 55 | 0 | 9 | 64 |
| lib\home\homescreen\pages\title_view | 10 | 1,539 | 2 | 84 | 1,625 |
| lib\home\homescreen\pages\title_view\bloc | 4 | 145 | 0 | 34 | 179 |
| lib\home\homescreen\pages\title_view\title_entry_card | 3 | 981 | 2 | 28 | 1,011 |
| lib\home\homescreen\pages\trends | 4 | 204 | 1 | 16 | 221 |
| lib\home\homescreen\pages\yesterday | 4 | 206 | 0 | 19 | 225 |
| lib\home\homescreen\right_frame | 6 | 204 | 0 | 36 | 240 |
| lib\home\homescreen\right_frame\question_page | 3 | 44 | 0 | 18 | 62 |
| lib\home\title_entry | 3 | 512 | 0 | 26 | 538 |
| lib\home\ui | 1 | 88 | 0 | 14 | 102 |
| lib\login | 9 | 531 | 0 | 66 | 597 |
| lib\login\bloc | 4 | 198 | 0 | 36 | 234 |
| lib\notification | 3 | 318 | 2 | 18 | 338 |
| lib\parse_entry | 3 | 232 | 77 | 41 | 350 |
| lib\profile | 32 | 3,525 | 11 | 250 | 3,786 |
| lib\profile\entry_card | 3 | 947 | 1 | 29 | 977 |
| lib\profile\settings | 14 | 1,524 | 5 | 93 | 1,622 |
| lib\profile\settings\blockeds | 3 | 218 | 2 | 28 | 248 |
| lib\profile\settings\followers | 6 | 285 | 3 | 31 | 319 |
| lib\profile\settings\user_choices | 1 | 143 | 0 | 7 | 150 |
| lib\profile\settings\user_info | 3 | 508 | 0 | 16 | 524 |
| lib\profile\tabs | 10 | 433 | 2 | 71 | 506 |
| lib\profile\tabs\commented_entries | 3 | 126 | 0 | 21 | 147 |
| lib\profile\tabs\favourites | 3 | 126 | 1 | 20 | 147 |
| lib\profile\tabs\last_entries | 3 | 133 | 1 | 20 | 154 |
| lib\profile\tabs\pprofile | 1 | 48 | 0 | 10 | 58 |
| lib\register | 8 | 679 | 0 | 60 | 739 |
| lib\register\bloc | 4 | 201 | 0 | 33 | 234 |
| lib\search | 14 | 1,041 | 2 | 75 | 1,118 |
| lib\search\category | 4 | 259 | 1 | 21 | 281 |
| lib\search\new_title | 4 | 259 | 0 | 20 | 279 |
| lib\search\search_page | 6 | 523 | 1 | 34 | 558 |
| lib\style | 1 | 20 | 4 | 7 | 31 |
| lib\teddy | 3 | 192 | 27 | 34 | 253 |
| lib\theme | 1 | 62 | 0 | 14 | 76 |
| test | 1 | 14 | 10 | 7 | 31 |

[details](details.md)