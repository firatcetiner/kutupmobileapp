# Details

Date : 2020-02-04 18:17:52

Directory c:\Users\asus\Documents\FlutterProjects\kutupsozluk_app

Total : 161 files,  12009 codes, 171 comments, 1047 blanks, all 13227 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [README.md](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/README.md) | Markdown | 13 | 0 | 6 | 19 |
| [android\app\build.gradle](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/android/app/build.gradle) | Groovy | 53 | 3 | 12 | 68 |
| [android\app\src\debug\AndroidManifest.xml](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/android/app/src/debug/AndroidManifest.xml) | XML | 4 | 3 | 1 | 8 |
| [android\app\src\main\AndroidManifest.xml](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/android/app/src/main/AndroidManifest.xml) | XML | 25 | 2 | 1 | 28 |
| [android\app\src\main\res\drawable\launch_background.xml](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/android/app/src/main/res/drawable/launch_background.xml) | XML | 4 | 7 | 2 | 13 |
| [android\app\src\main\res\values\styles.xml](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/android/app/src/main/res/values/styles.xml) | XML | 6 | 2 | 1 | 9 |
| [android\app\src\profile\AndroidManifest.xml](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/android/app/src/profile/AndroidManifest.xml) | XML | 4 | 3 | 1 | 8 |
| [android\build.gradle](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/android/build.gradle) | Groovy | 27 | 0 | 5 | 32 |
| [android\gradle.properties](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/android/gradle.properties) | Properties | 4 | 0 | 1 | 5 |
| [android\gradle\wrapper\gradle-wrapper.properties](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/android/gradle/wrapper/gradle-wrapper.properties) | Properties | 5 | 1 | 1 | 7 |
| [android\settings.gradle](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/android/settings.gradle) | Groovy | 12 | 0 | 4 | 16 |
| [ios\Runner\AppDelegate.swift](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/ios/Runner/AppDelegate.swift) | Swift | 12 | 0 | 2 | 14 |
| [ios\Runner\Assets.xcassets\AppIcon.appiconset\Contents.json](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/ios/Runner/Assets.xcassets/AppIcon.appiconset/Contents.json) | JSON | 122 | 0 | 1 | 123 |
| [ios\Runner\Assets.xcassets\LaunchImage.imageset\Contents.json](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/ios/Runner/Assets.xcassets/LaunchImage.imageset/Contents.json) | JSON | 23 | 0 | 1 | 24 |
| [ios\Runner\Assets.xcassets\LaunchImage.imageset\README.md](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/ios/Runner/Assets.xcassets/LaunchImage.imageset/README.md) | Markdown | 3 | 0 | 2 | 5 |
| [ios\Runner\Base.lproj\LaunchScreen.storyboard](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/ios/Runner/Base.lproj/LaunchScreen.storyboard) | XML | 36 | 1 | 1 | 38 |
| [ios\Runner\Base.lproj\Main.storyboard](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/ios/Runner/Base.lproj/Main.storyboard) | XML | 25 | 1 | 1 | 27 |
| [ios\Runner\Runner-Bridging-Header.h](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/ios/Runner/Runner-Bridging-Header.h) | C++ | 1 | 0 | 0 | 1 |
| [lib\authentication_bloc\authentication_bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/authentication_bloc/authentication_bloc.dart) | Dart | 61 | 0 | 9 | 70 |
| [lib\authentication_bloc\authentication_event.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/authentication_bloc/authentication_event.dart) | Dart | 22 | 0 | 6 | 28 |
| [lib\authentication_bloc\authentication_state.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/authentication_bloc/authentication_state.dart) | Dart | 20 | 0 | 7 | 27 |
| [lib\authentication_bloc\bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/authentication_bloc/bloc.dart) | Dart | 3 | 0 | 1 | 4 |
| [lib\custom_themes.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/custom_themes.dart) | Dart | 251 | 3 | 7 | 261 |
| [lib\custom_transitions.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/custom_transitions.dart) | Dart | 211 | 0 | 5 | 216 |
| [lib\data\model\announcement_model.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/data/model/announcement_model.dart) | Dart | 26 | 0 | 5 | 31 |
| [lib\data\model\comment_model.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/data/model/comment_model.dart) | Dart | 37 | 0 | 4 | 41 |
| [lib\data\model\entry_model.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/data/model/entry_model.dart) | Dart | 78 | 0 | 5 | 83 |
| [lib\data\model\notification_model.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/data/model/notification_model.dart) | Dart | 60 | 0 | 5 | 65 |
| [lib\data\model\question_model.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/data/model/question_model.dart) | Dart | 38 | 0 | 5 | 43 |
| [lib\data\model\title_model.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/data/model/title_model.dart) | Dart | 33 | 0 | 4 | 37 |
| [lib\home\homescreen\home_screen.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/home_screen.dart) | Dart | 217 | 0 | 11 | 228 |
| [lib\home\homescreen\homescreen_bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/homescreen_bloc.dart) | Dart | 15 | 0 | 8 | 23 |
| [lib\home\homescreen\homescreen_provider.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/homescreen_provider.dart) | Dart | 6 | 0 | 1 | 7 |
| [lib\home\homescreen\pages\events_and_states\event.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/events_and_states/event.dart) | Dart | 25 | 0 | 12 | 37 |
| [lib\home\homescreen\pages\events_and_states\state.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/events_and_states/state.dart) | Dart | 33 | 0 | 15 | 48 |
| [lib\home\homescreen\pages\home_page.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/home_page.dart) | Dart | 91 | 0 | 6 | 97 |
| [lib\home\homescreen\pages\home_page_bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/home_page_bloc.dart) | Dart | 11 | 0 | 5 | 16 |
| [lib\home\homescreen\pages\last_month\bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/last_month/bloc.dart) | Dart | 40 | 0 | 8 | 48 |
| [lib\home\homescreen\pages\last_month\last_month_page.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/last_month/last_month_page.dart) | Dart | 121 | 1 | 4 | 126 |
| [lib\home\homescreen\pages\last_month\last_month_screen.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/last_month/last_month_screen.dart) | Dart | 23 | 0 | 5 | 28 |
| [lib\home\homescreen\pages\last_month\repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/last_month/repository.dart) | Dart | 20 | 0 | 1 | 21 |
| [lib\home\homescreen\pages\main\bloc\bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/main/bloc/bloc.dart) | Dart | 1 | 0 | 0 | 1 |
| [lib\home\homescreen\pages\main\bloc\mainview_bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/main/bloc/mainview_bloc.dart) | Dart | 41 | 0 | 8 | 49 |
| [lib\home\homescreen\pages\main\lazy_load_scroll_view.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/main/lazy_load_scroll_view.dart) | Dart | 73 | 7 | 16 | 96 |
| [lib\home\homescreen\pages\main\main_view_page.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/main/main_view_page.dart) | Dart | 119 | 0 | 2 | 121 |
| [lib\home\homescreen\pages\main\main_view_repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/main/main_view_repository.dart) | Dart | 20 | 0 | 1 | 21 |
| [lib\home\homescreen\pages\main\main_view_screen.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/main/main_view_screen.dart) | Dart | 24 | 0 | 5 | 29 |
| [lib\home\homescreen\pages\onceuptime\bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/onceuptime/bloc.dart) | Dart | 58 | 0 | 14 | 72 |
| [lib\home\homescreen\pages\onceuptime\once_upon_a_time.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/onceuptime/once_upon_a_time.dart) | Dart | 224 | 0 | 6 | 230 |
| [lib\home\homescreen\pages\onceuptime\once_upon_a_time_screen.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/onceuptime/once_upon_a_time_screen.dart) | Dart | 23 | 0 | 7 | 30 |
| [lib\home\homescreen\pages\onceuptime\repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/onceuptime/repository.dart) | Dart | 22 | 0 | 2 | 24 |
| [lib\home\homescreen\pages\random\bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/random/bloc.dart) | Dart | 35 | 0 | 8 | 43 |
| [lib\home\homescreen\pages\random\repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/random/repository.dart) | Dart | 20 | 0 | 1 | 21 |
| [lib\home\homescreen\pages\title_view\bloc\bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/title_view/bloc/bloc.dart) | Dart | 3 | 0 | 0 | 3 |
| [lib\home\homescreen\pages\title_view\bloc\event.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/title_view/bloc/event.dart) | Dart | 7 | 0 | 7 | 14 |
| [lib\home\homescreen\pages\title_view\bloc\state.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/title_view/bloc/state.dart) | Dart | 29 | 0 | 13 | 42 |
| [lib\home\homescreen\pages\title_view\bloc\title_view_bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/title_view/bloc/title_view_bloc.dart) | Dart | 106 | 0 | 14 | 120 |
| [lib\home\homescreen\pages\title_view\entry_list_page.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/title_view/entry_list_page.dart) | Dart | 307 | 0 | 8 | 315 |
| [lib\home\homescreen\pages\title_view\entry_list_screen.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/title_view/entry_list_screen.dart) | Dart | 23 | 0 | 8 | 31 |
| [lib\home\homescreen\pages\title_view\title_entry_card\bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/title_view/title_entry_card/bloc.dart) | Dart | 329 | 2 | 14 | 345 |
| [lib\home\homescreen\pages\title_view\title_entry_card\repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/title_view/title_entry_card/repository.dart) | Dart | 92 | 0 | 8 | 100 |
| [lib\home\homescreen\pages\title_view\title_entry_card\title_entry_card.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/title_view/title_entry_card/title_entry_card.dart) | Dart | 560 | 0 | 6 | 566 |
| [lib\home\homescreen\pages\title_view\title_view_repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/title_view/title_view_repository.dart) | Dart | 83 | 0 | 6 | 89 |
| [lib\home\homescreen\pages\trends\bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/trends/bloc.dart) | Dart | 40 | 0 | 7 | 47 |
| [lib\home\homescreen\pages\trends\repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/trends/repository.dart) | Dart | 20 | 0 | 1 | 21 |
| [lib\home\homescreen\pages\trends\trends_page.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/trends/trends_page.dart) | Dart | 121 | 1 | 3 | 125 |
| [lib\home\homescreen\pages\trends\trends_screen.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/trends/trends_screen.dart) | Dart | 23 | 0 | 5 | 28 |
| [lib\home\homescreen\pages\yesterday\bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/yesterday/bloc.dart) | Dart | 44 | 0 | 9 | 53 |
| [lib\home\homescreen\pages\yesterday\repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/yesterday/repository.dart) | Dart | 20 | 0 | 1 | 21 |
| [lib\home\homescreen\pages\yesterday\yesterday_page.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/yesterday/yesterday_page.dart) | Dart | 119 | 0 | 4 | 123 |
| [lib\home\homescreen\pages\yesterday\yesterday_screen.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/pages/yesterday/yesterday_screen.dart) | Dart | 23 | 0 | 5 | 28 |
| [lib\home\homescreen\right_frame\question_page\question_page.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/right_frame/question_page/question_page.dart) | Dart | 33 | 0 | 9 | 42 |
| [lib\home\homescreen\right_frame\question_page\question_page_bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/right_frame/question_page/question_page_bloc.dart) | Dart | 11 | 0 | 8 | 19 |
| [lib\home\homescreen\right_frame\question_page\question_page_repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/right_frame/question_page/question_page_repository.dart) | Dart | 0 | 0 | 1 | 1 |
| [lib\home\homescreen\right_frame\right_frame_bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/right_frame/right_frame_bloc.dart) | Dart | 29 | 0 | 7 | 36 |
| [lib\home\homescreen\right_frame\right_frame_page.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/right_frame/right_frame_page.dart) | Dart | 107 | 0 | 8 | 115 |
| [lib\home\homescreen\right_frame\right_frame_repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/homescreen/right_frame/right_frame_repository.dart) | Dart | 24 | 0 | 3 | 27 |
| [lib\home\title_entry\new_entry_bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/title_entry/new_entry_bloc.dart) | Dart | 31 | 0 | 8 | 39 |
| [lib\home\title_entry\new_entry_page.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/title_entry/new_entry_page.dart) | Dart | 444 | 0 | 13 | 457 |
| [lib\home\title_entry\new_entry_repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/title_entry/new_entry_repository.dart) | Dart | 37 | 0 | 5 | 42 |
| [lib\home\ui\bubble_indication_painter.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/home/ui/bubble_indication_painter.dart) | Dart | 88 | 0 | 14 | 102 |
| [lib\login\bloc\bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/login/bloc/bloc.dart) | Dart | 3 | 0 | 1 | 4 |
| [lib\login\bloc\login_bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/login/bloc/login_bloc.dart) | Dart | 66 | 0 | 10 | 76 |
| [lib\login\bloc\login_event.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/login/bloc/login_event.dart) | Dart | 38 | 0 | 14 | 52 |
| [lib\login\bloc\login_state.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/login/bloc/login_state.dart) | Dart | 91 | 0 | 11 | 102 |
| [lib\login\create_account_button.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/login/create_account_button.dart) | Dart | 36 | 0 | 5 | 41 |
| [lib\login\login.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/login/login.dart) | Dart | 5 | 0 | 1 | 6 |
| [lib\login\login_button.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/login/login_button.dart) | Dart | 30 | 0 | 4 | 34 |
| [lib\login\login_form.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/login/login_form.dart) | Dart | 237 | 0 | 16 | 253 |
| [lib\login\login_screen.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/login/login_screen.dart) | Dart | 25 | 0 | 4 | 29 |
| [lib\main.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/main.dart) | Dart | 77 | 1 | 7 | 85 |
| [lib\notification\notification_bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/notification/notification_bloc.dart) | Dart | 42 | 2 | 9 | 53 |
| [lib\notification\notification_page.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/notification/notification_page.dart) | Dart | 251 | 0 | 7 | 258 |
| [lib\notification\notification_repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/notification/notification_repository.dart) | Dart | 25 | 0 | 2 | 27 |
| [lib\parse_entry\matched_text.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/parse_entry/matched_text.dart) | Dart | 19 | 17 | 9 | 45 |
| [lib\parse_entry\parse_text.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/parse_entry/parse_text.dart) | Dart | 201 | 48 | 30 | 279 |
| [lib\parse_entry\regex_options.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/parse_entry/regex_options.dart) | Dart | 12 | 12 | 2 | 26 |
| [lib\profile\entry_card\bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/entry_card/bloc.dart) | Dart | 321 | 0 | 11 | 332 |
| [lib\profile\entry_card\profile_entry_card.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/entry_card/profile_entry_card.dart) | Dart | 534 | 1 | 10 | 545 |
| [lib\profile\entry_card\repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/entry_card/repository.dart) | Dart | 92 | 0 | 8 | 100 |
| [lib\profile\indication_painter.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/indication_painter.dart) | Dart | 40 | 0 | 11 | 51 |
| [lib\profile\other_profile_page.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/other_profile_page.dart) | Dart | 187 | 0 | 8 | 195 |
| [lib\profile\profile_page.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/profile_page.dart) | Dart | 164 | 1 | 8 | 173 |
| [lib\profile\profile_screen_bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/profile_screen_bloc.dart) | Dart | 31 | 0 | 9 | 40 |
| [lib\profile\settings\blockeds\bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/settings/blockeds/bloc.dart) | Dart | 35 | 0 | 12 | 47 |
| [lib\profile\settings\blockeds\blockeds_page.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/settings/blockeds/blockeds_page.dart) | Dart | 144 | 1 | 12 | 157 |
| [lib\profile\settings\blockeds\repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/settings/blockeds/repository.dart) | Dart | 39 | 1 | 4 | 44 |
| [lib\profile\settings\followers\bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/settings/followers/bloc.dart) | Dart | 51 | 0 | 11 | 62 |
| [lib\profile\settings\followers\follower_model.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/settings/followers/follower_model.dart) | Dart | 20 | 0 | 2 | 22 |
| [lib\profile\settings\followers\followers_page.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/settings/followers/followers_page.dart) | Dart | 141 | 1 | 9 | 151 |
| [lib\profile\settings\followers\repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/settings/followers/repository.dart) | Dart | 28 | 2 | 5 | 35 |
| [lib\profile\settings\followers\small_follower_model.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/settings/followers/small_follower_model.dart) | Dart | 9 | 0 | 2 | 11 |
| [lib\profile\settings\followers\user_info_model.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/settings/followers/user_info_model.dart) | Dart | 36 | 0 | 2 | 38 |
| [lib\profile\settings\profile_settings.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/settings/profile_settings.dart) | Dart | 370 | 0 | 11 | 381 |
| [lib\profile\settings\user_choices\general_choices.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/settings/user_choices/general_choices.dart) | Dart | 143 | 0 | 7 | 150 |
| [lib\profile\settings\user_info\general_info.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/settings/user_info/general_info.dart) | Dart | 434 | 0 | 8 | 442 |
| [lib\profile\settings\user_info\user_info_bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/settings/user_info/user_info_bloc.dart) | Dart | 9 | 0 | 3 | 12 |
| [lib\profile\settings\user_info\user_info_repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/settings/user_info/user_info_repository.dart) | Dart | 65 | 0 | 5 | 70 |
| [lib\profile\tabs\commented_entries\bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/tabs/commented_entries/bloc.dart) | Dart | 37 | 0 | 8 | 45 |
| [lib\profile\tabs\commented_entries\commented_entries_tab.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/tabs/commented_entries/commented_entries_tab.dart) | Dart | 77 | 0 | 11 | 88 |
| [lib\profile\tabs\commented_entries\repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/tabs/commented_entries/repository.dart) | Dart | 12 | 0 | 2 | 14 |
| [lib\profile\tabs\favourites\bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/tabs/favourites/bloc.dart) | Dart | 37 | 0 | 8 | 45 |
| [lib\profile\tabs\favourites\favorite_entries_tab.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/tabs/favourites/favorite_entries_tab.dart) | Dart | 77 | 0 | 10 | 87 |
| [lib\profile\tabs\favourites\repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/tabs/favourites/repository.dart) | Dart | 12 | 1 | 2 | 15 |
| [lib\profile\tabs\last_entries\bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/tabs/last_entries/bloc.dart) | Dart | 37 | 0 | 8 | 45 |
| [lib\profile\tabs\last_entries\last_entries_tab.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/tabs/last_entries/last_entries_tab.dart) | Dart | 76 | 0 | 11 | 87 |
| [lib\profile\tabs\last_entries\repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/tabs/last_entries/repository.dart) | Dart | 20 | 1 | 1 | 22 |
| [lib\profile\tabs\pprofile\profile_tab.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/tabs/pprofile/profile_tab.dart) | Dart | 48 | 0 | 10 | 58 |
| [lib\profile\user_repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/profile/user_repository.dart) | Dart | 199 | 2 | 21 | 222 |
| [lib\register\bloc\bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/register/bloc/bloc.dart) | Dart | 3 | 0 | 1 | 4 |
| [lib\register\bloc\register_bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/register/bloc/register_bloc.dart) | Dart | 74 | 0 | 10 | 84 |
| [lib\register\bloc\register_event.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/register/bloc/register_event.dart) | Dart | 33 | 0 | 11 | 44 |
| [lib\register\bloc\register_state.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/register/bloc/register_state.dart) | Dart | 91 | 0 | 11 | 102 |
| [lib\register\register.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/register/register.dart) | Dart | 4 | 0 | 1 | 5 |
| [lib\register\register_button.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/register/register_button.dart) | Dart | 27 | 0 | 4 | 31 |
| [lib\register\register_form.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/register/register_form.dart) | Dart | 390 | 0 | 18 | 408 |
| [lib\register\register_screen.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/register/register_screen.dart) | Dart | 57 | 0 | 4 | 61 |
| [lib\search\category\bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/search/category/bloc.dart) | Dart | 44 | 0 | 8 | 52 |
| [lib\search\category\category_page.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/search/category/category_page.dart) | Dart | 172 | 1 | 6 | 179 |
| [lib\search\category\category_screen.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/search/category/category_screen.dart) | Dart | 30 | 0 | 6 | 36 |
| [lib\search\category\repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/search/category/repository.dart) | Dart | 13 | 0 | 1 | 14 |
| [lib\search\new_title\new_title_bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/search/new_title/new_title_bloc.dart) | Dart | 7 | 0 | 2 | 9 |
| [lib\search\new_title\new_title_page.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/search/new_title/new_title_page.dart) | Dart | 111 | 0 | 6 | 117 |
| [lib\search\new_title\new_title_repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/search/new_title/new_title_repository.dart) | Dart | 28 | 0 | 3 | 31 |
| [lib\search\new_title\title_detail_page.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/search/new_title/title_detail_page.dart) | Dart | 113 | 0 | 9 | 122 |
| [lib\search\search_page\bloc.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/search/search_page/bloc.dart) | Dart | 27 | 0 | 6 | 33 |
| [lib\search\search_page\repository.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/search/search_page/repository.dart) | Dart | 13 | 0 | 3 | 16 |
| [lib\search\search_page\search_event.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/search/search_page/search_event.dart) | Dart | 14 | 0 | 6 | 20 |
| [lib\search\search_page\search_page.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/search/search_page/search_page.dart) | Dart | 431 | 1 | 5 | 437 |
| [lib\search\search_page\search_screen.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/search/search_page/search_screen.dart) | Dart | 20 | 0 | 6 | 26 |
| [lib\search\search_page\search_state.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/search/search_page/search_state.dart) | Dart | 18 | 0 | 8 | 26 |
| [lib\simple_bloc_delegate.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/simple_bloc_delegate.dart) | Dart | 18 | 0 | 4 | 22 |
| [lib\splash_screen.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/splash_screen.dart) | Dart | 16 | 0 | 2 | 18 |
| [lib\style\theme.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/style/theme.dart) | Dart | 20 | 4 | 7 | 31 |
| [lib\teddy\input_helper.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/teddy/input_helper.dart) | Dart | 33 | 3 | 5 | 41 |
| [lib\teddy\teddy_controller.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/teddy/teddy_controller.dart) | Dart | 94 | 20 | 23 | 137 |
| [lib\teddy\tracking_text_input.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/teddy/tracking_text_input.dart) | Dart | 65 | 4 | 6 | 75 |
| [lib\theme\custom_theme.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/theme/custom_theme.dart) | Dart | 62 | 0 | 14 | 76 |
| [lib\validators.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/lib/validators.dart) | Dart | 20 | 0 | 4 | 24 |
| [pubspec.yaml](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/pubspec.yaml) | YAML | 30 | 0 | 11 | 41 |
| [test\widget_test.dart](file:///c%3A/Users/asus/Documents/FlutterProjects/kutupsozluk_app/test/widget_test.dart) | Dart | 14 | 10 | 7 | 31 |

[summary](results.md)