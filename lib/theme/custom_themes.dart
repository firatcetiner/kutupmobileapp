import 'package:flutter/material.dart';
import '../home/ui/bubble_indication_painter.dart';
import '../style/custom_colors.dart' show CustomColors;

enum ThemeType { LIGHT_BLUE, LIGHT_RED, LIGHT_ORANGE, DARK, DARKER }

class MyThemes {

  static final ThemeData lightBlueTheme = ThemeData(
    bottomAppBarColor: CustomColors.lb_appBar,
    cardColor: Colors.white,
    accentColor: CustomColors.lb_indicator,
    indicatorColor: CustomColors.lb_indicator,
    //canvasColor: colors.CustomColors.lb_indicator,
    buttonTheme: ButtonThemeData(
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      buttonColor: Colors.white,
      padding: EdgeInsets.all(2.0),
      minWidth: 30,
      height: 30,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5)
      )
    ),
    primaryColor: CustomColors.lb_indicator,
    brightness: Brightness.light,
    iconTheme: IconThemeData(
      color: Colors.black,
      size: 18,
    ),
    cardTheme: CardTheme(
      margin: EdgeInsets.zero,
      elevation: 0.0,
      color: Colors.white,
    ),
    primaryColorDark: Colors.transparent,
    dividerColor: Colors.grey[200],
    appBarTheme: AppBarTheme(
      elevation: 0,
      brightness: Brightness.light,
      color: Colors.white,
      iconTheme: IconThemeData(
        color: Colors.black,
        size: 18,
      ),
      textTheme: TextTheme(
        title: TextStyle(color:Colors.black, fontSize: 18, fontWeight: FontWeight.w500),
        overline: TextStyle(color: Colors.black, fontSize: 15),
        body1: TextStyle(color: Colors.black, fontSize: 15),
        body2: TextStyle(color: Colors.black, fontSize: 18),
        subhead: TextStyle(color: Colors.black, fontSize: 15),
      ),
    ),
    tabBarTheme: TabBarTheme(
      labelColor: Colors.white,
      unselectedLabelColor: Colors.grey,
        indicator: BubbleTabIndicator(
          indicatorColor: CustomColors.lb_indicator,
          insets: EdgeInsets.only(left: 5.0, right: 5.0, bottom: 10, top: 10),
          indicatorHeight: 48.0,
          tabBarIndicatorSize: TabBarIndicatorSize.tab,
        ),
      labelStyle: TextStyle(
        fontWeight: FontWeight.bold
      ),
      unselectedLabelStyle: TextStyle(
        color: Colors.black87,
        fontWeight: FontWeight.normal
      )
    ),
    bottomAppBarTheme: BottomAppBarTheme(
      color: Colors.white,
      elevation: 0.0,
    ),
    textTheme: TextTheme(
      title: TextStyle(color: Colors.black),
      overline: TextStyle(color: Colors.black),
      body1: TextStyle(color: Colors.black),
      body2: TextStyle(color: Colors.black),
      subhead: TextStyle(color: Colors.black),
      button: TextStyle(color: Colors.black, fontWeight: FontWeight.normal)
    ),
    splashColor: Colors.transparent,
    highlightColor: Colors.transparent,
    scaffoldBackgroundColor: CustomColors.lb_backGround
  );

  static final ThemeData darkBlueTheme = ThemeData(
    bottomAppBarColor: Colors.grey[900],
    cardColor: Colors.grey[900],
    indicatorColor: CustomColors.lb_indicator,
    //canvasColor: colors.CustomColors.lb_indicator,
    buttonTheme: ButtonThemeData(
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      buttonColor: Colors.white,
      padding: EdgeInsets.all(2.0),
      minWidth: 30,
      height: 30,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15)
      )
    ),
    primaryColor: CustomColors.lb_indicator,
    brightness: Brightness.dark,
    iconTheme: IconThemeData(
      color: Colors.white70,
      size: 18,
    ),
    cardTheme: CardTheme(
      margin: EdgeInsets.zero,
      elevation: 0.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      color: Colors.grey[900],
    ),
    dividerTheme: DividerThemeData(
      color: Colors.grey[800],
      thickness: 1.0,
    ),
    dividerColor: Colors.grey[800],
    appBarTheme: AppBarTheme(
      elevation: 0.0,
      brightness: Brightness.dark,
      color: Colors.grey[900],
      iconTheme: IconThemeData(
        color: Colors.white,
        size: 25,
      ),
      textTheme: TextTheme(
        title: TextStyle(color:Colors.white, fontSize: 18, fontWeight: FontWeight.w500),
        overline: TextStyle(color: Colors.white, fontSize: 15),
        body1: TextStyle(color: Colors.white, fontSize: 15),
        body2: TextStyle(color: Colors.white, fontSize: 18),
        subhead: TextStyle(color: Colors.white, fontSize: 15),
      ),
    ),
    tabBarTheme: TabBarTheme(
      labelColor: Colors.white,
      unselectedLabelColor: Colors.grey,
      indicator: BubbleTabIndicator(
        indicatorColor: CustomColors.lb_indicator,
        insets: EdgeInsets.only(left: 5.0, right: 5.0, bottom: 10, top: 10),
        indicatorHeight: 48.0,
        tabBarIndicatorSize: TabBarIndicatorSize.tab,
      ),
      labelStyle: TextStyle(
        fontWeight: FontWeight.bold
      ),
      unselectedLabelStyle: TextStyle(
        color: Colors.black87,
        fontWeight: FontWeight.normal
      )
    ),
    bottomAppBarTheme: BottomAppBarTheme(
      color: Colors.black87,
      elevation: 0.0,
    ),
    textTheme: TextTheme(
      title: TextStyle(color: Colors.white),
      overline: TextStyle(color: Colors.white),
      body1: TextStyle(color: Colors.white),
      body2: TextStyle(color: Colors.white),
      subhead: TextStyle(color: Colors.white),
      button: TextStyle(color: Colors.white, fontWeight: FontWeight.normal),
    ),
    splashColor: Colors.transparent,
    highlightColor: Colors.transparent,
    primaryColorBrightness: Brightness.dark,
    scaffoldBackgroundColor: Colors.black
  );

  static final ThemeData lightRedTheme = ThemeData(
    bottomAppBarColor: CustomColors.lr_appBar,
    cardColor: Colors.white,
    indicatorColor: CustomColors.lr_indicator,
    //canvasColor: colors.CustomColors.lr_indicator,
    buttonTheme: ButtonThemeData(
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      buttonColor: Colors.black,
      padding: EdgeInsets.all(2.0),
      minWidth: 30,
      height: 30,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15)
      )
    ),
    primaryColor: CustomColors.lr_indicator,
    brightness: Brightness.light,
    iconTheme: IconThemeData(
      color: Colors.black87,
      size: 18,
    ),
    cardTheme: CardTheme(
      elevation: 0.0,
      color: Colors.white,
    ),
    dividerColor: Colors.grey[200],
    appBarTheme: AppBarTheme(
        elevation: 0.0,
        brightness: Brightness.light,
        color: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
          size: 18,
        ),
        textTheme: TextTheme(
          title: TextStyle(color:Colors.grey[800], fontSize: 16),
          overline: TextStyle(color: Colors.black87, fontSize: 15),
          body1: TextStyle(color: Colors.black38, fontSize: 15),
          body2: TextStyle(color: Colors.black87, fontSize: 18),
          subhead: TextStyle(color: Colors.black87, fontSize: 15),
        ),
      ),
    tabBarTheme: TabBarTheme(
      labelColor: Colors.white,
      unselectedLabelColor: Colors.grey,
        indicator: BubbleTabIndicator(
          indicatorColor: CustomColors.lr_indicator,
          insets: EdgeInsets.only(left: 5.0, right: 5.0, bottom: 10, top: 10),
          indicatorHeight: 48.0,
          tabBarIndicatorSize: TabBarIndicatorSize.tab,
        ),
      labelStyle: TextStyle(
        fontWeight: FontWeight.bold
      ),
      unselectedLabelStyle: TextStyle(
        color: Colors.black87,
        fontWeight: FontWeight.normal
      )
    ),
    bottomAppBarTheme: BottomAppBarTheme(
      color: Colors.white,
      elevation: 0.0,
    ),
    textTheme: TextTheme(
      title: TextStyle(color: Colors.black),
      overline: TextStyle(color: Colors.black),
      body1: TextStyle(color: Colors.black87),
      body2: TextStyle(color: Colors.black),
      subhead: TextStyle(color: Colors.black),
      button: TextStyle(color: Colors.black87, fontWeight: FontWeight.normal)
    ),
    splashColor: Colors.transparent,
    highlightColor: Colors.transparent,
    scaffoldBackgroundColor: CustomColors.lr_backGround
  );


  static ThemeData getThemeFromKey(ThemeType themeKey) {
    switch (themeKey) {
      case ThemeType.LIGHT_BLUE:
        return lightBlueTheme;
      case ThemeType.LIGHT_RED:
        return lightRedTheme;
      case ThemeType.DARK:
        return darkBlueTheme;
      default:
        return lightBlueTheme;
    }
  }
}