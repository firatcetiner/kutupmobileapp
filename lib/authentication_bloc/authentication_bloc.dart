import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:kutupsozluk_app/main.dart';
import 'package:meta/meta.dart';
import 'package:kutupsozluk_app/authentication_bloc/bloc.dart';
import 'package:kutupsozluk_app/profile/user_repository.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserRepository _userRepository;

  AuthenticationBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  AuthenticationState get initialState => Uninitialized();

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AppStarted) {
      yield* _mapAppStartedToState();
    } else if (event is LoggedIn) {
      yield* _mapLoggedInToState();
    } else if (event is LoggedOut) {
      yield* _mapLoggedOutToState();
    } else if(event is TokenRenewed) {
      yield* _mapTokenRenewedToState();
    }
  }

  Stream<AuthenticationState> _mapAppStartedToState() async* {
    try {
      final isSignedIn = await _userRepository.isSignedIn();
      if (isSignedIn) {
        final name = prefs.getString('nick');
        yield Authenticated(name);
      } else {
        yield Unauthenticated();
      }
    } catch (_) {
      yield Unauthenticated();
    }
  }

  Stream<AuthenticationState> _mapTokenRenewedToState() async* {
    try {
      var nick = prefs.getString('nick');
      var password = prefs.getString('password');
      final tokenRenewed = await _userRepository.signIn(nick, password);
      if(tokenRenewed) yield Authenticated(nick);
      else yield Unauthenticated();
    } catch(_) {
      yield Unauthenticated();
    }
  }

  Stream<AuthenticationState> _mapLoggedInToState() async* {
    final token = prefs.getString('token');
    if(token != null) yield Authenticated(token);
  }

  Stream<AuthenticationState> _mapLoggedOutToState() async* {
    yield Unauthenticated();
    _userRepository.signOut();
  }
}
