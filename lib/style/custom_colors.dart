import 'dart:ui';
import 'package:flutter/material.dart';

class CustomColors {
  const CustomColors();

  static const Color loginGradientStart = const Color(0xFF56A8E2);
  static const Color primaryColorWithOpacity = const Color(0xFFeef6fc);
  static const Color darkenBackgroundColor = const Color(0xFFe1eff9);


  /* colors for light-blue theme */
  static const Color lb_backGround = const Color(0xFFe1eff9);
  static const Color lb_appBar = Colors.white;
  static const Color lb_indicator = const Color(0xFF56A8E2);

  /* colors for light-green theme */
  static const Color lg_backGround = const Color(0xffe0f3e6);
  static const Color lg_appBar = Colors.white;
  static const Color lg_indicator = const Color(0xff42b883);

  /* colors for light-red theme */
  static const Color lr_backGround = const Color(0xfffff0f0);
  static const Color lr_appBar = Colors.white;
  static const Color lr_indicator = const Color(0xffff0000);

  /* colors for light-orange theme */
  static const Color lo_backGround = const Color(0xfffcf9ea);
  static const Color lo_appBar = Colors.white;
  static const Color lo_indicator = const Color(0xffe25822);
}