import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:kutupsozluk_app/data/model/question_model.dart';

class QuestionPage extends StatefulWidget {

  final QuestionModel questionModel;

  QuestionPage({Key key, this.questionModel}) : super(key: key);

  @override
  _QuestionPageState createState() => _QuestionPageState();
}

class _QuestionPageState extends State<QuestionPage> {



  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(42),
        child: Row(
          children: <Widget>[
            IconButton(
              icon: Icon(EvaIcons.arrowIosBackOutline),
              onPressed: () => Navigator.pop(context),
            ),
            Text('sorular')
          ],
        ),
      ),
      body: AppBar(),
    );
  }
}