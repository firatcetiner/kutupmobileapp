import 'package:kutupsozluk_app/data/model/question_model.dart';
import 'package:rxdart/rxdart.dart' show PublishSubject;

class QuestionPageBloc {

  final PublishSubject<QuestionModel> _controller = PublishSubject<QuestionModel>();


  Future<List<QuestionModel>> get stream => _controller.stream.toList();

  Future<void> getQuestionsAndAdd() {

  }

  void dispose() {
    _controller.close();
  }

}