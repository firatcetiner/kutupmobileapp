import 'package:kutupsozluk_app/main.dart';
import 'package:dio/dio.dart' show Options, DioError, DioErrorType;

class RightFrameRepository {

  Future<dynamic> fetchQuestions(int page) async {
    final url = '$baseUrl/api/forum/question/get?page=$page';
    return await client.get(
      url,
      options: Options(
        headers: {
          'accept': 'application/json',
        }
      )
    ).then((response) {
      if(response.statusCode == 200 || response.statusCode == 304) {
        return response.data['model'] as List<dynamic>;
      } else {
        throw DioError(response: response, type: DioErrorType.RESPONSE);
      }
    }).catchError((error) {
      print(error);
      throw error;
    });
  }

}