import 'package:kutupsozluk_app/data/model/question_model.dart';
import 'package:kutupsozluk_app/home/homescreen/right_frame/right_frame_repository.dart';
import 'package:rxdart/rxdart.dart';

class RightFrameBloc {

  PublishSubject<QuestionModel> _controller = PublishSubject<QuestionModel>();
  final _pageController = PublishSubject<int>();
  final _repository = RightFrameRepository();

  RightFrameBloc({int firstPage}) {
    _pageController.sink.add(firstPage);
    getQuestionsAndAdd(firstPage);
  }

  Future<List<QuestionModel>> get allQuestions => _controller.stream.toList();


  Future<void> getQuestionsAndAdd(int page) async {
    _controller = PublishSubject<QuestionModel>();
    return await _repository.fetchQuestions(page).then((response) {
      final questions = response.map<QuestionModel>((json) => QuestionModel.fromJson(json)).toList();
      for(var q in questions) {
        _controller.sink.add(q);
      }
    }).catchError((error) => _controller.sink.addError(error)).whenComplete(() {
      _controller.close();
      _pageController.sink.add(page);
    });
  }

  void dispose() {
    _pageController.close();
    _controller.close();
  }
}