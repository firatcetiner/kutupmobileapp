import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kutupsozluk_app/data/model/question_model.dart';
import 'package:kutupsozluk_app/home/homescreen/right_frame/right_frame_bloc.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';

class RightFramePage extends StatefulWidget {

  @override
  _RightFramePageState createState() => _RightFramePageState();
}

class _RightFramePageState extends State<RightFramePage> with AutomaticKeepAliveClientMixin {

  RightFrameBloc _rightFrameBloc;

  @override
  void initState() {
    super.initState();
    _rightFrameBloc = RightFrameBloc(firstPage: 1);
  }

  @override
  void dispose() {
    _rightFrameBloc.dispose();
    super.dispose();
  }

@override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      color: CustomTheme.of(context).cardColor,
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        children: [
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(
                    icon: Icon(EvaIcons.arrowIosBackOutline),
                    onPressed: () => Navigator.pop(context),
                  ),
                  Text('en son sorulanlar', style: TextStyle(fontWeight: FontWeight.w500)),
                  IconButton(
                    icon: Icon(EvaIcons.edit2, color: CustomTheme.of(context).indicatorColor),
                    onPressed: () {
                      print('new questions');
                    },
                  )
                ],
              ),
              Divider(height: 0, thickness: 1)
            ],
          ),
          RefreshIndicator(
            displacement: 10,
            onRefresh: () async => await _rightFrameBloc.getQuestionsAndAdd(1),
            color: Colors.white,
            backgroundColor: CustomTheme.of(context).indicatorColor,
            child: FutureBuilder<List<QuestionModel>>(
              future: _rightFrameBloc.allQuestions,
              builder: (context, snapshot) {
                if(!snapshot.hasData) return Text('loading');
                return ListView.separated(
                  addAutomaticKeepAlives: true,
                  shrinkWrap: true,
                  separatorBuilder: (context,index) => Divider(height: 0, thickness: 1),
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    return FlatButton(
                      onPressed: () {
                        print('open question detail');
                      },
                      child: Container(
                        color: CustomTheme.of(context).cardColor,
                        padding: EdgeInsets.all(10.0),
                        child: Row(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.center,
                              height: 20,
                              width: 20,
                              decoration: BoxDecoration(
                                color: CustomTheme.of(context).indicatorColor.withOpacity(0.6),
                                shape: BoxShape.circle
                              ),
                              child: Text(snapshot.data[index].answerCount.toString(), style: TextStyle(color: Colors.white)),
                            ),
                            Expanded(child: Padding(
                              padding: const EdgeInsets.only(left: 15),
                              child: Text(snapshot.data[index].title),
                            ))
                          ],
                        ),
                      ),
                    );
                  },
                );
              },
            ),
          )
        ],
      )
    );
  }

  @override
  bool get wantKeepAlive => true;
}