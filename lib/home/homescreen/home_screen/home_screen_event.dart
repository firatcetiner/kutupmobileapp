abstract class HomeScreenEvent {

}

class GoMain extends HomeScreenEvent {}

class GoSearch extends HomeScreenEvent {}

class GoMessage extends HomeScreenEvent {}

class GoNotifications extends HomeScreenEvent {}

class GoProfile extends HomeScreenEvent {}