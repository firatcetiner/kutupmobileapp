import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/homepage/home_page.dart';
import 'package:kutupsozluk_app/home/homescreen/right_frame/right_frame_page.dart';
import 'package:kutupsozluk_app/main.dart';
import 'package:kutupsozluk_app/notification/notification_screen.dart';
import 'package:kutupsozluk_app/profile/profile_page.dart';
import 'package:kutupsozluk_app/search/search_page/search_screen.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';
import 'home_screen_bloc.dart';


class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

class _HomeScreenState extends State<HomeScreen> with SingleTickerProviderStateMixin {
  final _homePageKey = GlobalKey<NavigatorState>();
  final _searchPageKey = GlobalKey<NavigatorState>();
  final _messagePageKey = GlobalKey<NavigatorState>();
  final _notificationPageKey = GlobalKey<NavigatorState>();
  final _profilePageKey = GlobalKey<NavigatorState>();
  List<GlobalKey<NavigatorState>> keys = List<GlobalKey<NavigatorState>>();
  HomeScreenStateBloc _homeScreenStateBloc;

  @override
  void initState() {
    super.initState();
    _homeScreenStateBloc = HomeScreenStateBloc();
    keys.add(_homePageKey);
    keys.add(_searchPageKey);
    keys.add(_messagePageKey);
    keys.add(_notificationPageKey);
    keys.add(_profilePageKey);
  }

  @override
  void dispose() {
    _homeScreenStateBloc.dispose();

    super.dispose();
  }

  void _onTap(int val, BuildContext context) {
    if(val == _homeScreenStateBloc.navigationProvider.currentPage) {
      switch (val) {
        case 0:
          _homePageKey.currentState.popUntil((route) => route.isFirst);
          break;
        case 1:
          _searchPageKey.currentState.popUntil((route) => route.isFirst);
          break;
        case 2:
          _messagePageKey.currentState.popUntil((route) => route.isFirst);
          break;
        case 3:
          _notificationPageKey.currentState.popUntil((route) => route.isFirst);
          break;
        case 4:
          _profilePageKey.currentState.popUntil((route) => route.isFirst);
          break;
        default:
      }
    } else _homeScreenStateBloc.updateNavigation(val);
  }


  @override
  Widget build(BuildContext context) {
    return StreamBuilder<int>(
      stream: _homeScreenStateBloc.getNavigation,
      initialData: _homeScreenStateBloc.navigationProvider.currentPage,
      builder: (context, snapshot) {
        var index = snapshot.data;
        return WillPopScope(
          onWillPop: () async =>
          !await keys[snapshot.data].currentState.maybePop(),
          child: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(FocusNode());
              },
              child: Scaffold(
                bottomNavigationBar: Container(
                  decoration: BoxDecoration(
                    color: CustomTheme.of(context).bottomAppBarColor,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(5),
                        topRight: Radius.circular(5)
                    ),
                    boxShadow: [
                      BoxShadow(color: Colors.black.withOpacity(0.06), blurRadius: 8.0, spreadRadius: 1.0)
                    ],
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      IconButton(
                        icon: index == 0 ? Icon(FontAwesomeIcons.solidSnowflake, color: CustomTheme.of(context).indicatorColor) :
                        Icon(FontAwesomeIcons.snowflake, color: Colors.grey),
                        onPressed: () {
                          _onTap(0, context);
                        },
                      ),
                      IconButton(
                        icon: index == 1 ? Icon(EvaIcons.search, color: CustomTheme.of(context).indicatorColor) :
                        Icon(EvaIcons.searchOutline, color: Colors.grey),
                        onPressed: () {
                          _onTap(1, context);
                        },
                      ),
                      IconButton(
                        icon: index == 2 ? Icon(EvaIcons.messageCircle, color: CustomTheme.of(context).indicatorColor) :
                        Icon(EvaIcons.messageCircleOutline, color: Colors.grey),
                        onPressed: () {
                          _onTap(2, context);
                        },
                      ),
                      IconButton(
                        icon: index == 3 ? Icon(EvaIcons.bell, color: CustomTheme.of(context).indicatorColor) :
                        Icon(EvaIcons.bellOutline, color: Colors.grey),
                        onPressed: () {
                          _onTap(3, context);
                        },
                      ),
                      GestureDetector(
                        onTap: () {
                          _onTap(4, context);
                        },
                        child: Container(
                          height: 30,
                          width: 30,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(image: NetworkImage(prefs.getString('avatar')), fit: BoxFit.fill),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                key: scaffoldKey,
                endDrawer: RightFramePage(),
                body: IndexedStack(
                  index: snapshot.data,
                  children: <Widget>[
                    Navigator(
                      key: _homePageKey,
                      onGenerateRoute: (route) => MaterialPageRoute(
                        settings: route,
                        builder: (context) => HomePage(),
                      ),
                    ),
                    Navigator(
                      key: _searchPageKey,
                      onGenerateRoute: (route) => MaterialPageRoute(
                        settings: route,
                        builder: (context) => SearchScreen(),
                      ),
                    ),
                    Container(),
                    Navigator(
                      key: _notificationPageKey,
                      onGenerateRoute: (route) => MaterialPageRoute(
                          settings: route,
                          builder: (context) => NotificationScreen()
                      ),
                    ),
                    Navigator(
                      key: _profilePageKey,
                      onGenerateRoute: (route) => MaterialPageRoute(
                        settings: route,
                        builder: (context) => ProfilePage(),
                      ),
                    )
                  ],
                ),
              )
          ),
        );
      }
    );
  }
}