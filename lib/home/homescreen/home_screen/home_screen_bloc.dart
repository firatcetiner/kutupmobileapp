import 'dart:async';
import 'package:rxdart/rxdart.dart';
import 'home_screen_provider.dart';

class HomeScreenStateBloc {
  final _navigationController = PublishSubject<int>();
  final navigationProvider = HomeScreenProvider();

  Stream get getNavigation => _navigationController.stream;

  void updateNavigation(int page) {
    navigationProvider.updatePage(page);
    _navigationController.sink.add(navigationProvider.currentPage);
  }

  void dispose() {
    _navigationController.close();
  }
}
