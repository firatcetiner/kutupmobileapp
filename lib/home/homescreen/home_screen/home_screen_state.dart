import 'package:equatable/equatable.dart';

abstract class HomeScreenState extends Equatable {

  @override
  List get props => [];
}

class Main extends HomeScreenState {
  final int index;

  Main({this.index});

  @override
  List get props => [index];
}

class Search extends HomeScreenState {
  final int index;

  Search({this.index});

  @override
  List get props => [index];
}

class Message extends HomeScreenState {
  final int index;

  Message({this.index});

  @override
  List get props => [index];
}

class Notifications extends HomeScreenState {
  final int index;

  Notifications({this.index});

  @override
  List get props => [index];
}


class Profile extends HomeScreenState {
  final int index;

  Profile({this.index});

  @override
  List get props => [index];
}
