import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/events_and_states/event.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/events_and_states/state.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/title_view/entry_list_screen.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'bloc/bloc.dart';

@immutable
class TrendsPage extends StatefulWidget {
  @override
  _TrendsPageState createState() => _TrendsPageState();
}

class _TrendsPageState extends State<TrendsPage> with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {

  ScrollController _scrollController;
  Completer<void> _refreshCompleter;
  RefreshController _refreshController;
  AnimationController _rotateController;

  @override
  void initState() {
    super.initState();
    _rotateController = AnimationController(vsync: this, duration: Duration(seconds: 5));
    _rotateController.repeat();
    _scrollController = ScrollController();
    _refreshCompleter = Completer<void>();
    _refreshController = RefreshController();
  }

  @override
  void dispose() {
    _rotateController.dispose();
    _scrollController.dispose();
    _refreshCompleter.complete();
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocListener<TrendsBloc, TitleState>(
      listener: (context, state) {
        if(state is Loaded) {
          _refreshCompleter?.complete();
          _refreshCompleter = Completer();
        }
      },
      child: BlocBuilder<TrendsBloc, TitleState>(
          builder: (context, state) {
            if (state is TitleError) {
              return Center(
                child: Text('Oops something went wrong!'),
              );
            }
            if(state is Loaded) {
              if(state.titles.isEmpty) {
                return Text('başlık yok');
              }
              return SmartRefresher(
                enablePullUp: true,
                controller: _refreshController,
                header: CustomHeader(
                  builder: (context, status) {
                    return AnimatedBuilder(
                      animation: _rotateController,
                      child: SizedBox(
                        width: 30,
                        height: 30,
                        child: Image.asset('images/snowflake-solid.png', color: CustomTheme.of(context).indicatorColor),
                      ),
                      builder: (context, child) {
                        return Padding(
                          padding: EdgeInsets.only(bottom: 15),
                          child: Transform.rotate(
                            angle: _rotateController.value * 360,
                            child: child,
                          ),
                        );
                      },
                    );
                  },
                ),
                footer: CustomFooter(
                  builder: (BuildContext context, LoadStatus mode) {
                    if(state.hasReachedMax) {
                      return Container(height: 0);
                    }
                    return AnimatedBuilder(
                      animation: _rotateController,
                      child: SizedBox(
                        width: 30,
                        height: 30,
                        child: Image.asset('images/snowflake-solid.png', color: CustomTheme.of(context).indicatorColor),
                      ),
                      builder: (context, child) {
                        return Padding(
                          padding: EdgeInsets.only(top: 15),
                          child: Transform.rotate(
                            angle: _rotateController.value * 360,
                            child: child,
                          ),
                        );
                      },
                    );
                  },
                ),
                scrollController: _scrollController,
                onRefresh: () {
                  BlocProvider.of<TrendsBloc>(context).add(Fetch(page: 1));
                  return _refreshCompleter.future.whenComplete(() {
                    _refreshController.refreshCompleted();
                  });
                },
                onLoading: () {
                  state.update(loading: true);
                  return Future.value(BlocProvider.of<TrendsBloc>(context)..add(FetchNext(page: state.nextPage))).whenComplete(() {
                    state.update(loading: false);
                    _refreshController.loadComplete();
                  });
                },
                child: ListView.separated(
                  shrinkWrap: true,
                  controller: _scrollController,
                  padding: EdgeInsets.only(top: 5),
                  separatorBuilder: (context, index) => SizedBox(height: 5),
                  itemCount: state.titles.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      child: Container(
                        decoration: BoxDecoration(
                            boxShadow: [BoxShadow(
                                color: Colors.black12.withOpacity(0.05),
                                spreadRadius: 0.5,
                                blurRadius: 2.5
                            )]
                        ),
                        child: Card(
                          child: FlatButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5)
                            ),
                            padding: EdgeInsets.all(10),
                            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                            onPressed: () {
                              Navigator.of(context).push(
                                  MaterialPageRoute(
                                      fullscreenDialog: true,
                                      builder: (context) => EntryListScreen(titleId: state.titles[index].id, titleText: state.titles[index].text)
                                  )
                              );
                            },
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    state.titles[index].text.toLowerCase(),
                                    overflow: TextOverflow.clip,
                                  ),
                                ),
                                state.titles[index].entryCount == 0 ? Container(height: 0) :
                                Text(state.titles[index].entryCount.toString())
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              );
            }
            return Center(child: Text('loading'));
          }
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}