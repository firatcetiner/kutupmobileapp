import 'package:kutupsozluk_app/main.dart';
import 'package:dio/dio.dart' show DioError, DioErrorType, Options;

class YesterdayRepository {
  Future<dynamic> fetchTitles(int date, int page) async {
    final response = await client.get(
      '$baseUrl/api/title/get-by-date?date=$date&page=$page',
      options: Options(
        headers: {
          "accept": "application/json"
        },
      )
    );
    if(response.statusCode == 200 || response.statusCode == 304) {
      return response.data['model'] as List<dynamic>;
    }
    else {
      throw DioError(response: response, type: DioErrorType.RESPONSE);
    }
  }
}