import 'package:equatable/equatable.dart';
import 'package:kutupsozluk_app/data/model/title_model.dart';
import 'package:meta/meta.dart';

abstract class TitleState extends Equatable {
  const TitleState();

  @override
  List<TitleModel> get props => [];
}

class Loading extends TitleState {}

class Loaded extends TitleState {
  final List<TitleModel> titles;
  final bool hasReachedMax;
  final int nextPage;
  final bool isLoading;

  const Loaded({@required this.isLoading, @required this.titles, this.nextPage, @required this.hasReachedMax});

  @override
  List<TitleModel> get props => titles;

  @override
  String toString() => 'Loaded { items: ${titles.length}, hasReachedMax: $hasReachedMax }';

  Loaded update({
    bool loading,
  }) {
    return Loaded(
      isLoading: loading,
      hasReachedMax: this.hasReachedMax,
      nextPage: this.nextPage,
      titles: this.titles
    );
  }
}


class Uninitialized extends TitleState {}

class TitleError extends TitleState {}

class Refreshing extends TitleState {}

