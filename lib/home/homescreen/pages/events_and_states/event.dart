import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class TitleEvent extends Equatable {
  const TitleEvent();

  @override
  List<Object> get props => [];
}

class Fetch extends TitleEvent {
  final int page;

  Fetch({@required this.page});

  @override
  String toString() => 'Fetch Titles { page: $page }';
}

class FetchNext extends TitleEvent {
  final int page;

  FetchNext({@required this.page});

  @override
  String toString() => 'Fetch Next Page of Titles: { page: $page }';
}

class FetchAnotherYear extends TitleEvent {
  final int page;
  final year;

  FetchAnotherYear({@required this.page, @required this.year});

  @override
  String toString() => 'Fetch Next Page of Titles: { page: $page, year: $year}';
}

class CheckNext extends TitleEvent {
  final int page;

  CheckNext({@required this.page});

  @override
  String toString() => 'Check Next Titles: { page: $page }';
}
