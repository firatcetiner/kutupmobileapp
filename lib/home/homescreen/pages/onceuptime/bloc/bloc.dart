import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/data/model/title_model.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/events_and_states/event.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/events_and_states/state.dart';
import 'package:rxdart/rxdart.dart';

import '../repository/repository.dart';


class UOTBloc extends Bloc<TitleEvent, TitleState> {
  final _repository = UOTRepository();
  final _yearController = PublishSubject<int>();

  bool hasReachedMax(TitleState state) => state is Loaded && state.hasReachedMax;

  @override
  TitleState get initialState => Loading();

  @override
  Stream<TitleState> mapEventToState(TitleEvent event) async* {
    if (event is Fetch) {
      yield* _mapFetchToState(event);
    } else if(event is FetchNext) {
      yield* _mapFetchNextToState(event);
    } else if (event is FetchAnotherYear) {
      yield* _mapFetchAnotherYearToState(event);
    }
  }

  Stream<TitleState> _mapFetchNextToState(FetchNext event) async* {
    final currentState = state;
    if(currentState is Loaded) {
      try {
        final response = await _repository.fetchTitlesByYear(_currentYear, currentState.nextPage);
        final titles = response.map<TitleModel>((json) => TitleModel.fromJson(json)).toList();
        yield Loaded(isLoading: false, titles: currentState.titles + titles, hasReachedMax: titles.isNotEmpty ? false : true, nextPage: currentState.nextPage + 1);
      } catch(_) {
        yield TitleError();
      }
    }
  }

  Stream<TitleState> _mapFetchAnotherYearToState(FetchAnotherYear event) async* {
    final currentState = state;
    if(currentState is Loaded) {
      yield Loading();
      try {
        final response = await _repository.fetchTitlesByYear(event.year, event.page);
        final titles = response.map<TitleModel>((json) => TitleModel.fromJson(json)).toList();
        yield Loaded(isLoading: false, titles: currentState.titles, hasReachedMax: titles.isNotEmpty ? false : true, nextPage: currentState.nextPage + 1);
      } catch(_) {
        yield TitleError();
      }
    }
  }

  Stream<TitleState> _mapFetchToState(Fetch event) async* {
    try {
      final response = await _repository.fetchTitlesByYear(_currentYear, 1);
      final items = response.map<TitleModel>((json) => TitleModel.fromJson(json)).toList();
      yield Loaded(isLoading: false, titles: items, hasReachedMax: false, nextPage: 2);
    } catch (_) {
      yield TitleError();
    }
  }

  int _currentPage = 1;
  int _currentYear = 2015;

  get currentYear => _yearController.stream;
  get currentPage => _currentPage;

  UOTBloc() {
    _yearController.sink.add(_currentYear);
  }


  void changeYear(int year) {
    this._currentYear = year;
    _yearController.sink.add(_currentYear);
  }

  @override
  Future<void> close() {
    _yearController.close();
    return super.close();
  }

}