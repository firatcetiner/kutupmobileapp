import 'package:kutupsozluk_app/main.dart';
import 'package:dio/dio.dart' show Options;
import 'package:dio/dio.dart' show DioError;
import 'package:dio/dio.dart' show DioErrorType;

class UOTRepository {

  Future<dynamic> fetchTitlesByYear(int year, int page) async {
    final response = await client.get(
      '$baseUrl/api/title/get-by-year/$year?page=$page',
      options: Options(
        headers: {
          "accept": "application/json"
        },
      )
    );
    if(response.statusCode == 200 || response.statusCode == 304) {
      return response.data['model'] as List<dynamic>;
    }
    else {
      throw DioError(response: response, type: DioErrorType.RESPONSE);
    }
  }
}