import 'dart:async';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/events_and_states/event.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/events_and_states/state.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/title_view/entry_list_screen.dart';
import 'package:kutupsozluk_app/style/custom_colors.dart' as Theme;
import 'package:kutupsozluk_app/theme/custom_theme.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../../../custom_transitions.dart';
import 'bloc/bloc.dart';


class OnceUponATimePage extends StatefulWidget {
  @override
  _OnceUponATimePageState createState() => _OnceUponATimePageState();
}

class _OnceUponATimePageState extends State<OnceUponATimePage> with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {

  ScrollController _scrollController;
  Completer<void> _refreshCompleter;
  RefreshController _refreshController;
  AnimationController _rotateController;

  UOTBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of<UOTBloc>(context);
    _rotateController = AnimationController(vsync: this, duration: Duration(seconds: 5));
    _rotateController.repeat();
    _scrollController = ScrollController();
    _refreshCompleter = Completer<void>();
    _refreshController = RefreshController();
  }

  @override
  void dispose() {

    _scrollController.dispose();
    _refreshCompleter.complete();
    _refreshController.dispose();
    super.dispose();
  }


  List<String> dates = [
    '2013',
    '2014',
    '2015',
    '2016',
    '2017',
    '2018',
  ];

  Future<void> _showPicker(BuildContext context) async {
    var _selectedIndex = 0;
    var _selectedYear = 2015;
    return await showCupertinoModalPopup(
      context: context,
      builder: (context) {
        return Container(
          color: CustomTheme.of(context).cardColor,
          margin: EdgeInsets.only(top: 445),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  CupertinoButton(
                    child: Text('iptal', style: TextStyle(color: Colors.redAccent)),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    padding: const EdgeInsets.symmetric(
                      horizontal: 16.0,
                      vertical: 5.0,
                    ),
                  ),
                  CupertinoButton(
                    child: Text('tamam', style: TextStyle(color: Theme.CustomColors.loginGradientStart)),
                    onPressed: () {
                      Navigator.pop(context);
                      _selectedYear = int.parse(dates[_selectedIndex]);
                      _bloc.changeYear(_selectedYear);
                      _bloc.add(FetchAnotherYear(page: 1, year: _selectedYear));
                    },
                    padding: const EdgeInsets.symmetric(
                      horizontal: 16.0,
                      vertical: 5.0,
                    ),
                  )
                ],
              ),
              Container(
                height: 194.0,
                color: CustomTheme.of(context).appBarTheme.color,
                child: CupertinoPicker(
                  backgroundColor: Theme.CustomColors.primaryColorWithOpacity,
                  children: dates.map((date) {
                    return Text(date);
                  }).toList(),
                  itemExtent: 40,
                  onSelectedItemChanged: (item) {
                    _selectedIndex = item;
                  },
                ),
              )
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 10, bottom: 5),
          child: StreamBuilder(
            initialData: 2015,
            stream: BlocProvider.of<UOTBloc>(context).currentYear,
            builder: (context, year) {
              return FlatButton(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                color: CustomTheme.of(context).cardColor,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                onPressed: () {
                  _showPicker(context);
                },
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(year.data.toString()),
                    Padding(
                      padding: const EdgeInsets.only(left: 5),
                      child: Icon(EvaIcons.arrowDownOutline),
                    )
                  ],
                ),
              );
            },
          ),
        ),
        Expanded(
          child: BlocListener<UOTBloc, TitleState>(
            listener: (context, state) {
              if(state is Loaded) {
                _refreshCompleter?.complete();
                _refreshCompleter = Completer();
              }
            },
            child: BlocBuilder<UOTBloc, TitleState>(
                builder: (context, state) {
                  if (state is TitleError) {
                    return Center(
                      child: Text('Oops something went wrong!'),
                    );
                  }
                  if(state is Loaded) {
                    if(state.titles.isEmpty) {
                      return Text('başlık yok');
                    }
                    return SmartRefresher(
                      enablePullUp: true,
                      controller: _refreshController,
                      header: CustomHeader(
                        builder: (context, status) {
                          return AnimatedBuilder(
                            animation: _rotateController,
                            child: SizedBox(
                              width: 30,
                              height: 30,
                              child: Image.asset('images/snowflake-solid.png', color: CustomTheme.of(context).indicatorColor),
                            ),
                            builder: (context, child) {
                              return Padding(
                                padding: EdgeInsets.only(bottom: 15),
                                child: Transform.rotate(
                                  angle: _rotateController.value * 360,
                                  child: child,
                                ),
                              );
                            },
                          );
                        },
                      ),
                      footer: CustomFooter(
                        builder: (BuildContext context, LoadStatus mode) {
                          bool hasReachedMax = BlocProvider.of<UOTBloc>(context).hasReachedMax(state);
                          if(hasReachedMax) {
                            return Container(height: 0);
                          }
                          else {
                            return AnimatedBuilder(
                              animation: _rotateController,
                              child: SizedBox(
                                width: 30,
                                height: 30,
                                child: Image.asset('images/snowflake-solid.png', color: CustomTheme.of(context).indicatorColor),
                              ),
                              builder: (context, child) {
                                return Padding(
                                  padding: EdgeInsets.only(top: 15),
                                  child: Transform.rotate(
                                    angle: _rotateController.value * 360,
                                    child: child,
                                  ),
                                );
                              },
                            );
                          }
                        },
                      ),
                      scrollController: _scrollController,
                      onRefresh: () {
                        BlocProvider.of<UOTBloc>(context).add(Fetch(page: 1));
                        return _refreshCompleter.future.whenComplete(() {
                          _refreshController.refreshCompleted();
                        });
                      },
                      onLoading: () {
                        state.update(loading: true);
                        return Future.value(BlocProvider.of<UOTBloc>(context)..add(FetchNext(page: state.nextPage))).whenComplete(() {
                          state.update(loading: false);
                          _refreshController.loadComplete();
                        });
                      },
                      child: ListView.separated(
                        shrinkWrap: true,
                        controller: _scrollController,
                        padding: EdgeInsets.only(top: 5),
                        separatorBuilder: (context, index) => SizedBox(height: 5),
                        itemCount: state.titles.length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 5),
                            child: Container(
                              decoration: BoxDecoration(
                                  boxShadow: [BoxShadow(
                                      color: Colors.black12.withOpacity(0.05),
                                      spreadRadius: 0.5,
                                      blurRadius: 2.5
                                  )]
                              ),
                              child: Card(
                                child: FlatButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5)
                                  ),
                                  padding: EdgeInsets.all(10),
                                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                  onPressed: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            fullscreenDialog: true,
                                            builder: (context) => EntryListScreen(titleId: state.titles[index].id, titleText: state.titles[index].text)
                                        )
                                    );
                                  },
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(
                                          state.titles[index].text.toLowerCase(),
                                          overflow: TextOverflow.clip,
                                        ),
                                      ),
                                      state.titles[index].entryCount == 0 ? Container(height: 0) :
                                      Text(state.titles[index].entryCount.toString())
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    );
                  }
                  return AnimatedBuilder(
                    animation: _rotateController,
                    child: SizedBox(
                      width: 30,
                      height: 30,
                      child: Image.asset('images/snowflake-solid.png', color: CustomTheme.of(context).indicatorColor),
                    ),
                    builder: (context, child) {
                      return Padding(
                        padding: EdgeInsets.only(bottom: 15),
                        child: Transform.rotate(
                          angle: _rotateController.value * 360,
                          child: child,
                        ),
                      );
                    },
                  );
                }
            ),
          ),
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}