import 'package:dio/dio.dart';
import 'package:kutupsozluk_app/main.dart';
class MainViewRepository {

  Future<dynamic> fetchTitles(int page) async {
    var watch = Stopwatch()..start();
    final response = await client.get(
      "$baseUrl/api/title/get?page=$page",
      options: Options(
        headers: {
          "accept": "application/json"
        },
      )
    );
    if(response.statusCode == 200 || response.statusCode == 304) {
      print(watch.elapsedMilliseconds);
      return response.data['model'];
    }
    else {
      throw DioError(response: response, type: DioErrorType.RESPONSE);
    }
  }
}