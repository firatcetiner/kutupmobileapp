import 'package:dio/dio.dart';
import 'package:kutupsozluk_app/main.dart';
class LastMonnthRepository {

  Future<dynamic> fetchTitles(int page) async {
    final response = await client.get(
      "$baseUrl/api/title/last-month/get?page=$page",
      options: Options(
        headers: {
          "accept": "application/json"
        },
      )
    );
    if(response.statusCode == 200 || response.statusCode == 304) {
      return response.data['model'] as List<dynamic>;
    }
    else {
      throw DioError(response: response, type: DioErrorType.RESPONSE);
    }
  }
}