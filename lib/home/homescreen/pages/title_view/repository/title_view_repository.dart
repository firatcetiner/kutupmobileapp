import 'package:dio/dio.dart';

import '../../../../../main.dart';

class TitleViewRepository {

  Future<dynamic> fetchEntriesFromTitleId(int titleId, int page) async {
    final url = '$baseUrl/api/entry/get?titleId=$titleId&page=$page';
    final token = await prefs.get('token');
    try {
      return await client.get(
      url,
      options: Options(
        headers: {
          'accept': 'application/json',
          'Authorization': 'Bearer $token'
        },
      )
    )
    .then((response) {
      client.resolve(response);
      if(response.statusCode == 200 || response.statusCode == 304) {
        return response.data['model'];
      } else {
        return response.data['message'];
      }
    }).catchError((error) {
      client.reject(error);
      print(error);
    }).timeout(Duration(milliseconds: 3000), onTimeout: () => throw DioError(type: DioErrorType.RECEIVE_TIMEOUT));
    } catch(e) {
      print(e);
      throw DioError(type: DioErrorType.RESPONSE);
    }
  }

  Future<dynamic> fetchEntriesFromTitleText(String titleText, int page) async {
    final url = 'https://$baseUrl/api/entry/get-by-title-text?titleText=$titleText&page=$page';
    final token = await prefs.get('token');
    final response = await client.get(
      url,
      options: Options(
        headers: {
          'accept': 'application/json',
          'Authorization': 'Bearer $token'
        }
      )
    );
    if(response.statusCode == 200 || response.statusCode == 304) {
      return response.data['model'] as List<dynamic>;
    } else {
      print(response.headers);
      throw DioError(response: response, type: DioErrorType.RESPONSE);
    }
  }

  Future<dynamic> followTitle(int titleId) async {
    final token = await prefs.get('token');
    return await client.post(
      '$baseUrl/api/title/$titleId/follow',
      options: Options(
        headers: {
          'accept': 'application/json',
          'Authorization': 'Bearer $token'
        }
      )
    ).then((response) {
      if(response.statusCode == 200 || response.statusCode == 304) {
        return true;
      } else return response.data['message'];
    }).catchError((error) => print(error));
  }

  Future<dynamic> unFollowTitle(int titleId) async {
    final token = await prefs.get('token');
    return await client.post(
      '$baseUrl/api/title/$titleId/unfollow',
      options: Options(
        headers: {
          'accept': 'application/json',
          'Authorization': 'Bearer $token'
        }
      )
    ).then((response) {
      if(response.statusCode == 200 || response.statusCode == 304) {
        return true;
      } else return response.data['message'];
    }).catchError((error) => print(error));
  }
}