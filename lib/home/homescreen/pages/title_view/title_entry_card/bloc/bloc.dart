import 'package:kutupsozluk_app/data/model/entry_model.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/title_view/title_entry_card/bloc/entry_event.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/title_view/title_entry_card/bloc/entry_state.dart';
import 'package:meta/meta.dart';
import '../repository/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TitleEntryCardBloc extends Bloc<EntryEvent, EntryState> {

  final Entry _entry;
  final _repository = TitleEntryCardRepository();


  TitleEntryCardBloc({@required Entry entry}) : assert(entry != null), _entry = entry;


  @override
  get initialState => Loaded(
    entry: _entry,
    isSuccess: null,
    isUpVote: _entry.voteType == 1,
    isFavourite: _entry.isFavourite == 1,
    isWarmVote: _entry.voteType == 2,
    isMinusVote:  _entry.voteType == 0,
    isLoading: false
  );

  @override
  Stream<EntryState> mapEventToState(EntryEvent event) async* {
    if(event is AddUpVote) {
      yield* _mapAddUpVoteToState(event);
    } else if(event is RemoveUpVote) {
      yield* _mapRemoveUpVoteToState(event);
    } else if(event is AddWarmVote) {
      yield* _mapAddWarmVoteToState(event);
    } else if(event is RemoveWarmVote) {
      yield* _mapRemoveWarmVoteToState(event);
    } else if(event is AddMinusVote) {
      yield* _mapAddMinusVoteToState(event);
    } else if(event is RemoveMinusVote) {
      yield* _mapRemoveMinusVoteToState(event);
    } else if(event is InitializeEntry) {
      yield* _mapInitializeEntryToState(event);
    }
  }



  Stream<EntryState> _mapAddUpVoteToState(AddUpVote event) async* {
    try {
      yield state.update(isLoading: true, isUpVote: true);
      await _repository.addVote(_entry.id, 1);
      yield state.update(isLoading: false);
    } catch(_) {
      yield state.update(isLoading: false);
    }
  }

  Stream<EntryState> _mapRemoveUpVoteToState(RemoveUpVote event) async* {
    try {
      yield state.update(isLoading: true, isUpVote: false);
      await _repository.removeVote(_entry.id);
      yield state.update(isLoading: false);
    } catch(_) {
      yield Loaded(
        entry: _entry,
        isUpVote: _entry.voteType == 1,
        isWarmVote: _entry.voteType == 2,
        isMinusVote: _entry.voteType == 0,
        isFavourite: _entry.isFavourite == 1,
        isSuccess: false,
        isLoading: false
      );
    }
  }

  Stream<EntryState> _mapAddWarmVoteToState(AddWarmVote event) async* {
    try {
      _repository.addVote(_entry.id, 2);
      yield Loaded(entry: _entry, isUpVote: false, isWarmVote: false, isMinusVote: false, isFavourite: _entry.isFavourite == 1, isSuccess: true);
    } catch(_) {
      yield Loaded(
        entry: _entry,
        isUpVote: _entry.voteType == 1,
        isWarmVote: _entry.voteType == 2,
        isMinusVote: _entry.voteType == 0,
        isFavourite: _entry.isFavourite == 1,
        isSuccess: false
      );
    }
  }

  Stream<EntryState> _mapRemoveWarmVoteToState(RemoveWarmVote event) async* {
    try {
      yield state.update(isLoading: true, isWarmVote: false);
      await _repository.removeVote(_entry.id);
      yield state.update(isLoading: false);
    } catch(_) {
      yield Loaded(
        entry: _entry,
        isUpVote: _entry.voteType == 1,
        isWarmVote: _entry.voteType == 2,
        isMinusVote: _entry.voteType == 0,
        isFavourite: _entry.isFavourite == 1,
        isSuccess: false,
        isLoading: false
      );
    }
  }

  Stream<EntryState> _mapAddMinusVoteToState(AddMinusVote event) async* {
    try {
      _repository.addVote(_entry.id, 0);
      yield Loaded(entry: _entry, isUpVote: false, isWarmVote: false, isMinusVote: true, isFavourite: _entry.isFavourite == 1, isSuccess: true);
    } catch(_) {
      yield Loaded(
        entry: _entry,
        isUpVote: _entry.voteType == 1,
        isWarmVote: _entry.voteType == 2,
        isMinusVote: _entry.voteType == 0,
        isFavourite: _entry.isFavourite == 1,
        isSuccess: false
      );
    }
  }

  Stream<EntryState> _mapRemoveMinusVoteToState(RemoveMinusVote event) async* {
    try {
      _repository.removeVote(_entry.id);
      yield Loaded(entry: _entry, isUpVote: false, isWarmVote: false, isMinusVote: false, isFavourite: _entry.isFavourite == 1, isSuccess: true);
    } catch(_) {
      yield Loaded(
        entry: _entry,
        isUpVote: _entry.voteType == 1,
        isWarmVote: _entry.voteType == 2,
        isMinusVote: _entry.voteType == 0,
        isFavourite: _entry.isFavourite == 1,
        isSuccess: false
      );
    }
  }

  Stream<EntryState> _mapInitializeEntryToState(InitializeEntry event) async* {
    yield Loaded(
      entry: event.entry,
      isUpVote: event.entry.voteType == 1,
      isWarmVote: event.entry.voteType == 2,
      isMinusVote: event.entry.voteType == 0,
      isFavourite: event.entry.isFavourite == 1,
      isSuccess: null,
      isLoading: false
    );
  }

}