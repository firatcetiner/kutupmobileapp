import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:kutupsozluk_app/data/model/entry_model.dart';

class EntryState extends Equatable {
  final Entry entry;
  final bool isUpVote;
  final bool isWarmVote;
  final bool isMinusVote;
  final bool isFavourite;
  final bool isSuccess;
  final bool isLoading;

  EntryState({
    @required this.entry,
    @required this.isUpVote,
    @required this.isWarmVote,
    @required this.isMinusVote,
    @required this.isFavourite,
    @required this.isSuccess,
    @required this.isLoading
  });

  @override
  // TODO: implement props
  List get props => [];

  EntryState update({@required bool isLoading, bool isUpVote, bool isMinusVote, bool isWarmVote, bool isFavourite}) {
    return Loaded(
      entry: this.entry,
      isUpVote: isUpVote ?? this.isUpVote,
      isLoading: isLoading,
      isMinusVote: isMinusVote ?? this.isMinusVote,
      isWarmVote: isWarmVote ?? this.isWarmVote,
      isFavourite: isFavourite ?? this.isFavourite,
      isSuccess: this.isSuccess,
    );
  }

  @override
  String toString() {
    return '''EntryState {
      isUpVote: $isUpVote,
      isWarmVote: $isWarmVote,      
      isMinusVote: $isMinusVote,
      isFavourite: $isFavourite,
      isSuccess: $isSuccess,
      isLoading: $isLoading
    }''';
  }
}

class Loaded extends EntryState {
  final Entry entry;
  final bool isUpVote;
  final bool isWarmVote;
  final bool isMinusVote;
  final bool isFavourite;
  final bool isSuccess;
  final bool isLoading;

  Loaded({
    @required this.entry,
    @required this.isUpVote,
    @required this.isWarmVote,
    @required this.isMinusVote,
    @required this.isFavourite,
    @required this.isSuccess,
    @required this.isLoading
  });

  @override
  Loaded update({@required bool isLoading, bool isUpVote, bool isMinusVote, bool isWarmVote, bool isFavourite}) {
    return Loaded(
      entry: this.entry,
      isUpVote: isUpVote ?? this.isUpVote,
      isLoading: isLoading,
      isMinusVote: isMinusVote ?? this.isMinusVote,
      isWarmVote: isWarmVote ?? this.isWarmVote,
      isFavourite: isFavourite ?? this.isFavourite,
      isSuccess: this.isSuccess,
    );
  }

  @override
  List get props => [entry, isUpVote, isWarmVote, isMinusVote, isFavourite, isSuccess, isLoading];

  @override
  String toString() {
    return '''Loaded {
      isUpVote: $isUpVote,
      isWarmVote: $isWarmVote,      
      isMinusVote: $isMinusVote,
      isFavourite: $isFavourite,
      isSuccess: $isSuccess,
      isLoading: $isLoading
    }''';
  }
}

class Loading extends EntryState {}

class Failure extends Entry {}