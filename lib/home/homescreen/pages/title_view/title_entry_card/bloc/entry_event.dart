import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:kutupsozluk_app/data/model/entry_model.dart';

abstract class EntryEvent extends Equatable {

  const EntryEvent();

  @override
  List get props => [];
}

class AddUpVote extends EntryEvent {}
class RemoveUpVote extends EntryEvent {}
class AddWarmVote extends EntryEvent {}
class RemoveWarmVote extends EntryEvent {}
class AddMinusVote extends EntryEvent {}
class RemoveMinusVote extends EntryEvent {}
class AddFavourite extends EntryEvent {}
class RemoveFavourite extends EntryEvent {}

class InitializeEntry extends EntryEvent {
  final Entry entry;

  const InitializeEntry({@required this.entry});

  @override
  List get props => [entry];
}

class FetchComments extends EntryEvent {}