import 'package:kutupsozluk_app/main.dart';
import 'package:dio/dio.dart' show Options;
import 'package:dio/dio.dart' show DioError;
import 'package:dio/dio.dart' show DioErrorType;

class TitleEntryCardRepository {

  Future<bool> addFavorite(int entryId) async {
    var url = '$baseUrl/api/entry/favourite/add';
    var token = await prefs.get('token');

    return await client.post(
      url,
      data: {
        'entryId': entryId
      },
      options: Options(
        headers: {
          'accept': 'application/json',
          'Content-Type': 'application/json-patch+json',
          'Authorization': 'Bearer $token'
        }
      )
    ).then((response) {
      if(response.statusCode == 200 || response.statusCode == 304)
        return true;
      else
        return false;
    }).catchError((error) => print(error));

  }

  Future<bool> removeFavorite(int entryId) async {
    final url = '$baseUrl/api/entry/$entryId/favourite/remove';
    final token = await prefs.get('token');
    final response = await client.delete(
        url,
        options: Options(
          headers: {
            'accept': 'application/json',
            'Authorization': 'Bearer $token'
          },
        )
    );
    if(response.statusCode == 200 || response.statusCode == 304)
      return true;
    else return false;
  }

  Future<bool> addVote(int entryId, int voteType) async {
    final url = '$baseUrl/api/entry/vote/add';
    final token = await prefs.get('token');
    print(entryId);
    return await client.postUri(
      Uri.parse(url),
      data: {
        'entryId': entryId,
        'voteTypeId': voteType
      },
      options: Options(
        headers: {
          'accept': 'application/json',
          'Content-Type': 'application/json-patch+json',
          'Authorization': 'Bearer $token'
        },
      )
    ).then((response) {
      if(response.statusCode == 200 || response.statusCode == 304)
        return true;
      else
        return false;
    }).catchError((error) => print(error));
  }

  Future<int> removeVote(int entryId) async {
    final url = '$baseUrl/api/entry/$entryId/vote/remove';
    final token = await prefs.get('token');
    return await client.delete(
      url,
      options: Options(
        headers: {
          'accept': 'application/json',
          'Authorization': 'Bearer $token'
        },
      )
    ).then((response) {
      if(response.statusCode == 200 || response.statusCode == 304)
        return 200;
      else
        return response.statusCode;
    });
  }

  Future<dynamic> getComments(int entryId) async {
    return await client.get('$baseUrl/api/comment/get?entryId=$entryId').then((response) {
      if(response.statusCode == 200 || response.statusCode == 304)
        return response.data['model'];
      else
        throw DioError(response: response, type: DioErrorType.RESPONSE);
    }).catchError((error) => throw error);
  }

}