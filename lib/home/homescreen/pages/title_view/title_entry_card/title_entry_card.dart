import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kutupsozluk_app/data/model/entry_model.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/title_view/title_entry_card/bloc/entry_event.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/title_view/title_entry_card/bloc/entry_state.dart';
import 'package:kutupsozluk_app/main.dart';
import 'package:kutupsozluk_app/profile/other_profile_page.dart';
import 'package:kutupsozluk_app/style/custom_colors.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';
import 'bloc/bloc.dart';

class TitleEntryCard extends StatelessWidget {
  final Entry entry;
  const TitleEntryCard({Key key, this.entry}) : assert(entry != null), super(key: key);

  String parseHtmlString(String htmlText) {
    RegExp exp = RegExp(
      r"<br>",
      multiLine: true,
      caseSensitive: true
    );
    return htmlText.replaceAll(exp, '\n');
  }

  @override
  Widget build(BuildContext context) {
    final uid = prefs.getString('uid');
    final _indicatorColor = CustomTheme.of(context).indicatorColor;
    return BlocBuilder<TitleEntryCardBloc, EntryState>(
      builder: (context, state) {
        if(state is Loading) {
          return Text('loading');
        }
        return Container(
        decoration: BoxDecoration(
            boxShadow: [BoxShadow(
              color: Colors.black.withOpacity(0.05),
              spreadRadius: 0.5,
              blurRadius: 2.5
            )]
          ),
          child: Card(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                if(entry.userId != int.parse(uid)) {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => OtherProfilePage(userId: entry.userId)
                                      )
                                  );
                                }
                              },
                              child: Container(
                                width: 25,
                                height: 25,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                      image: NetworkImage(entry.userAvatar),
                                    )
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    GestureDetector(
                                      child: Padding(
                                        padding: const EdgeInsets.only(bottom: 2.0),
                                        child: Text(
                                            entry.nick.toLowerCase(),
                                            style: TextStyle(color: _indicatorColor, fontWeight: FontWeight.w600)
                                        ),
                                      ),
                                      onTap: () {
                                        if(entry.userId != int.parse(prefs.getString('uid'))) {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(builder: (context) => OtherProfilePage(userId: entry.userId))
                                          );
                                        }
                                      },
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 0.0),
                                      child: Text(entry.lastUpdateDate ?? entry.date,
                                        style: TextStyle(color: Colors.grey, fontSize: 12.0),),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      GestureDetector(
                        onTap: () {

                        },
                        child: Container(child: Icon(EvaIcons.moreVertical, size: 18.0, color: Colors.grey[600]),
                        ),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Text((parseHtmlString(entry.entryText)).toLowerCase()),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            FlatButton(
                              onPressed: () {
                                if(entry.userId == int.parse(uid)) {
                                  Scaffold.of(context)
                                    ..hideCurrentSnackBar()
                                    ..showSnackBar(SnackBar(
                                      duration: Duration(milliseconds: 700),
                                      content: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text('kendi entryinize oy veremezsiniz'),
                                          Icon(FontAwesomeIcons.snowflake, size: 17)
                                        ],
                                      ),
                                    ));
                                }
                                else {
                                  if(state.isLoading) return;
                                  else {
                                    if(state.isUpVote) {
                                      BlocProvider.of<TitleEntryCardBloc>(context).add(RemoveUpVote());
                                    }
                                    if(!state.isUpVote && state.isWarmVote) {
                                      BlocProvider.of<TitleEntryCardBloc>(context).add(RemoveWarmVote());
                                      state.update(isLoading: true, isWarmVote: false, isUpVote: true);
                                      BlocProvider.of<TitleEntryCardBloc>(context).add(AddUpVote());
                                    }
                                  }
                                }
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(right: 6.0),
                                    child: Icon(FontAwesomeIcons.snowflake, size: 17, color: state.isUpVote ? CustomColors.loginGradientStart : Colors.grey[400]),
                                  ),
                                  state.entry.plus != 0 ? Text(state.entry.plus.toString(),
                                      style: TextStyle(fontSize: 18, color: state.isUpVote ? CustomColors.loginGradientStart : Colors.grey[400])) :
                                  Container(height: 0)
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: FlatButton(
                                onPressed: () {
                                  if(entry.userId == int.parse(uid)) {
                                    Scaffold.of(context)
                                      ..hideCurrentSnackBar()
                                      ..showSnackBar(SnackBar(
                                        duration: Duration(milliseconds: 700),
                                        content: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text('kendi entryinize oy veremezsiniz'),
                                            Icon(FontAwesomeIcons.thermometerHalf, size: 17)
                                          ],
                                        ),
                                      ));
                                  }
                                  else BlocProvider.of<TitleEntryCardBloc>(context).add(AddWarmVote());
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(right: 6.0),
                                      child: Icon(FontAwesomeIcons.thermometerHalf, size: 17, color: state.isWarmVote ? Colors.green : Colors.grey[400]),
                                    ),
                                    state.entry.warm != 0 ? Text(state.entry.warm.toString(),
                                        style: TextStyle(fontSize: 18, color: state.isWarmVote ? Colors.green : Colors.grey[400])) :
                                    Text('')
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: FlatButton(
                                onPressed: () {
                                  if(entry.userId == int.parse(uid)) {
                                    Scaffold.of(context)
                                      ..hideCurrentSnackBar()
                                      ..showSnackBar(SnackBar(
                                        duration: Duration(milliseconds: 700),
                                        content: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text('kendi entryinize oy veremezsiniz'),
                                            Icon(FontAwesomeIcons.fire, size: 17)
                                          ],
                                        ),
                                      ));
                                  }
                                  else BlocProvider.of<TitleEntryCardBloc>(context).add(AddMinusVote());
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(right: 6.0),
                                      child: Icon(FontAwesomeIcons.fire, size: 17, color: state.isMinusVote ? Colors.redAccent : Colors.grey[400]),
                                    ),
                                    state.entry.minus != 0 ? Text(state.entry.minus.toString(),
                                        style: TextStyle(fontSize: 18, color: state.isMinusVote ? Colors.redAccent : Colors.grey[400])) :
                                    Container(height: 0)
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: FlatButton(
                                onPressed: () {
                                  if(entry.userId == int.parse(uid)) {
                                    Scaffold.of(context)
                                      ..hideCurrentSnackBar()
                                      ..showSnackBar(SnackBar(
                                        duration: Duration(milliseconds: 700),
                                        content: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text('kendi entryinizi favori ekleyemezsiniz'),
                                            Icon(FontAwesomeIcons.heartBroken, size: 17)
                                          ],
                                        ),
                                      ));
                                  }
                                  else BlocProvider.of<TitleEntryCardBloc>(context).add(AddFavourite());
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(right: 6.0),
                                      child: Icon(state.isFavourite ? FontAwesomeIcons.solidHeart : FontAwesomeIcons.heart,
                                          size: 17,
                                          color: state.isFavourite ? Colors.red : Colors.grey[400]),
                                    ),
                                    state.entry.favorite != 0 ? Text(state.entry.favorite.toString(),
                                        style: TextStyle(fontSize: 18, color: state.isMinusVote ? Colors.red : Colors.grey[400])) :
                                    Container(height: 0)
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(right: 20.0),
                              child: FlatButton(
                                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                padding: EdgeInsets.zero,
                                onPressed: () {

                                },
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      height: 20.0,
                                      width: 20.0,
                                      child: Icon(EvaIcons.messageCircleOutline, size: 20, color: Colors.grey[400]),
                                    ),
                                    entry.commentCount != 0 ? Padding(
                                      padding: const EdgeInsets.only(left: 2.5),
                                      child: Text(entry.commentCount.toString(), style: TextStyle(color: Colors.grey, fontSize: 20)),
                                    ) : Container(height: 0),
                                  ],
                                ),
                              ),
                            ),
                            FlatButton(
                              onPressed: () {

                              },
                              child: Container(
                                height: 20.0,
                                width: 20.0,
                                child: Icon(EvaIcons.share, size: 20.0, color: Colors.grey),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

}