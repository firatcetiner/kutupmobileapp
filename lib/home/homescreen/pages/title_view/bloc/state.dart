import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:kutupsozluk_app/data/model/entry_model.dart';

class EntryListState extends Equatable {
  const EntryListState();

  @override
  List<Object> get props => [];
}

class Loading extends EntryListState {}

class Loaded extends EntryListState {
  final List<Entry> entries;

  const Loaded({@required this.entries});

  @override
  List<Entry> get props => entries;
}

class Refreshing extends EntryListState {
  final List<Entry> currentEntries;

  const Refreshing({@required this.currentEntries});

  @override
  List<Entry> get props => currentEntries;
}

class Failure extends EntryListState {
  final String message;

  const Failure({@required this.message});
}

class NetworkFailure extends EntryListState {
  final String errorMessage;

  NetworkFailure({@required this.errorMessage});
}