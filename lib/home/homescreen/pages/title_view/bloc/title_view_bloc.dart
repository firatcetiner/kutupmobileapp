import 'dart:async';
import 'package:connectivity/connectivity.dart';
import 'package:kutupsozluk_app/data/model/entry_model.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/title_view/bloc/bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../repository/title_view_repository.dart';
import 'bloc.dart';


class TitleViewBloc extends Bloc<EntryListEvent, EntryListState> {
  final _titleRepository = TitleViewRepository();
  final _followController = PublishSubject<bool>();
  final _subscription = Connectivity();

  int currentPage = 1;
  bool _following = false;
  bool _followingAction = false;
  int _titleId;
  String _titleText;

  Stream<bool> get followStream => _followController.stream;

  TitleViewBloc({int titleId, String titleText}) {
    this._titleId = titleId;
    this._titleText = titleText;
  }


  Future<void> followTitle() async {
    if(_followingAction) return;
    else {
      if(_following) {
        _followingAction = true;
        _following = false;
        _followController.sink.add(_following);
        return await _titleRepository.unFollowTitle(_titleId)
        .catchError((error) {
          print('BAŞARISIZ | BAŞLIK TAKİPTEN ÇIK: $_titleId, $_titleText');
          _following = true;
          _followController.sink.add(_following);
        })
        .whenComplete(() {
          _followingAction = false;
        });
      }
      else {
        _followingAction = true;
        _following = true;
        _followController.sink.add(_following);
        return await _titleRepository.followTitle(_titleId)
        .catchError((error) {
          print('BAŞLIK TAKİP BAŞARISIZ: $_titleId, $_titleText');
          _following = false;
          _followController.sink.add(_following);
        }).whenComplete(() {
          print('BAŞLIK TAKİP | $_following');
          _followingAction = false;
        });
      }
    }
  }

  @override
  get initialState => Loading();

  @override
  void onError(Object error, StackTrace stacktrace) {
    print(error);
    super.onError(error, stacktrace);
  }




  @override
  Future<void> close() {
    _followController.close();
    return super.close();
  }

  @override
  Stream<EntryListState> mapEventToState(EntryListEvent event) async* {
    var result = await _subscription.checkConnectivity();
    if(event is FetchEntries) {
      if(result == ConnectivityResult.none) {
        yield NetworkFailure(errorMessage: 'internet bağlantısı yok');
      }
      else yield* _mapFetchEntriesToState(event);
    }
    else if(event is RefreshEntries) {
      if(result == ConnectivityResult.none) {
        yield NetworkFailure(errorMessage: 'internet bağlantısı yok');
      }
      else yield* _mapRefreshEntriesToState(event);
    }
    else if(event is FetchEntriesWithTitleText) {
      if(result == ConnectivityResult.none) {
        yield NetworkFailure(errorMessage: 'internet bağlantısı yok');
      }
      else yield* _mapFetchEntriesWithTitleTextToState(event);
    }
    else if(event is RefreshEntriesWithTitleText) {
      if(result == ConnectivityResult.none) {
        yield NetworkFailure(errorMessage: 'internet bağlantısı yok');
      }
      else yield* _mapRefreshEntriesWithTitleTextToState(event);
    }
  }

  Stream<EntryListState> _mapFetchEntriesToState(FetchEntries event) async* {
    try {
      yield Loading();
      final response = await _titleRepository.fetchEntriesFromTitleId(_titleId, 1);
      final entries = response.map<Entry>((json) => Entry.fromJson(json)).toList();
      yield Loaded(entries: entries);
    } catch(_) {
      yield Failure(message: 'internet bağlantısı yok');
    }
  }

  Stream<EntryListState> _mapRefreshEntriesToState(RefreshEntries event) async* {
    try {
      final response = await _titleRepository.fetchEntriesFromTitleId(_titleId, 1);
      final entries = response.map<Entry>((json) => Entry.fromJson(json)).toList();
      yield Loaded(entries: entries);
    } catch(_) {
      yield Failure(message: 'entryler getirilemedi');
    }
  }

  Stream<EntryListState> _mapFetchEntriesWithTitleTextToState(FetchEntriesWithTitleText event) async* {
    try {
      final response = await _titleRepository.fetchEntriesFromTitleText(_titleText, 1);
      final entries = response.map<Entry>((json) => Entry.fromJson(json)).toList();
      yield Loaded(entries: entries);
    } catch(_) {
      yield Failure(message: 'entryler getirilemedi');
    }
  }

  Stream<EntryListState> _mapRefreshEntriesWithTitleTextToState(RefreshEntriesWithTitleText event) async* {
    try {
      final response = await _titleRepository.fetchEntriesFromTitleText(_titleText, 1);
      final entries = response.map<Entry>((json) => Entry.fromJson(json)).toList();
      yield Loaded(entries: entries);
    } catch(_) {
      yield Failure(message: 'entryler getirilemedi');
    }
  }
}