

import 'package:equatable/equatable.dart';

abstract class EntryListEvent extends Equatable {

  const EntryListEvent();

  List<Object> get props => [];
}

class FetchEntries extends EntryListEvent {}

class RefreshEntries extends EntryListEvent {}

class FetchEntriesWithTitleText extends EntryListEvent {}

class RefreshEntriesWithTitleText extends EntryListEvent {}