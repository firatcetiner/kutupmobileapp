import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/title_view/bloc/bloc.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/title_view/entry_list_page.dart';

class EntryListScreen extends StatefulWidget {

  final int titleId;
  final String titleText;

  const EntryListScreen({Key key, @required this.titleId, @required this.titleText}) : super(key: key);

  @override
  _EntryListScreenState createState() => _EntryListScreenState();
}

class _EntryListScreenState extends State<EntryListScreen> with AutomaticKeepAliveClientMixin, SingleTickerProviderStateMixin {

  AnimationController _rotateController;
  @override
  void initState() {
    super.initState();
    _rotateController = AnimationController(vsync: this, duration: Duration(seconds: 5));
    _rotateController.repeat();
  }

  @override
  void dispose() {
    _rotateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocProvider.value(
      value: TitleViewBloc(titleId: widget.titleId, titleText: widget.titleText)..add(FetchEntriesWithTitleText()),
      child: TitleView(titleId: widget.titleId, titleText: widget.titleText, rotateController: _rotateController)
    );
  }

  @override
  bool get wantKeepAlive => true;

}