import 'dart:async';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/main/main_view_page.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/title_view/bloc/bloc.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/title_view/title_entry_card/bloc/bloc.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/title_view/title_entry_card/bloc/entry_event.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/title_view/title_entry_card/title_entry_card.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/title_view/bloc/title_view_bloc.dart';
import 'package:kutupsozluk_app/home/title_entry/new_entry_page.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

@immutable
class TitleView extends StatelessWidget {

  final int titleId;
  final String titleText;
  final AnimationController rotateController;
  TitleView({Key key, @required this.titleId, @required this.titleText, @required this.rotateController}): super(key: key);
  final ScrollController _scrollController = ScrollController();
  Completer<void> _refreshCompleter = Completer<void>();
  final _refreshController = RefreshController();


  @override
  Widget build(BuildContext context) {
    var divColor = CustomTheme.of(context).dividerColor;
    return Scaffold(
      floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      floatingActionButton: FloatingActionButton(
        backgroundColor: CustomTheme.of(context).indicatorColor,
        child: Padding(
          padding: const EdgeInsets.only(bottom: 2.5),
          child: Icon(EvaIcons.edit2Outline, size: 22),
        ),
        onPressed: () {
          Navigator.of(context).push(
              MaterialPageRoute(
                  builder: (context) => NewEntry(titleId: titleId, titleText: titleText)
              )
          );
        },
      ),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(90),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.05),
              spreadRadius: 1,
              offset: Offset(0, 2.0),
              blurRadius: 4.0,
            )
          ]),
          child: AppBar(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            titleSpacing: 0,
            title: RichText(
              text: TextSpan(
                text: titleText.toLowerCase(),
                style: TextStyle(
                  fontSize: CustomTheme.of(context).appBarTheme.textTheme.title.fontSize,
                  color: CustomTheme.of(context).appBarTheme.textTheme.title.color,
                  fontWeight: CustomTheme.of(context).appBarTheme.textTheme.title.fontWeight
                ),
              ),
            ),
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(30),
              child: Column(
                children: <Widget>[
                  Divider(height: 0, thickness: 1),
                  Padding(
                    padding: const EdgeInsets.all(8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: Text('güncel'),
                        ),
                        Row(
                          children: <Widget>[
                            StreamBuilder<bool>(
                                stream: BlocProvider.of<TitleViewBloc>(context).followStream,
                                initialData: false,
                                builder: (context, following) {
                                  return Padding(
                                    padding: const EdgeInsets.only(right: 10.0),
                                    child: GestureDetector(
                                      onTap: () {
                                        if(following.data) {
                                          BlocProvider.of<TitleViewBloc>(context).followTitle();
                                          Scaffold.of(context).showSnackBar(
                                              SnackBar(
                                                content: Text('zaten takip ediliyor'),
                                              )
                                          );
                                        }
                                        else BlocProvider.of<TitleViewBloc>(context).followTitle().catchError((error) {
                                          showDialog(
                                              context: context,
                                              child: Text('zaten takip ediliyor')
                                          );
                                        });
                                      },
                                      child: Container(
                                        alignment: Alignment.center,
                                        width: 25,
                                        height: 25,
                                        decoration: BoxDecoration(
                                            color: following.data ? CustomTheme.of(context).indicatorColor.withOpacity(0.5) : divColor,
                                            shape: BoxShape.circle
                                        ),
                                        child: Icon(EvaIcons.personAddOutline, color: Colors.grey[600], size: 15),
                                      ),
                                    ),
                                  );
                                }
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: GestureDetector(
                                onTap: () {

                                },
                                child: Container(
                                  width: 25,
                                  height: 25,
                                  decoration: BoxDecoration(
                                      color: divColor,
                                      shape: BoxShape.circle
                                  ),
                                  child: Icon(EvaIcons.cornerUpRightOutline, size: 15),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: GestureDetector(
                                onTap: () {

                                },
                                child: Container(
                                  width: 25,
                                  height: 25,
                                  decoration: BoxDecoration(
                                      color: divColor,
                                      shape: BoxShape.circle
                                  ),
                                  child: Icon(EvaIcons.optionsOutline, size: 15),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: GestureDetector(
                                onTap: () {

                                },
                                child: Container(
                                  width: 25,
                                  height: 25,
                                  decoration: BoxDecoration(
                                      color: divColor,
                                      shape: BoxShape.circle
                                  ),
                                  child: Icon(EvaIcons.optionsOutline, size: 15),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: GestureDetector(
                                onTap: () {

                                },
                                child: Container(
                                  width: 25,
                                  height: 25,
                                  decoration: BoxDecoration(
                                      color: divColor,
                                      shape: BoxShape.circle
                                  ),
                                  child: Icon(EvaIcons.optionsOutline, size: 15),
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(EvaIcons.arrowIosBackOutline),
            ),
          ),
        ),
      ),
      body: BlocListener<TitleViewBloc, EntryListState>(
        listener: (context, state) {
          _refreshCompleter?.complete();
          _refreshCompleter = Completer();
        },
        child: BlocBuilder<TitleViewBloc, EntryListState>(
          builder: (context, state) {
            if(state is Failure) {
              return FlatButton(
                onPressed: () {
                  BlocProvider.of<TitleViewBloc>(context).add(FetchEntries());
                },
                child: Center(child: Text(state.message))
              );
            }
            if(state is NetworkFailure) {
              return FlatButton(
                  onPressed: () {
                    BlocProvider.of<TitleViewBloc>(context).add(FetchEntries());
                  },
                  child: Center(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Icon(EvaIcons.refreshOutline, size: 25),
                          Padding(
                            padding: const EdgeInsets.only(top: 20.0),
                            child: Text(state.errorMessage),
                          ),
                        ],
                      )
                  )
              );
            }
            if(state is Loaded) {
              if(state.entries.isEmpty) {
                return Text('entry bulunamadı.');
              }
              return SmartRefresher(
                controller: _refreshController,
                header: CustomHeader(
                  builder: (context, status) {
                    return AnimatedBuilder(
                      animation: rotateController,
                      child: SizedBox(
                        width: 30,
                        height: 30,
                        child: Image.asset('images/snowflake-solid.png', color: CustomTheme.of(context).indicatorColor),
                      ),
                      builder: (context, child) {
                        return Padding(
                          padding: EdgeInsets.only(bottom: 15),
                          child: Transform.rotate(
                            angle: rotateController.value * 360,
                            child: child,
                          ),
                        );
                      },
                    );
                  },
                ),
                scrollController: _scrollController,
                onRefresh: () {
                  BlocProvider.of<TitleViewBloc>(context).add(RefreshEntries());
                  return _refreshCompleter.future.whenComplete(() => _refreshController.refreshCompleted());
                },
                child: ListView.separated(
                    padding: EdgeInsets.symmetric(vertical: 5),
                    shrinkWrap: true,
                    physics: AlwaysScrollableScrollPhysics(),
                    separatorBuilder: (context, index) => SizedBox(height: 5),
                    controller: _scrollController,
                    itemCount: state.entries.length,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: BlocProvider.value(
                          value: TitleEntryCardBloc(entry: state.entries[index])..add(InitializeEntry(entry: state.entries[index])),
                          child: TitleEntryCard(entry: state.entries[index])
                        ),
                      );
                    },
                  )
              );
            }
            return AnimatedBuilder(
              animation: rotateController,
              child: Center(
                child: SizedBox(
                  width: 30,
                  height: 30,
                  child: Image.asset('images/snowflake-solid.png', color: CustomTheme.of(context).indicatorColor),
                ),
              ),
              builder: (context, child) {
                return Transform.rotate(
                  angle: rotateController.value * 360,
                  child: child,
                );
              },
            );
          },
        ),
      ),
    );
  }
}
