import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_tab_bar_no_ripple/flutter_tab_bar_no_ripple.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/events_and_states/event.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/homepage/snapping_scroll_physics.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/last_month/bloc/bloc.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/last_month/last_month_page.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/main/bloc/bloc.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/main/main_view_page.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/onceuptime/bloc/bloc.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/onceuptime/once_upon_a_time_page.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/random/bloc/bloc.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/random/random_page.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/trends/bloc/bloc.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/trends/trends_page.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/yesterday/bloc/bloc.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/yesterday/yesterday_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
  HomePage({Key key}) : super(key: key);
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> mainKey = GlobalKey<ScaffoldState>();
  TabController _tabController;


  @override
  void initState() {
    _tabController = TabController(vsync: this, length: 6);
    super.initState();
  }


  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: mainKey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(42),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.05),
              spreadRadius: 1,
              offset: Offset(0, 2.0),
              blurRadius: 4.0,
            )
          ]),
          child: AppBar(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            titleSpacing: 0,
            title: SizedBox(
              height: 30,
              child: TabBar(
                labelPadding: EdgeInsets.only(left: 15.0, right: 15.0),
                isScrollable: true,
                controller: _tabController,
                tabs: [
                  Text('bugün'),
                  Text('gündem'),
                  Text('dün'),
                  Text('yıl olmuş x'),
                  Text('geçen ay'),
                  Text('rastgele')
                ]
              ),
            ),
            automaticallyImplyLeading: false,
            primary: true,
          ),
        ),
      ),
      body: TabBarView(
        physics: PageScrollPhysics(),
        controller: _tabController,
        children: [
          BlocProvider<MainBloc>(
            create: (context) => MainBloc()..add(Fetch(page: 1)),
            child: LatestPage(),
          ),
          BlocProvider<TrendsBloc>(
            create: (context) => TrendsBloc()..add(Fetch(page: 1)),
            child: TrendsPage(),
          ),
          BlocProvider<YesterdayBloc>(
            create: (context) => YesterdayBloc()..add(Fetch(page: 1)),
            child: YesterdayPage(),
          ),
          BlocProvider<UOTBloc>(
            create: (context) => UOTBloc()..add(Fetch(page: 1)),
            child: OnceUponATimePage(),
          ),
          BlocProvider<LastMonthBloc>(
            create: (context) => LastMonthBloc()..add(Fetch(page: 1)),
            child: LastMonthPage(),
          ),
          BlocProvider<RandomBloc>(
            create: (context) => RandomBloc()..add(Fetch(page: 1)),
            child: RandomPage(),
          ),
        ]
      )
    );
  }
}