import 'package:rxdart/rxdart.dart';

class HomePageBloc {

  ReplaySubject<double> _scrollController = ReplaySubject<double>();

  Stream<double> get value => _scrollController.stream;

  void update(double value) {
    _scrollController.sink.add(value);
  }

  void dispose() {
    _scrollController.close();
  }
}