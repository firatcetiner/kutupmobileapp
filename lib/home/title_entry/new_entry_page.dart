import 'dart:io';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kutupsozluk_app/data/model/title_model.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/title_view/entry_list_screen.dart';
import 'package:kutupsozluk_app/home/title_entry/new_entry_bloc.dart';
import 'package:kutupsozluk_app/style/custom_colors.dart' as Theme;
import 'package:kutupsozluk_app/theme/custom_theme.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class NewEntry extends StatefulWidget {

  NewEntry({@required this.titleText, @required this.titleId}) :
        assert(titleText != null), assert(titleId != null);

  final String titleText;
  final int titleId;

  @override
  _NewEntryState createState() => _NewEntryState();
}

class _NewEntryState extends State<NewEntry> {

  NewEntryBloc _bloc;
  String get _titleText => widget.titleText.toLowerCase();
  TextEditingController _controller = TextEditingController();
  TextEditingController _bkzController = TextEditingController();
  List<Widget> _optionList;
  int length = 0;

  @override
  void initState() {
    super.initState();
    _bloc = NewEntryBloc();
    _controller.addListener(() {
      setState(() {
        length = _controller.text.replaceAll(new RegExp(r"\s+\b|\b\s"), "").trim().length;
      });
    });
    _optionList = [
      FlatButton(
        onPressed: _showBkzDialog,
        child: Text('bkz'),
      ),
      FlatButton(
        onPressed: () {
          print('gbkz');
        },
        child: Text('gbkz'),
      ),
      FlatButton(
        onPressed: () {
          print('*');
        },
        child: Text('*')
      ),
      FlatButton(
        onPressed: () {
          print('link');
        },
        child: Text('link'),
      ),
      FlatButton(
        onPressed: () {
          print('yazar');
        },
        child: Text('yazar'),
      ),
      FlatButton(
        onPressed: () {
          print('spoiler');
        },
        child: Text('spoiler'),
      )
    ];
  }

  void _addEntry() {
    _bloc.addEntry(_controller.text, widget.titleId.toString(), 'nope').then((val) {
      if(val) {
        Navigator.popUntil(context, ModalRoute.withName(Navigator.defaultRouteName));
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => EntryListScreen(titleText: _titleText, titleId: widget.titleId)
          )
        );
      } else {
        Navigator.pop(context);
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
    _bkzController.dispose();
    _bloc.dispose();
  }



  void _showBkzDialog() {

    if(Platform.isIOS) {
      showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            content: Stack(
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Flexible(
                      child: Stack(
                        children: <Widget>[
                          Card(
                            elevation: 0,
                            margin: EdgeInsets.only(bottom: 20.0),
                            child: Padding(
                              padding: EdgeInsets.all(10.0),
                              child: Row(
                                children: <Widget>[
                                  TextField(
                                    controller: _bkzController,
                                    style: TextStyle(fontSize: 15),
                                    decoration: InputDecoration.collapsed(
                                      fillColor: Colors.grey[200],
                                      hintText: ''
                                    ),
                                  ),
                                  _bkzController.text != '' ? IconButton(
                                    iconSize: 12,
                                    icon: Icon(Icons.close),
                                    onPressed: () {
                                      setState(() {
                                       _bkzController.text = ''; 
                                      });
                                    },
                                  ) : Container(height: 0)
                                ],
                              ),
                            ),
                          ),
                          _bkzController.text != '' ? Padding(
                            padding: EdgeInsets.only(top: 30),
                            child: Align(
                              alignment: Alignment.bottomCenter,
                              child: Card(
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(5.0),
                                    bottomRight: Radius.circular(5.0)
                                  )
                                ),
                                margin: EdgeInsets.only(bottom: 10),
                                child: StreamBuilder<List<TitleModel>>(
                                  stream: _bloc.titleList,
                                  builder: (context, snapshot) {
                                    return snapshot.hasData ? CupertinoScrollbar(
                                      child: ListView.builder(
                                        padding: EdgeInsets.only(bottom: 8.0),
                                        itemCount: snapshot.data.length,
                                        itemBuilder: (context, index) {
                                          return Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Divider(),
                                              Padding(
                                                padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                                                child: Text(snapshot.data[index].text.toLowerCase()),
                                              ),
                                            ],
                                          );
                                        },
                                      ),
                                    ) : Container(height: 0);
                                  },
                                ),
                              ),
                            ),
                          ) : Container(height: 0)
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        SizedBox(
                          height: 10,
                          child: OutlineButton(
                            padding: EdgeInsets.only(bottom: 0, left: 25, right: 25),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0)
                            ),
                            onPressed: () {
                              print('cancel');
                              setState(() {
                                _controller.text = '';
                              });
                              Navigator.pop(context);
                            },
                            child: Text('iptal', style: TextStyle(fontSize: 12)),
                          ),
                        ),
                        FlatButton(
                          autofocus: true,
                          padding: EdgeInsets.only(left: 20, right: 20),
                          color: Theme.CustomColors.loginGradientStart,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0)
                          ),
                          onPressed: () {
                            print('ok');
                          },
                          child: Text('tamam', style: TextStyle(color: Colors.white, fontSize: 12)),
                        ),
                      ],
                    )
                  ],
                ),
              ],
            ),
          );
        }
      );
    }
    else if(Platform.isAndroid) {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0)
            ),
            title: Text('hangi başlığa bkz verilecek?'),
            contentPadding: EdgeInsets.symmetric(horizontal: 21, vertical: 10),
            actions: <Widget>[
              OutlineButton(
                padding: EdgeInsets.only(bottom: 0, left: 25, right: 25),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0)
                ),
                onPressed: () {
                  print('cancel');
                  setState(() {
                    _bkzController.text = '';
                  });
                  Navigator.pop(context);
                },
                child: Text('iptal', style: TextStyle(fontSize: 12)),
              ),
              FlatButton(
                autofocus: true,
                padding: EdgeInsets.only(left: 20, right: 20),
                color: Theme.CustomColors.loginGradientStart,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0)
                ),
                onPressed: () {
                  setState(() {
                    _controller.text += '(bkz: ${_bkzController.text})';
                    _bkzController.text = '';
                  });
                  Navigator.pop(context);
                },
                child: Text('tamam', style: TextStyle(color: Colors.white, fontSize: 12)),
              ),
            ],
            content: StreamBuilder<bool>(
                initialData: false,
                stream: _bloc.searching,
                builder: (context, snapshot) {
                  var searching = snapshot.data;
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Card(
                        color: CustomTheme.of(context).indicatorColor.withOpacity(0.2),
                        child: Row(
                          children: <Widget>[
                            IconButton(
                              icon: Icon(EvaIcons.searchOutline, size: 20),
                              onPressed: () {
                                _bloc.updateSearch(true);
                                _bloc.getTitlesAndAdd(_bkzController.text);
                              },
                            ),
                            Expanded(
                              child: TextField(
                                controller: _bkzController,
                                style: TextStyle(fontSize: 15),
                                decoration: InputDecoration.collapsed(
                                    fillColor: Colors.grey[200],
                                    hintText: ''
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      searching ? Expanded(
                        flex: 1,
                        child: Card(
                          margin: EdgeInsets.zero,
                          child: StreamBuilder<List<TitleModel>>(
                            stream: _bloc.titleList,
                            builder: (context, snapshot) {
                              if(snapshot.hasData) {
                                if(snapshot.data.isNotEmpty) {
                                  return ListView.separated(
                                    shrinkWrap: true,
                                    separatorBuilder: (context, index) => Divider(),
                                    padding: EdgeInsets.only(top: 10),
                                    itemCount: snapshot.data.length,
                                    itemBuilder: (context, index) {
                                      return GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            _bkzController.text = snapshot.data[index].text;
                                          });
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                                          child: Text(snapshot.data[index].text.toLowerCase()),
                                        ),
                                      );
                                    },
                                  );
                                }
                                return Container(height: 0);
                              }
                              return Center(
                                child: SizedBox(
                                  width: 15,
                                  height: 15,
                                  child: CircularProgressIndicator(strokeWidth: 2),
                                ),
                              );
                            },
                          ),
                        ),
                      ) : Container(height: 0)
                    ],
                  );
                }
            ),
          );
        }
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(100),
        child: Container(
          decoration: BoxDecoration(
            boxShadow: [BoxShadow(
              color: Colors.black.withOpacity(0.05),
              spreadRadius: 1,
              offset: Offset(0, 2.0),
              blurRadius: 4.0,
            )]
          ),
          child: AppBar(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(5),
              bottomLeft: Radius.circular(5)
            )),
            centerTitle: false,
            leading: FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('iptal'),
            ),
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(30),
              child: Stack(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Divider(height: 0, thickness: 1),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                        child: Text(_titleText, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18)),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Align(
                      alignment: Alignment.bottomRight,
                      child: Text(
                        '$length/70',
                        style: TextStyle(color: length >= 70 ? Colors.green : Colors.red),
                      ),
                    ),
                  )
                ],
              )
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text('kenara koy'),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 10),
                child: StreamBuilder<bool>(
                  stream: _bloc.submitting,
                  initialData: false,
                  builder: (context, snapshot) {
                    if(!snapshot.data)
                      return Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: FlatButton(
                          onPressed: () {
                            if(length < 70) {
                              showDialog(
                                context: context,
                                builder: (context) {
                                  return AlertDialog(
                                    title: Text('başlık hatası'),
                                    content: const Text('girdiniz en az 70 karakter uzunluğunda olmalı'),
                                    actions: <Widget>[
                                      FlatButton(
                                        child: Text('tamam'),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                    ],
                                  );
                                }
                              );
                            }
                            else _addEntry();
                          },
                          child: Text('gönder', style: TextStyle(color: Theme.CustomColors.loginGradientStart, fontWeight: FontWeight.w600)),
                        ),
                      );
                    return Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: Center(child: SizedBox(width: 10, height: 10, child: CircularProgressIndicator(strokeWidth: 2.0))),
                    );
                  },
                ),
              ),
            ],
            automaticallyImplyLeading: false,
          ),
        ),
      ),
      body: StreamBuilder<Object>(
          stream: _bloc.submitting,
          initialData: false,
          builder: (context, snapshot) {
            return ModalProgressHUD(
              color: Colors.black12.withOpacity(0.2),
              inAsyncCall: snapshot.data,
              progressIndicator: SizedBox(height: 20.0, width: 20.0, child: CircularProgressIndicator(strokeWidth: 2.0)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      decoration: BoxDecoration(
                        boxShadow: [BoxShadow(
                          color: Colors.black12.withOpacity(0.1),
                          spreadRadius: 1,
                          blurRadius: 5
                        )]
                      ),
                      child: Card(
                        margin: EdgeInsets.zero,
                        child: Padding(
                          padding: const EdgeInsets.all(10),
                          child: TextField(
                            autofocus: true,
                            maxLines: null,
                            controller: _controller,
                            decoration: InputDecoration.collapsed(
                                hintText: 'entry girin'
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0, bottom: 20.0),
                    decoration: BoxDecoration(
                      boxShadow: [BoxShadow(
                        color: Colors.black12.withOpacity(0.05),
                        spreadRadius: 1,
                        blurRadius: 5.0,
                      )]
                    ),
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: _optionList
                        ),
                      ),
                    ),
                  )
                ],
              ),
            );
          }
      ),
    );
  }
}