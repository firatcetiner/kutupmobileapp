import 'package:dio/dio.dart' show Options;
import 'package:dio/dio.dart' show DioError;
import 'package:dio/dio.dart' show DioErrorType;
import 'package:kutupsozluk_app/main.dart';

class NewEntryRepository {

  Future<dynamic> fetchTitlesByWord(String word) async {
    final response = await client.get('$baseUrl/api/title/contains?word=$word');
    if(response.statusCode == 200) {
      return response.data['model'];
    } else {
      throw DioError(response: response, type: DioErrorType.RESPONSE);
    }
  }

  Future<bool> addEntry(String entryText, String titleId, String thematically) async {
    var url = '$baseUrl/api/entry/add';
    var token = await prefs.get('token');

    final response =  await client.post(
      url,
      data: {
        'entryText': entryText,
        'titleId': titleId,
        'thematically' : thematically
      },
      options: Options(
        headers: {
          'accept': 'application/json',
          'Content-Type': 'application/json-patch+json',
          'Authorization': 'Bearer $token'
        },
      )
    );

    if(response.statusCode == 200 || response.statusCode == 304)
      return true;
    else
      throw DioError(response: response, type: DioErrorType.RESPONSE);
  }
}