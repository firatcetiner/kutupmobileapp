import 'package:kutupsozluk_app/data/model/title_model.dart';
import 'package:kutupsozluk_app/home/title_entry/new_entry_repository.dart';
import 'package:rxdart/rxdart.dart';

class NewEntryBloc {

  final NewEntryRepository _repository = NewEntryRepository();

  final PublishSubject<List<TitleModel>> _titleController = PublishSubject<List<TitleModel>>();
  final PublishSubject<bool> _searchController = PublishSubject<bool>();
  final PublishSubject<bool> _entryController = PublishSubject<bool>();
  List<TitleModel> allTitles = List<TitleModel>();

  Future<bool> addEntry(String entryText, String titleId, String thematically) async {
    _entryController.sink.add(true);
    return _repository.addEntry(entryText, titleId, thematically).whenComplete(() => _entryController.sink.add(false));
  }

  Stream<List<TitleModel>> get titleList => _titleController.stream;
  Stream<bool> get searching => _searchController.stream;
  Stream<bool> get submitting => _entryController.stream;

  void updateSearch(bool b) {
    _searchController.sink.add(b);
  }

  Future<void> getTitlesAndAdd(String word) async {
    return await _repository.fetchTitlesByWord(word).then((response) {
      allTitles = response.map<TitleModel>((json) => TitleModel.fromJson(json)).toList();
      _titleController.sink.add(allTitles);
    });
  }

  void dispose() {
    _titleController.close();
    _searchController.close();
    _entryController.close();
  }
}