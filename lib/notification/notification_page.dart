import 'dart:async';
import 'dart:io';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/notification/bloc/notification_bloc.dart';
import 'package:kutupsozluk_app/notification/bloc/notification_event.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../main.dart';
import '../theme/custom_themes.dart';
import 'bloc/notification_state.dart';

@immutable
// ignore: must_be_immutable
class NotificationPage extends StatelessWidget {
  final _scrollController = ScrollController();
  Completer<void> _refreshCompleter = Completer<void>();
  final _refreshController = RefreshController();


  void _changeTheme(BuildContext buildContext, ThemeType key) {
    CustomTheme.instanceOf(buildContext).changeTheme(key);
  }

  Future<void> _showThemeChangeDialog(BuildContext context) {
    if(Platform.isIOS) {
      return showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog();
        }
      );
    }
    else if(Platform.isAndroid) {
      return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0)
            ),
            title: Center(child: Text('bir tema seçin')),
            content: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text('karanlık temalar'),
                Wrap(
                  spacing: 10,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        _changeTheme(context, ThemeType.LIGHT_BLUE);
                        Navigator.pop(context);
                      },
                      child: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          shape: BoxShape.circle
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        _changeTheme(context, ThemeType.DARK);
                        Navigator.pop(context);
                      },
                      child: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          color: Colors.red,
                          shape: BoxShape.circle
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          );
        }
      );
    }
    return null;
  }
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(42),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.05),
              spreadRadius: 1,
              offset: Offset(0, 2.0),
              blurRadius: 4.0,
            )
          ]),
          child: AppBar(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            leading: IconButton(
              icon: Icon(
                EvaIcons.activity,
                size: 20,
              ),
              onPressed: () async {
                print(prefs.getString('token'));
              },
            ),
            centerTitle: true,
            actions: <Widget>[
              IconButton(
                onPressed: () {
                  _showThemeChangeDialog(context);
                },
                icon: Icon(
                  EvaIcons.searchOutline,
                  size: 20,
                ),
              )
            ],
            title: Text('bildirimler'),
          ),
        ),
      ),
      body: BlocListener<NotificationBloc, NotificationState>(
        listener: (context, state) {
          if(state is Loaded) {
            _refreshCompleter?.complete();
            _refreshCompleter = Completer();
          }
        },
        child: BlocBuilder<NotificationBloc, NotificationState>(
          builder: (context, state) {
            if(state is Failure) {
              return Center(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: FlatButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)
                      ),
                      onPressed: () async {
                        BlocProvider.of<NotificationBloc>(context)..add(FetchNotifications(page: 1));
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Icon(EvaIcons.refreshOutline, size: 25),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: Text('bildirimler getirilemedi. tekrar denemek için tıkla.'),
                          ),
                        ],
                      ),
                    ),
                  )
              );
            }
            if(state is Loaded) {
              if(state.notifications.isEmpty) {
                return Text('0 notifications');
              }
              return SmartRefresher(
                controller: _refreshController,
                scrollController: _scrollController,
                onRefresh: () {
                  BlocProvider.of<NotificationBloc>(context).add(RefreshNotifications(page: 1));
                  return _refreshCompleter.future;
                },
                child: ListView.separated(
                    physics: AlwaysScrollableScrollPhysics(),
                    controller: _scrollController,
                    cacheExtent: 0,
                    separatorBuilder: (context, index) => SizedBox(height: 5),
                    padding: EdgeInsets.only(top: 5),
                    shrinkWrap: true,
                    itemCount: state.notifications.length + 1,
                    itemBuilder: (context, index) {
                      if(index == state.notifications.length) {
                        return Center(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: SizedBox(
                              width: 15,
                              height: 15,
                              child: CircularProgressIndicator(strokeWidth: 2),
                            ),
                          ),
                        );
                      }
                      var subject = state.notifications[index].subject;
                      var icon = Icon(EvaIcons.eye, color: CustomTheme.of(context).indicatorColor);
                      var id = 0;
                      var message = state.notifications[index].message;
                      if(subject == 'favori') icon = Icon(EvaIcons.heart, color: CustomTheme.of(context).indicatorColor);
                      else if(subject == 'eksi') icon = Icon(EvaIcons.minus, color: CustomTheme.of(context).indicatorColor);
                      else if(subject == 'yorumarti') icon = Icon(EvaIcons.plus, color: CustomTheme.of(context).indicatorColor);
                      else if(subject == 'yorumeksi') icon = Icon(EvaIcons.minus, color: CustomTheme.of(context).indicatorColor);
                      else if(subject == 'arti') icon = Icon(EvaIcons.plus, color: CustomTheme.of(context).indicatorColor);
                      else if(subject == 'ilik') icon = Icon(EvaIcons.thermometer, color: CustomTheme.of(context).indicatorColor);
                      if(state.notifications[index].id != null) id = state.notifications[index].id;
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: Container(
                          decoration: BoxDecoration(
                            boxShadow: [BoxShadow(
                              color: Colors.black.withOpacity(0.05),
                              spreadRadius: 0.5,
                              blurRadius: 2.5
                            )]
                          ),
                          child: Card(
                            child: FlatButton(
                              onPressed: () {
                                print('go notifcation details');
                              },
                              padding: EdgeInsets.all(10),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  icon,
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 10.0),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          message != null ? SelectableText(message) : Container(height: 0),
                                          id != 0 ? Padding(
                                            padding: const EdgeInsets.only(top: 3.0),
                                            child: Text('#${state.notifications[index].id}', style: TextStyle(fontWeight: FontWeight.w500),),
                                          ) : Container(height: 0)
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    }
                ),
              );
            }
            return Text('loading');
          },
        ),
      )
    );
  }

}