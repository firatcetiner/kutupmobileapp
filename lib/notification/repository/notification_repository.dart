import 'package:dio/dio.dart' show Options;
import 'package:dio/dio.dart' show DioError;
import 'package:dio/dio.dart' show DioErrorType;
import 'package:kutupsozluk_app/main.dart';

class NotificationRepository {

  Future<dynamic> fetchNotifications(int page) async {
    var userId = prefs.getString('uid');
    final url = '$baseUrl/api/user/$userId/notification?page=$page';
    final token = prefs.getString('token');
    final response = await client.get(
      url,
      options: Options(
        headers: {
          'accept': 'application/json',
          'Authorization': 'Bearer $token'
        },
      )
    );
    if(response.statusCode == 200 || response.statusCode == 304) {
      return response.data['model'] as List<dynamic>;
    } else {
      throw DioError(response: response, type: DioErrorType.RESPONSE);
    }
  }
}