import 'package:kutupsozluk_app/data/model/notification_model.dart';
import 'package:kutupsozluk_app/notification/bloc/notification_event.dart';
import 'package:kutupsozluk_app/notification/bloc/notification_state.dart';
import 'package:kutupsozluk_app/notification/repository/notification_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  final NotificationRepository _repository = NotificationRepository();


  @override
  get initialState => Loading(page: 1);

  @override
  Stream<NotificationState> mapEventToState(NotificationEvent event) async* {
    if(event is FetchNotifications) {
      yield* _mapFetchNotificationsToState(event);
    } else if(event is RefreshNotifications) {
      yield* _mapRefreshNotificationsToState(event);
    } else if(event is FetchNextNotifications) {
      yield* _mapFetchNextNotificationsToState(event);
    }
  }

  Stream<NotificationState> _mapFetchNotificationsToState(FetchNotifications fetch) async* {
    try {
      yield Loading(page: fetch.page);
      final response = await _repository.fetchNotifications(fetch.page);
      final notifications = response.map<NotificationModel>((json) => NotificationModel.fromJson(json)).toList();
      yield Loaded(notifications: notifications, page: fetch.page);
    } catch(_) {
      Failure(lastPageLoaded: fetch.page - 1);
    }
  }

  Stream<NotificationState> _mapRefreshNotificationsToState(RefreshNotifications refresh) async* {
    final currentState = state;
    try {
      if(currentState is Loaded) {
        final response = await _repository.fetchNotifications(refresh.page);
        final notifications = response.map<NotificationModel>((json) => NotificationModel.fromJson(json)).toList();
        yield Loaded(notifications: notifications, page: refresh.page);
      }
    } catch(_) {
      Failure(lastPageLoaded: 0);
    }
  }

  Stream<NotificationState> _mapFetchNextNotificationsToState(FetchNextNotifications fetchNext) async* {
    final currentState = state;
    try {
      if(currentState is Loaded) {
        final response = await _repository.fetchNotifications(fetchNext.page);
        final notifications = response.map<NotificationModel>((json) => NotificationModel.fromJson(json)).toList();
        yield Loaded(notifications: currentState.notifications + notifications, page: fetchNext.page);
      }
    } catch(_) {
      Failure(lastPageLoaded: 0);
    }
  }
}