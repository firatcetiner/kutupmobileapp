import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:kutupsozluk_app/data/model/notification_model.dart';

abstract class NotificationState extends Equatable {

  const NotificationState();

  @override
  List<Object> get props => [];
}

class Loaded extends NotificationState {
  final int page;
  final List<NotificationModel> notifications;

  Loaded({@required this.page, @required this.notifications});

  @override
  List<Object> get props => notifications;
}

class Loading extends NotificationState {
  final int page;

  Loading({@required this.page});

  @override
  List<Object> get props => [page];
}

class Failure extends NotificationState {
  final int lastPageLoaded;

  Failure({@required this.lastPageLoaded});

  @override
  List<Object> get props => [lastPageLoaded];
}