import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class NotificationEvent extends Equatable {
  const NotificationEvent();

  @override
  List<Object> get props => [];
}

class FetchNotifications extends NotificationEvent {
  final int page;

  const FetchNotifications({@required this.page});

  @override
  List<Object> get props => [page];
}

class RefreshNotifications extends NotificationEvent {
  final int page;

  const RefreshNotifications({@required this.page});

  @override
  List<Object> get props => [page];
}

class FetchNextNotifications extends NotificationEvent {
  final int currentPage;
  final int page;

  const FetchNextNotifications({@required this.page, @required this.currentPage});

  @override
  List<Object> get props => [currentPage, page];
}