import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/notification/bloc/notification_bloc.dart';
import 'package:kutupsozluk_app/notification/bloc/notification_event.dart';
import 'package:kutupsozluk_app/notification/notification_page.dart';

class NotificationScreen extends StatefulWidget {

  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> with AutomaticKeepAliveClientMixin {

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocProvider<NotificationBloc>(
      create: (context) => NotificationBloc()..add(FetchNotifications(page: 1)),
      child: NotificationPage(),
    );
  }

  @override
  bool get wantKeepAlive => true;

}