import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart' show DioErrorType;
import 'package:dio/dio.dart' show DioError;
import 'package:dio/dio.dart' show Options;
import 'package:flutter/foundation.dart';
import '../main.dart';

Future<dynamic> getUserInfo(int userId) async {
  return await client.get('$baseUrl/api/user/$userId/info').then((response) {
    if(response.statusCode == 200 || response.statusCode == 304)
      return response.data['model'];
    else
      throw DioError(response: response, type: DioErrorType.RESPONSE);
  }).catchError((error) => throw error);
}

class UserRepository {


  Future<dynamic> fetchAnnouncements(int page) async {
    final url = '$baseUrl/api/announcement/get?page=$page';
    var token = await prefs.get('token');
    var response = await client.get(
      url,
      options: Options(
        headers: {
          'accept': 'application/json',
          'Authorization': 'Bearer $token'
        }
      )
    );
    if(response.statusCode == 200 || response.statusCode == 304) {
      return response.data;
    } else {
      throw DioError(response: response, type: DioErrorType.RESPONSE);
    }
  }

  Future<dynamic> fetchQuestion(int page) async {
    final url = '$baseUrl/api/forum/question/get?page=$page';
    final response = await client.get(
      url,
      options: Options(
        headers: {
          'accept': 'application/json',
        }
      )
    );
    if(response.statusCode == 200 || response.statusCode == 304) {
      return response.data['model'] as List<dynamic>;
    } else {
      throw DioError(response: response, type: DioErrorType.RESPONSE);
    }
  }


  Future<dynamic> hasTokenExpired() async {
    final userId = prefs.getString('uid');
    var url = '$baseUrl/api/user/$userId/notification?page=1';
    var token = await prefs.get('token');
    return await client.get(
      url,
      options: Options(
        headers: {
          'accept': 'application/json',
          'Authorization': 'Bearer $token'
        },
      )
    ).then((response) {
      if(response.statusCode == 401)
        return true;
      else return false;
    });
  }

  Future<void> persisToken() async {
    final nick = await prefs.get('token');
    final password = await prefs.get('token');

    await signIn(nick, password);
  }




  Future<bool> signIn(String nick, String password) async {
    var url = '$baseUrl/api/auth/login';
    var body = {
      'nickOrEmail': nick,
      'password': password,
    };
    final response =  await client.post(
      url,
      data: body,
      options: Options(
        headers: {
          'accept': 'application/json',
          'Content-Type': 'application/json-patch+json',
        },
      )
    ).catchError((error) => throw error);
    if(response.statusCode == 200 || response.statusCode == 304) {
      var token = response.data['model'];
      var map = parseJwt(token);
      await prefs.setString('token', token).then((_) {
        prefs.setString('nick', nick);
        prefs.setString('password', password);
        prefs.setString('uid', map['nameid']);
        prefs.setString('uname', map['unique_name']);
        prefs.setInt('nbf', map['nbf']);
        prefs.setInt('exp', map['exp']);
        prefs.setInt('iat', map['iat']);
        prefs.setString('role', map['role']);
        prefs.setString('status', map['status']);
        prefs.setString('thematically', map['thematically']);
        return true;
      }).catchError((error) => print(error));
      print(prefs.getKeys());
      return true;
    } else return response.data['message'];
  }

  Future<dynamic> signUp({String nick, String password,
    String displayName, String birthDate, String email, String gender}) async {
    var url = '$baseUrl/api/auth/register';
    var body = {
      'nick': nick,
      'password': password,
      'name': displayName,
      'birthDate': birthDate,
      'email': email,
      'gender': gender,
      'mailPermission': 0
    };
    print(body);
    final response = await client.post(
      url,
      data: body,
      options: Options(
        headers: {
          'accept': 'application/json',
          'Content-Type': 'application/json-patch+json',
        },
      ),
    ).catchError((error) => throw error);
    print(response.statusCode);
    if(response.statusCode == 200 || response.statusCode == 304) {
      var token = response.data['model'];
      print(token);
      var map = parseJwt(token);
      await prefs.setString('token', token).then((_) {
        prefs.setString('nick', nick);
        prefs.setString('password', password);
        prefs.setString('uid', map['nameid']);
        prefs.setString('uname', map['unique_name']);
        prefs.setInt('nbf', map['nbf']);
        prefs.setInt('exp', map['exp']);
        prefs.setInt('iat', map['iat']);
        prefs.setString('role', map['role']);
        prefs.setString('status', map['status']);
        prefs.setString('thematically', map['thematically']);
        return true;
      }).catchError((error) => print(error));
      return true;
    }
    else
      return response.data['message'];
  }

  Future<void> signOut() async {
    await prefs.clear();
  }

  Future<bool> isSignedIn() async {
    var token = prefs.get('token');
    print(token);
    return token != null;
  }

  /* get top rated entries of user with userId and the voteType */
  Future<dynamic> getTopRatedEntries(int userId, int voteType) async {
    return await client.get('$baseUrl/api/user/$userId/top-rated?voteType=$voteType').then((response) {
      if(response.statusCode == 200 || response.statusCode == 304)
        return response.data['model'];
      else
        throw DioError(response: response, type: DioErrorType.RESPONSE);
    }).catchError((error) => throw error);
  }


  /* parse jwt token to corresponding json */
  Map<String, dynamic> parseJwt(String token) {
    final parts = token.split('.');
    if (parts.length != 3) {
      throw Exception('invalid token');
    }

    final payload = _decodeBase64(parts[1]);
    final payloadMap = json.decode(payload);
    if (payloadMap is! Map<String, dynamic>) {
      throw Exception('invalid payload');
    }

    return payloadMap;
  }

  String _decodeBase64(String str) {
    String output = str.replaceAll('-', '+').replaceAll('_', '/');
    switch (output.length % 4) {
      case 0:
        break;
      case 2:
        output += '==';
        break;
      case 3:
        output += '=';
        break;
      default:
        throw Exception('Illegal base64url string!"');
    }
    return utf8.decode(base64Url.decode(output));
  }
}