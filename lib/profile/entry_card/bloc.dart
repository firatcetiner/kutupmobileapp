import 'package:kutupsozluk_app/data/model/comment_model.dart';
import 'package:kutupsozluk_app/data/model/entry_model.dart';
import 'package:rxdart/rxdart.dart';
import 'repository.dart';

class ProfileEntryCardBloc {
  final _commentsController = PublishSubject<List<Comment>>();
  var _allComments = List<Comment>();
  ProfileEntryCardRepository _repository = ProfileEntryCardRepository();
  bool _expanded = false;
  bool _voting = false;
  bool _faving = false;
  Entry _entry;

  final _expandedController = PublishSubject<bool>();

  PublishSubject<Entry> _likeController = PublishSubject<Entry>();

  Stream<bool> get expanded => _expandedController.stream;
  Stream<List<Comment>> get allComments => _commentsController.stream;
  Stream<Entry> get isLiked => _likeController.stream;
  bool get voting => _voting;


  ProfileEntryCardBloc({Entry entry}) {
    this._entry = entry;
    getCommentsByEntryId();
  }

  Future<void> getCommentsByEntryId() async {
    if(_commentsController.isClosed) return;
    else return await _repository.getComments(_entry.id).then((response) {
      _allComments = response.map<Comment>((json) => Comment.fromJson(json)).toList();
      _commentsController.sink.add(_allComments);
    }).catchError((error) {
      _commentsController.sink.addError(error);
      throw error;
    });
  }

  void openComments() {
    _expanded = !_expanded;
    _expandedController.sink.add(_expanded);
  }

  Future<dynamic> addVote(int voteType) async {
    if(voteType == 1 && _voting == false) {
      _voting = true;
      switch(_entry.voteType) {
        case 1: /* remove up vote from entry */
          _voting = true;
          _entry.plus--;
          _entry.voteType = null;
          _likeController.sink.add(_entry);
          await _repository.removeVote(_entry.id)
              .catchError((error) {
            print('--------------------------------');
            print('OYLAMA BAŞARISIZ (SİL | KARLI OY)\nEntryID: ${_entry.id}\n');
            print('--------------------------------');
            _entry.plus++;
            _entry.voteType = 1;
            _likeController.sink.add(_entry);
          }).whenComplete(() {
            print('--------------------------------');
            print('OYLAMA BAŞARILI (SİL | KARLI OY)\nEntryID: ${_entry.id}\n');
            print('--------------------------------');
            _voting = false;
          });
          break;
        case 0: /* remove minus vote then add up vote */
          _voting = true;
          _entry.minus--;
          _entry.plus++;
          _entry.voteType = 1;
          _likeController.sink.add(_entry);
          await _repository.removeVote(_entry.id);
          await _repository.addVote(_entry.id, 1)
              .catchError((error) {
            print('--------------------------------');
            print('OYLAMA BAŞARISIZ (EKLE | KARLI OY)\nEntryID: ${_entry.id}\n');
            print('--------------------------------');
            _entry.plus--;
            _entry.minus++;
            _entry.voteType = 0;
            _likeController.sink.add(_entry);
          }).whenComplete(() {
            print('--------------------------------');
            print('OYLAMA BAŞARILI (EKLE | KARLI OY)\nEntryID: ${_entry.id}\n');
            print('--------------------------------');
            _voting = false;
          });
          break;
        case 2: /* remove warm vote then add up vote */
          _voting = true;
          _entry.warm--;
          _entry.plus++;
          _entry.voteType = 1;
          _likeController.sink.add(_entry);
          await _repository.removeVote(_entry.id);
          await _repository.addVote(_entry.id, 1)
              .catchError((error) {
            print('--------------------------------');
            print('OYLAMA BAŞARISIZ (EKLE | KARLI OY)\nEntryID: ${_entry.id}\n');
            print('--------------------------------');
            _entry.warm++;
            _entry.plus--;
            _entry.voteType = 2;
            _likeController.sink.add(_entry);
          }).whenComplete(() {
            print('--------------------------------');
            print('OYLAMA BAŞARILI (EKLE | KARLI OY)\nEntryID: ${_entry.id}\n');
            print('--------------------------------');
            _voting = false;
          });
          break;
        default:
          if(_entry.voteType != 1 && _entry.voteType != 0 && _entry.voteType != 2) {
            _voting = true;
            _entry.plus++;
            _entry.voteType = 1;
            _likeController.sink.add(_entry);
            await _repository.addVote(_entry.id, 1)
                .catchError((error) {
              print('---------------------------------');
              print('OYLAMA BAŞARISIZ (EKLE | KARLI OY)\nEntryID: ${_entry.id}\n');
              print('---------------------------------');
              _entry.plus--;
              _entry.voteType = null;
              _likeController.sink.add(_entry);
            }).whenComplete(() {
              print('---------------------------------');
              print('OYLAMA BAŞARILI (EKLE | KARLI OY)\nEntryID: ${_entry.id}\n');
              print('---------------------------------');
              _voting = false;
            });
          }
          break;
      }
    }
    if(voteType == 2 && _voting == false) {
      _voting = true;
      switch(_entry.voteType) {
        case 2:
          _voting = true;
          _entry.warm--;
          _entry.voteType = null;
          _likeController.sink.add(_entry);
          await _repository.removeVote(_entry.id)
              .catchError((error) {
            print('OYLAMA BAŞARISIZ (SİL | ILIK OY)\nEntryID: ${_entry.id}\n');
            _entry.warm++;
            _entry.voteType = 2;
            _likeController.sink.add(_entry);
          }).whenComplete(() {
            print('OYLAMA BAŞARILI (SİL | ILIK OY)\nEntryID: ${_entry.id}\n');
            _voting = false;
          });
          break;
        case 0:
          _voting = true;
          _entry.minus--;
          _entry.warm++;
          _entry.voteType = 2;
          _likeController.sink.add(_entry);
          await _repository.removeVote(_entry.id);
          await _repository.addVote(_entry.id, 2)
              .catchError((error) {
            print('OYLAMA BAŞARISIZ (EKLE | ILIK OY)\nEntryID: ${_entry.id}\n');
            _entry.minus++;
            _entry.warm--;
            _entry.voteType = 0;
            _likeController.sink.add(_entry);
          }).whenComplete(() {
            print('OYLAMA BAŞARILI (EKLE | ILIK OY)\nEntryID: ${_entry.id}\n');
            _voting = false;
          });
          break;
        case 1:
          _voting = true;
          _entry.plus--;
          _entry.warm++;
          _entry.voteType = 2;
          _likeController.sink.add(_entry);
          await _repository.removeVote(_entry.id);
          await _repository.addVote(_entry.id, 2)
              .catchError((error) {
            print('OYLAMA BAŞARISIZ (EKLE | ILIK OY)\nEntryID: ${_entry.id}\n');
            _entry.plus++;
            _entry.warm--;
            _entry.voteType = 1;
            _likeController.sink.add(_entry);
          }).whenComplete(() {
            print('OYLAMA BAŞARILI (EKLE | ILIK OY)\nEntryID: ${_entry.id}\n');
            _voting = false;
          });
          break;
        default:
          if(_entry.voteType != 1 && _entry.voteType != 0 && _entry.voteType != 2) {
            _voting = true;
            _entry.warm++;
            _entry.voteType = 2;
            _likeController.sink.add(_entry);
            await _repository.addVote(_entry.id, 0)
                .catchError((error) {
              print('---------------------------------');
              print('OYLAMA BAŞARISIZ (EKLE | ILIK OY)\nEntryID: ${_entry.id}\n');
              print('---------------------------------');
              _entry.warm--;
              _entry.voteType = null;
              _likeController.sink.add(_entry);
            }).whenComplete(() {
              print('---------------------------------');
              print('OYLAMA BAŞARILI (EKLE | ILIK OY)\nEntryID: ${_entry.id}\n');
              print('---------------------------------');
              _voting = false;
            });
          }
          break;
      }
    }
    if(voteType == 0 && _voting == false) {
      _voting = true;
      switch(_entry.voteType) {
        case 0: /* remove minus vote from entry */
          _voting = true;
          _entry.minus--;
          _entry.voteType = null;
          _likeController.sink.add(_entry);
          await _repository.removeVote(_entry.id)
              .catchError((error) {
            print('--------------------------------');
            print('OYLAMA BAŞARISIZ (SİL |  OY)\nEntryID: ${_entry.id}\n');
            print('--------------------------------');
            _entry.minus++;
            _entry.voteType = 0;
            _likeController.sink.add(_entry);
          }).whenComplete(() {
            print('--------------------------------');
            print('OYLAMA BAŞARILI (SİL | KARLI OY)\nEntryID: ${_entry.id}\n');
            print('--------------------------------');
            _voting = false;
          });
          break;
        case 1: /* remove up vote and add minus vote */
          _voting = true;
          _entry.plus--;
          _entry.minus++;
          _entry.voteType = 0;
          _likeController.sink.add(_entry);
          await _repository.removeVote(_entry.id);
          await _repository.addVote(_entry.id, 0)
              .catchError((error) {
            print('--------------------------------');
            print('OYLAMA BAŞARISIZ (EKLE | ATEŞLİ OY)\nEntryID: ${_entry.id}\n');
            print('--------------------------------');
            _entry.minus--;
            _entry.plus++;
            _entry.voteType = 1;
            _likeController.sink.add(_entry);
          }).whenComplete(() {
            print('--------------------------------');
            print('OYLAMA BAŞARILI (EKLE | ATEŞLİ OY)\nEntryID: ${_entry.id}\n');
            print('--------------------------------');
            _voting = false;
          });
          break;
        case 2: /* remove warm vote and add minus vote */
          _voting = true;
          _entry.warm--;
          _entry.minus++;
          _entry.voteType = 0;
          _likeController.sink.add(_entry);
          await _repository.removeVote(_entry.id);
          await _repository.addVote(_entry.id, 0)
              .catchError((error) {
            print('--------------------------------');
            print('OYLAMA BAŞARISIZ (EKLE | ATEŞLİ OY)\nEntryID: ${_entry.id}\n');
            print('--------------------------------');
            _entry.warm--;
            _entry.plus++;
            _entry.voteType = 2;
            _likeController.sink.add(_entry);
          }).whenComplete(() {
            print('--------------------------------');
            print('OYLAMA BAŞARILI (EKLE | ATEŞLİ OY)\nEntryID: ${_entry.id}\n');
            print('--------------------------------');
            _voting = false;
          });
          break;
      }
    }
  }

  Future<bool> addFavourite() async {
    if(_entry.isFavourite == 1) {
      _faving = true;
      _entry.favorite--;
      _entry.isFavourite = 0;
      _likeController.sink.add(_entry);
      return await _repository.removeFavorite(_entry.id)
      .catchError((error) {
        _entry.favorite++;
        _entry.isFavourite = 1;
        _likeController.sink.add(_entry);
      })
      .whenComplete(() {
        _faving = false;
      });
    }
    else {
      _faving = true;
      _entry.favorite++;
      _entry.isFavourite = 1;
      _likeController.sink.add(_entry);
      return await _repository.addFavorite(_entry.id)
      .catchError((error) {
        _entry.favorite--;
        _entry.isFavourite = 0;
        _likeController.sink.add(_entry);
      })
          .whenComplete(() {
        _faving = false;
      });
    }
  }

  void dispose() {
    _commentsController.close();
    _expandedController.close();
    _likeController.close();
  }
}