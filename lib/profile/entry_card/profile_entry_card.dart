import 'dart:ui';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kutupsozluk_app/data/model/entry_model.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/title_view/bloc/bloc.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/title_view/entry_list_screen.dart';
import 'package:kutupsozluk_app/main.dart';
import 'package:kutupsozluk_app/parse_entry/matched_text.dart';
import 'package:kutupsozluk_app/parse_entry/parse_text.dart';
import 'package:kutupsozluk_app/parse_entry/regex_options.dart';
import 'package:kutupsozluk_app/profile/entry_card/bloc.dart';
import 'package:kutupsozluk_app/style/custom_colors.dart' as Theme;
import 'package:kutupsozluk_app/style/custom_colors.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';

class EntryCard extends StatefulWidget {
  EntryCard({@required this.entry, Key key});
  final Entry entry;

  @override
  _EntryCardState createState() => _EntryCardState();
}

class _EntryCardState extends State<EntryCard> {

  Entry get entry => widget.entry;
  ProfileEntryCardBloc _bloc;

  @override
  void initState() {
    _bloc = ProfileEntryCardBloc(entry: entry);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final uid = prefs.getString('uid');
    final _color = CustomTheme.of(context).indicatorColor;
    var _expanded = false;
    var comments;
    var parsedText = parseHtmlString(entry.entryText.toLowerCase());
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(
        boxShadow: [BoxShadow(
              color: Colors.black.withOpacity(0.05),
              spreadRadius: 0.5,
              blurRadius: 2.5
          )]
      ),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(5),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(child: Text(entry.titleText.toLowerCase(), style: TextStyle(fontWeight: FontWeight.bold))),
                        GestureDetector(
                          onTap: (){
                            print(_expanded);
                          },
                          child: SizedBox(
                            width: 20,
                            height: 20,
                            child: Icon(EvaIcons.moreVerticalOutline, size: 20, color: Colors.grey[800])
                          )
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: 25,
                          height: 25,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: NetworkImage(entry.userAvatar),
                              fit: BoxFit.fill
                            )
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(entry.nick.toLowerCase(), style: TextStyle(fontWeight: FontWeight.w500)),
                              Text('#${entry.id} | ${entry.date}', style: TextStyle(fontSize: 12, color: Colors.grey[400])),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    child: ParsedText(
                      text: parsedText,
                      parse: [
                        MatchText(
                          type: ParsedType.GBKZ,
                          regexOptions: RegexOptions(
                            multiLine: true,
                            caseSensitive: true
                          ),
                          pattern: r'\(gbkz: ([^()<>]*?)(?<!\))\)',
                          style: TextStyle(fontWeight: FontWeight.w600, color: _color),
                          renderText: ({String str, String pattern}) {
                            Map<String, String> map = Map<String, String>();
                            RegExp customRegExp = RegExp(pattern);
                            Match match = customRegExp.firstMatch(str);
                            map['value'] = match.group(1);
                            map['display'] = match.group(1);
                            return map;
                          },
                          onTap: (String str) async {
                            print(str);
                          }
                        ),
                        MatchText(
                          type: ParsedType.BKZ,
                          regexOptions: RegexOptions(
                            multiLine: true,
                            caseSensitive: true
                          ),
                          pattern: r'\(bkz: ([^()<>]*?)(?<!\))\)',
                          style: TextStyle(fontWeight: FontWeight.w600, color: _color),
                          renderText: ({String str, String pattern}) {
                            Map<String, String> map = Map<String, String>();
                            RegExp customRegExp = RegExp(pattern);
                            Match match = customRegExp.firstMatch(str);
                            map['value'] = match.group(1);
                            map['display'] = match.group(1);
                            return map;
                          },
                          onTap: (String str) async {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                fullscreenDialog: true,
                                builder: (context) => EntryListScreen(titleId: 0, titleText: str)
                              )
                            );
                          }
                        ),
                        MatchText(
                          type: ParsedType.GBKZL,
                          regexOptions: RegexOptions(
                            multiLine: true,
                            caseSensitive: true
                          ),
                          pattern: r'\(gbkzl: ([^()<>]*?)(?<!\)) : ([^()<>]*?)(?<!\))\)',
                          style: TextStyle(fontWeight: FontWeight.w600, color: _color),
                          renderText: ({String str, String pattern}) {
                            Map<String, String> map = Map<String, String>();
                            RegExp customRegExp = RegExp(pattern);
                            Match match = customRegExp.firstMatch(str);
                            map['display'] = match.group(1);
                            map['value'] = match.group(2);
                            return map;
                          },
                          onTap: (String str) async {
                            print(str);
                          }
                        ),
                        MatchText(
                          type: ParsedType.UKDE,
                          regexOptions: RegexOptions(
                            multiLine: true,
                            caseSensitive: true
                          ),
                          pattern: r'\(u: ([^()<>]*?)(?<!\))\)',
                          style: TextStyle(fontWeight: FontWeight.w600, color: _color),
                          renderText: ({String str, String pattern}) {
                            Map<String, String> map = Map<String, String>();
                            RegExp customRegExp = RegExp(pattern);
                            Match match = customRegExp.firstMatch(str);
                            map['display'] = match.group(1);
                            map['value'] = match.group(1);
                            return map;
                          },
                          onTap: (String str) async {
                            print(str);
                          }
                        ),
                        MatchText(
                          type: ParsedType.YAZAR,
                          regexOptions: RegexOptions(
                            multiLine: true,
                            caseSensitive: true
                          ),
                          pattern: r'\(yazar: ([^()<>]*?)(?<!\))\)',
                          style: TextStyle(fontWeight: FontWeight.w600, color: _color),
                          renderText: ({String str, String pattern}) {
                            Map<String, String> map = Map<String, String>();
                            RegExp customRegExp = RegExp(pattern);
                            Match match = customRegExp.firstMatch(str);
                            map['display'] = match.group(1);
                            map['value'] = match.group(1);
                            return map;
                          },
                          onTap: (String yazar) {
                            print(yazar);
                          }
                        ),
                        MatchText(
                          type: ParsedType.BOLD_ITALIC,
                          regexOptions: RegexOptions(
                            multiLine: true,
                            caseSensitive: true
                          ),
                          pattern: r'\(k: ([^()<>]*?)(?<!\))\)',
                          style: TextStyle(fontWeight: FontWeight.w600),
                          renderText: ({String str, String pattern}) {
                            Map<String, String> map = Map<String, String>();
                            RegExp customRegExp = RegExp(pattern);
                            Match match = customRegExp.firstMatch(str);
                            map['display'] = match.group(1);
                            map['value'] = match.group(1);
                            return map;
                          },
                          onTap: (String yazar) {
                            print(yazar);
                          }
                        ),
                        MatchText(
                          type: ParsedType.BOLD_ITALIC,
                          regexOptions: RegexOptions(
                            multiLine: true,
                            caseSensitive: true
                          ),
                          pattern: r'\(i: ([^()<>]*?)(?<!\))\)',
                          style: TextStyle(fontStyle: FontStyle.italic),
                          renderText: ({String str, String pattern}) {
                            Map<String, String> map = Map<String, String>();
                            RegExp customRegExp = RegExp(pattern);
                            Match match = customRegExp.firstMatch(str);
                            map['display'] = match.group(1);
                            map['value'] = match.group(1);
                            return map;
                          },
                          onTap: (String yazar) {
                            print(yazar);
                          }
                        ),
                        MatchText(
                          type: ParsedType.BOLD_ITALIC,
                          regexOptions: RegexOptions(
                            multiLine: true,
                            caseSensitive: false
                          ),
                          pattern: r'\(k: \(i: ([^()<>]*?)(?<!\))\)\)',
                          style: TextStyle(fontStyle: FontStyle.italic, fontWeight: FontWeight.w600, color: _color),
                          renderText: ({String str, String pattern}) {
                            Map<String, String> map = Map<String, String>();
                            RegExp customRegExp = RegExp(pattern);
                            Match match = customRegExp.firstMatch(str);
                            map['value'] = match.group(1);
                            map['display'] = match.group(1);
                            return map;
                          },
                          onTap: null
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        StreamBuilder<Entry>(
                          stream: _bloc.isLiked,
                          initialData: entry,
                          builder: (context, snapshot) {
                            var vt = snapshot.data.voteType;
                            var plus = snapshot.data.plus;
                            var minus = snapshot.data.minus;
                            var warm = snapshot.data.warm;
                            var fav = snapshot.data.favorite;
                            var isFav = snapshot.data.isFavourite == 1;
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    FlatButton(
                                      onPressed: () {
                                        if(entry.userId == int.parse(uid)) {
                                          Scaffold.of(context)
                                          ..hideCurrentSnackBar()
                                          ..showSnackBar(SnackBar(
                                            duration: Duration(milliseconds: 700),
                                            content: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                                Text('kendi entryinize oy veremezsiniz'),
                                                Icon(Icons.info_outline)
                                              ],
                                            ),
                                          ));
                                        }
                                        else _bloc.addVote(1);
                                      },
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.only(right: 6.0),
                                            child: Icon(FontAwesomeIcons.snowflake, color: vt==1 ? CustomColors.loginGradientStart : Colors.grey[400]),
                                          ),
                                          plus != 0 ? Text(plus.toString(),
                                              style: TextStyle(fontSize: 18, color: vt==1 ? CustomColors.loginGradientStart : Colors.grey[400])) :
                                          Container(height: 0)
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 10),
                                      child: FlatButton(
                                        onPressed: () {
                                          if(entry.userId == int.parse(uid)) {
                                            Scaffold.of(context)
                                              ..hideCurrentSnackBar()
                                              ..showSnackBar(SnackBar(
                                                duration: Duration(milliseconds: 700),
                                                content: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: <Widget>[
                                                    Text('kendi entryinize oy veremezsiniz'),
                                                    Icon(Icons.info_outline)
                                                  ],
                                                ),
                                            ));
                                          }
                                         else _bloc.addVote(2);
                                        },
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(right: 6.0),
                                              child: Icon(FontAwesomeIcons.thermometerHalf, color: vt==2 ? CustomColors.loginGradientStart : Colors.grey[400]),
                                            ),
                                            warm != 0 ? Text(warm.toString(),
                                                style: TextStyle(fontSize: 18, color: vt==2 ? CustomColors.loginGradientStart : Colors.grey[400])) :
                                            Text('')
                                          ],
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 10),
                                      child: FlatButton(
                                        onPressed: () {
                                          if(entry.userId == int.parse(uid)) {
                                            Scaffold.of(context)
                                              ..hideCurrentSnackBar()
                                              ..showSnackBar(SnackBar(
                                                duration: Duration(milliseconds: 700),
                                                content: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: <Widget>[
                                                    Text('kendi entryinize oy veremezsiniz'),
                                                    Icon(Icons.info_outline)
                                                  ],
                                                ),
                                            ));
                                          }
                                          else _bloc.addVote(0);
                                        },
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(right: 6.0),
                                              child: Icon(FontAwesomeIcons.fire, color: vt==0 ? CustomColors.loginGradientStart : Colors.grey[400]),
                                            ),
                                            minus != 0 ? Text(minus.toString(),
                                                style: TextStyle(fontSize: 18, color: vt==0 ? CustomColors.loginGradientStart : Colors.grey[400])) :
                                            Container(height: 0)
                                          ],
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 10),
                                      child: FlatButton(
                                        onPressed: () {
                                          if(entry.userId == int.parse(uid)) {
                                            Scaffold.of(context)
                                              ..hideCurrentSnackBar()
                                              ..showSnackBar(SnackBar(
                                                duration: Duration(milliseconds: 700),
                                                content: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: <Widget>[
                                                    Text('kendi entryinizi favori ekleyemezsiniz'),
                                                    Icon(FontAwesomeIcons.heartBroken, size: 17)
                                                  ],
                                                ),
                                              ));
                                          }
                                          else _bloc.addFavourite();
                                        },
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(right: 6.0),
                                              child: Icon(isFav ? FontAwesomeIcons.solidHeart : FontAwesomeIcons.heart,
                                                color: isFav ? CustomColors.loginGradientStart : Colors.grey[400]),
                                            ),
                                            fav != 0 ? Text(fav.toString(),
                                              style: TextStyle(fontSize: 18, color: isFav ? CustomColors.loginGradientStart : Colors.grey[400])) :
                                            Container(height: 0)
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(left: 20.0),
                                      child: FlatButton(
                                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                        padding: EdgeInsets.zero,
                                        onPressed: () {
                                          _bloc.openComments();
                                        },
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                              height: 20.0,
                                              width: 20.0,
                                              child: Icon(EvaIcons.messageCircleOutline, size: 20, color: Colors.grey[400]),
                                            ),
                                            entry.commentCount != 0 ? Padding(
                                              padding: const EdgeInsets.only(left: 2.5),
                                              child: Text(entry.commentCount.toString(), style: TextStyle(color: Colors.grey, fontSize: 20)),
                                            ) : Container(height: 0),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            );
                          }
                        ),

                      ],
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                child: _expanded ? Column(
                  children: <Widget>[
                    comments != null ? ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      padding: EdgeInsets.only(left: 6.0, right: 6.0),
                      shrinkWrap: true,
                      itemCount: comments.length,
                      itemBuilder: (context, index) {
                        return Column(
                          children: <Widget>[
                            Container(
                              height: 10,
                              width: 1,
                              color: Theme.CustomColors.loginGradientStart,
                            ),
                            Card(
                              margin: EdgeInsets.zero,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0, top: 8.0, right: 8.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Expanded(child: Text(comments[index].commentText.toLowerCase(), style: TextStyle(fontSize: 12))),
                                        GestureDetector(
                                          child: Icon(Icons.ac_unit, size: 13),
                                          onTap: () {
                                            print('open options menu for commentbox');
                                          },
                                        )
                                      ],
                                    ),
                                  ),
                                  //Divider(),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0, bottom: 8.0, right: 8.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      mainAxisSize: MainAxisSize.max,
                                      children: <Widget>[
                                        Expanded(
                                            child: RichText(
                                              maxLines: 2,
                                              text: TextSpan(
                                                  text: comments[index].nick.toLowerCase(),
                                                  style: TextStyle(fontSize: 12, color: Theme.CustomColors.loginGradientStart.withOpacity(0.8))
                                              ),
                                            )
                                        ),
                                        Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        );
                      },
                    ) : Container(height: 0),
                  ],
                ) : Container(height: 0),
              )
            ],
          ),
        ),
      ),
    );
  }

  String parseHtmlString(String htmlText) {
    RegExp exp = RegExp(
      r"<br>",
      multiLine: true,
      caseSensitive: true
    );
    return htmlText.replaceAll(exp, '\n');
  }
}

