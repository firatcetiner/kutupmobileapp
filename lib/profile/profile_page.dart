import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/main.dart';
import 'package:kutupsozluk_app/profile/profile_screen_bloc.dart';
import 'package:kutupsozluk_app/profile/settings/profile_settings.dart';
import 'package:kutupsozluk_app/profile/tabs/commented_entries/commented_entries_tab.dart';
import 'package:kutupsozluk_app/profile/tabs/favourites/favorite_entries_tab.dart';
import 'package:kutupsozluk_app/profile/tabs/last_entries/last_entries_tab.dart';
import 'package:kutupsozluk_app/profile/tabs/pprofile/bloc/bloc.dart';
import 'package:kutupsozluk_app/profile/tabs/pprofile/bloc/event.dart';
import 'package:kutupsozluk_app/profile/tabs/pprofile/profile_tab.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';

import 'settings/followers/models/user_info_model.dart';

class ProfilePage extends StatefulWidget {

  ProfilePage({Key key}) : super(key: key);
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> with SingleTickerProviderStateMixin , AutomaticKeepAliveClientMixin {

  TabController _tabController;
  ProfileScreenBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = ProfileScreenBloc();
    _tabController = TabController(vsync: this, length: 4);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(120),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.05),
              spreadRadius: 1,
              offset: Offset(0, 2.0),
              blurRadius: 4.0,
            )
          ]),
          child: AppBar(
            automaticallyImplyLeading: false,
            actions: <Widget>[
              IconButton(
                icon: Icon(EvaIcons.settings2Outline),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          fullscreenDialog: true,
                          builder: (context) => ProfileSettings()
                      )
                  );
                },
              ),
            ],
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            titleSpacing: 0,
            flexibleSpace: SafeArea(
              bottom: false,
              child: StreamBuilder<UserInfoModel>(
                stream: _bloc.userInfo,
                builder: (context, snapshot) {
                  var info = snapshot.data;
                  return Padding(
                    padding: const EdgeInsets.only(left: 10, right: 40),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Stack(
                          overflow: Overflow.visible,
                          children: <Widget>[
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                width: 80,
                                height: 80,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(image: NetworkImage(info.image), fit: BoxFit.fill),
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.center,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 90.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    info != null ? Text(info.nick.toLowerCase(), style: TextStyle(color: CustomTheme.of(context).indicatorColor,
                                        fontWeight: FontWeight.w500))
                                        : Text('nick'),
                                    info != null ? Padding(
                                      padding: const EdgeInsets.only(top: 10.0),
                                      child: Text('${info.generation}. nesil yazar'),
                                    )
                                        : Text('0. nesil yazar'),
                                    info != null ? Text(info.message.toLowerCase())
                                        : Text(''),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  );
                }
              ),
            ),
            bottom: TabBar(
              labelPadding: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 10, top: 10),
              controller: _tabController,
              isScrollable: false,
              tabs: <Widget>[
                Text('profil', style: TextStyle(fontSize: 13.0)),
                Text('girdileri', style: TextStyle(fontSize: 13.0)),
                Text('yorumları', style: TextStyle(fontSize: 13.0)),
                Text('favorileri', style: TextStyle(fontSize: 13.0))
              ],
            ),
          ),
        ),
      ),
      body: TabBarView(
        physics: PageScrollPhysics(),
        controller: _tabController,
        children: <Widget>[
          //ProfileTab(userId: int.parse(prefs.getString('uid'))),
          BlocProvider<ProfileMainBloc>(
            create: (context) => ProfileMainBloc(userId: int.parse(prefs.getString('uid')))..add(FetchInfo()),
            child: ProfileTab(userId: int.parse(prefs.getString('uid'))),
          ),
          LastEntriesTab(userId: int.parse(prefs.getString('uid'))),
          CommentedEntriesTab(userId: int.parse(prefs.getString('uid'))),
          FavoriteEntriesTab(userId: int.parse(prefs.getString('uid'))),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}