import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:kutupsozluk_app/profile/user_repository.dart';
import 'package:rxdart/rxdart.dart';
import '../main.dart';
import 'settings/followers/models/user_info_model.dart';


class ProfileScreenBloc {
  final appBarController = StreamController<bool>();
  Stream<bool> get isExpanded => appBarController.stream;
  PublishSubject<UserInfoModel> _infoController = PublishSubject<UserInfoModel>();

  UserInfoModel _userInfoModel;

  Stream<UserInfoModel> get userInfo => _infoController.stream;

  ProfileScreenBloc() {
    getInfo();
  }

  Future<void> getInfo() {
    var uid = int.parse(prefs.getString('uid'));
    return compute(getUserInfo, uid).then((response) {
      _userInfoModel = UserInfoModel.fromJson(response);
      prefs.setString('avatar', _userInfoModel.image);
      _infoController.sink.add(_userInfoModel);
    });
  }

  void updateAppBar(bool value) {
    appBarController.sink.add(value);
  }

  void dispose() {
    appBarController.close();
    _infoController.close();
  }
}