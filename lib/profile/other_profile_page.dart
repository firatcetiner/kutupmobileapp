import 'dart:async';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:kutupsozluk_app/profile/profile_screen_bloc.dart';
import 'package:kutupsozluk_app/profile/tabs/commented_entries/commented_entries_tab.dart';
import 'package:kutupsozluk_app/profile/tabs/favourites/favorite_entries_tab.dart';
import 'package:kutupsozluk_app/profile/tabs/last_entries/last_entries_tab.dart';
import 'package:kutupsozluk_app/profile/tabs/pprofile/profile_tab.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';
import 'package:kutupsozluk_app/profile/user_repository.dart';

class OtherProfilePage extends StatefulWidget {
  final UserRepository _userRepository;
  final int _userId;

  OtherProfilePage({Key key, @required UserRepository userRepository, @required int userId})
      : assert(userRepository != null, userId != null),
        _userRepository = userRepository,
        _userId = userId,
        super(key: key);
  @override
  _OtherProfilePageState createState() => _OtherProfilePageState();
}

class _OtherProfilePageState extends State<OtherProfilePage> with SingleTickerProviderStateMixin {
  TabController _tabController;
  UserRepository get _userRepository => widget._userRepository;
  int get _userId => widget._userId;
  ProfileScreenBloc _appBarBloc = ProfileScreenBloc();

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 4);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }


  Future<Map<String,dynamic>> fetchUserInfo() async {
    var userInfo = await getUserInfo(_userId);
    if(userInfo != null) {
      return userInfo['model'];
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            color: CustomTheme.of(context).appBarTheme.color,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        IconButton(
                          icon: Icon(EvaIcons.arrowIosBack),
                          onPressed: () => Navigator.pop(context),
                        ),
                        StreamBuilder<bool>(
                          stream: _appBarBloc.isExpanded,
                          builder: (context, snapshot) {
                            var value = false;
                            if(snapshot.hasData) value = snapshot.data;
                            return AnimatedSwitcher(
                              duration: Duration(milliseconds: 200),
                              child: value ? Text('aaa') : Container(height: 0)
                            );
                          }
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        IconButton(
                          icon: Icon(EvaIcons.messageSquareOutline),
                          onPressed: () {
                            print('send message');
                          },
                        ),
                        IconButton(
                          icon: Icon(EvaIcons.moreVertical),
                          onPressed: () {
                            print('open preferences');
                          },
                        )
                      ],
                    )
                  ],
                ),
                FutureBuilder(
                    future: fetchUserInfo(),
                    builder: (context, snapshot) {
                      switch(snapshot.connectionState) {
                        case ConnectionState.waiting:
                          return Text('waiting');
                        default: if(snapshot.hasError) return Text('error!');
                        return Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: Row(
                            children: <Widget>[
                              Container(
                                height: 60,
                                width: 60,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                    fit: BoxFit.fitHeight,
                                    image: NetworkImage(
                                        snapshot.data['userImagePath']
                                    )
                                  )
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 10.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(snapshot.data['nick'], style: TextStyle(color: CustomTheme.of(context).indicatorColor)),
                                      Text('${snapshot.data['generation']}. nesil yazar'),
                                      Text(snapshot.data['message'])
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                    }
                ),
                Column(
                  children: <Widget>[
                    TabBar(
                      labelPadding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10, top: 10),
                      controller: _tabController,
                      isScrollable: false,
                      tabs: <Widget>[
                        Text('profil', style: TextStyle(fontSize: 13.0)),
                        Text('girdileri', style: TextStyle(fontSize: 13.0)),
                        Text('yorumları', style: TextStyle(fontSize: 13.0)),
                        Text('favorileri', style: TextStyle(fontSize: 13.0))
                      ],
                    ),
                    Divider(height: 0, thickness: 1.0)
                  ],
                )
              ],
            ),
          ),
          NotificationListener<UserScrollNotification>(
            onNotification: (notification) {
              if(notification.direction == ScrollDirection.reverse) {
                _appBarBloc.updateAppBar(true);
              }
              if(notification.direction == ScrollDirection.forward) {
                _appBarBloc.updateAppBar(false);
              }
              return false;
            },
            child: Expanded(
              child: TabBarView(
                physics: PageScrollPhysics(),
                controller: _tabController,
                children: <Widget>[
                  ProfileTab(userId: _userId),
                  LastEntriesTab(userId: _userId),
                  CommentedEntriesTab(userId: _userId),
                  FavoriteEntriesTab(userId: _userId),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}