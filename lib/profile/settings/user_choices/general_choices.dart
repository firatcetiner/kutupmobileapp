import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kutupsozluk_app/profile/user_repository.dart';
import 'package:kutupsozluk_app/style/custom_colors.dart' as Theme;

class GeneralChoices extends StatefulWidget {
  final UserRepository _userRepository;
  final int _userId;

  GeneralChoices({Key key, @required UserRepository userRepository, @required int userId})
      : assert(userRepository != null),
        assert(userId != null),
        _userRepository = userRepository,
        _userId = userId,
        super(key: key);
  @override
  _GeneralChoicesState createState() => _GeneralChoicesState();
}

class _GeneralChoicesState extends State<GeneralChoices> {

  int get _userId => widget._userId;
  ScrollController _scrollController;

  @override
  void initState() {
    _scrollController = ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      controller: _scrollController,
      padding: EdgeInsets.only(top: 5.0),
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(5.0),
          margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.0)
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Text('genel bilgiler'),
              ),
              Platform.isAndroid ?
              Switch(
                activeColor: Theme.CustomColors.loginGradientStart,
                value: true,
                onChanged: (value) {
                  value = !value;
                  print('changed');
                },
              ) :
              CupertinoSwitch(
                activeColor: Theme.CustomColors.loginGradientStart,
                value: true,
                onChanged: (value) {
                  value = !value;
                  print('changed');
                },
              )
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.all(20.0),
          margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0)
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('şifre'),
              Icon(Icons.ac_unit, size: 13.0)
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.all(20.0),
          margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0)
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('email'),
              Icon(Icons.ac_unit, size: 13.0)
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.all(20.0),
          margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0)
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('avatar'),
              Icon(Icons.ac_unit, size: 13.0)
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.all(20.0),
          margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0)
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('profil müziği'),
              Icon(Icons.ac_unit, size: 13.0)
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.all(20.0),
          margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0)
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('kişisel metin'),
              Icon(Icons.ac_unit, size: 13.0)
            ],
          ),
        )
      ],
    );
  }
}