import 'package:cached_network_image/cached_network_image.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/main.dart';
import 'package:kutupsozluk_app/profile/settings/followers/bloc/bloc.dart';
import 'package:kutupsozluk_app/profile/settings/followers/bloc/follow_event.dart';
import 'package:kutupsozluk_app/profile/settings/followers/bloc/follow_state.dart';
import 'package:kutupsozluk_app/profile/settings/followers/follower_model.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';

class FollowersPage extends StatefulWidget {

  @override
  _FollowersPageState createState() => _FollowersPageState();

}

class _FollowersPageState extends State<FollowersPage> with SingleTickerProviderStateMixin, AutomaticKeepAliveClientMixin {

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(80),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.05),
              spreadRadius: 1,
              offset: Offset(0, 2.0),
              blurRadius: 4.0,
            )
          ]),
          child: AppBar(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            centerTitle: true,
            title: Text('takip'),
            automaticallyImplyLeading: false,
            leading: IconButton(
              onPressed: () => Navigator.pop(context),
              icon: Icon(EvaIcons.arrowIosBackOutline),
            ),
            bottom: TabBar(
              labelPadding: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 10, top: 10),
              indicatorSize: TabBarIndicatorSize.label,
              controller: _tabController,
              tabs: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Text('takip edenler'),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Text('takip ettiklerin'),
                )
              ],
            ),
          ),
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: <Widget>[
          BlocBuilder<FollowersPageBloc, FollowState>(
            builder: (context, state) {
              if(state is Failure) {
                return Center(child: Text('Error'));
              }
              if(state is FollowersLoaded) {
                if(state.followers.isEmpty) {
                  return Text('no followers');
                }
                return RefreshIndicator(
                  onRefresh: () async {
                    BlocProvider.of<FollowersPageBloc>(context).add(FetchFollowers());
                  },
                  child: ListView.separated(
                    separatorBuilder: (context, index) => Divider(height: 0, thickness: 1.0),
                    physics: AlwaysScrollableScrollPhysics(),
                    itemCount: state.followers.length,
                    itemBuilder: (context, index) {
                      var data = state.followers[index];
                      return Container(
                        color: CustomTheme.of(context).cardColor,
                        child: Row(
                          children: <Widget>[
                            CachedNetworkImage(
                              width: 50,
                              height: 50,
                              imageUrl: state.followers[index].image,
                              placeholder: (context, url) {
                                return Container(
                                  width: 50,
                                  height: 50,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                  ),
                                  child: Text(state.followers[index].nick[0].toUpperCase()),
                                );
                              },
                              imageBuilder: (context, image) {
                                return Container(
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: DecorationImage(image: image)
                                  ),
                                );
                              },
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(data.nick),
                                  Text('${data.generation}. nesil yazar (${data.registerDate.split(" ")[0]})'),
                                  Text(data.message)
                                ],
                              ),
                            )
                          ],
                        ),
                      );
                    },
                  ),
                );
              }
              return Center(child: Text('Loading'));
            },
          ),
          Container()
        ],
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

}