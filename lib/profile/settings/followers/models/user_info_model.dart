class UserInfoModel {
  String nick;
  String registerDate;
  int generation;
  String status;
  String message;
  String image;
  int entryCount;
  int plus;
  int warm;
  int favourite;
  int minus;
  int followerCount;
  int followingCount;
  int reputationPoint;
  String reputationMessage;

  UserInfoModel({this.nick, this.registerDate, this.generation, this.status, this.message, this.image, this.entryCount, this.plus,
    this.warm, this.favourite, this.minus, this.followerCount, this.followingCount, this.reputationPoint, this.reputationMessage});

  UserInfoModel.fromJson(Map<String, dynamic> json) {
    nick = json['nick'];
    image = json['userImagePath'];
    registerDate = json['registerDate'];
    generation = json['generation'];
    status = json['status'];
    message = json['message'];
    entryCount = json['entryCount'];
    plus = json['plus'];
    warm = json['warm'];
    favourite = json['favourite'];
    minus = json['minus'];
    followerCount = json['followerCount'];
    followingCount = json['followingCount'];
    reputationPoint = json['reputationPoint'];
    reputationMessage = json['reputationMessage'];
  }
}