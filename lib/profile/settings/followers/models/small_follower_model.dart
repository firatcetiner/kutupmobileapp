class SmallFollowerModel {
  int targetUserId;
  String nick;

  SmallFollowerModel({this.nick, this.targetUserId});

  SmallFollowerModel.fromJson(Map<String, dynamic> json) {
    targetUserId = json['targetUserId'];
    nick = json['nick'];
  }
}