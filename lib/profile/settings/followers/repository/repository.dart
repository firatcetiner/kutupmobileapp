import 'package:kutupsozluk_app/main.dart';
import 'package:dio/dio.dart' show DioError;
import 'package:dio/dio.dart' show DioErrorType;


class FollowersPageRepository {

  /* get followers userId and the unique_name */
  Future<dynamic> getFollowers(int userId) async {
    var response = await client.get('$baseUrl/api/user/$userId/followers');

    if(response.statusCode == 200 || response.statusCode== 304)
      return response.data['model'];
    else
      throw DioError(response: response, type: DioErrorType.RESPONSE);
  }
  /* get followings userId and the unique_name */
  Future<dynamic> getFollowings(int userId) async {
    return await client.get('$baseUrl/api/user/$userId/followings').then((response) {
      if(response.statusCode == 200 || response.statusCode== 304)
        return response.data['model'];
      else
        throw DioError(response: response, type: DioErrorType.RESPONSE);
    }).catchError((error) => throw error);
  }

  Future<dynamic> getUserInfo(int userId) async {
    return await client.get('$baseUrl/api/user/$userId/info').then((response) {
      if(response.statusCode == 200 || response.statusCode == 304)
        return response.data['model'];
      else
        throw DioError(response: response, type: DioErrorType.RESPONSE);
    }).catchError((error) => throw error);
  }
}