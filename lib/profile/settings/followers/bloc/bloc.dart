import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/profile/settings/followers/follower_model.dart';
import 'package:kutupsozluk_app/profile/settings/followers/repository/repository.dart';
import 'package:kutupsozluk_app/profile/settings/followers/models/small_follower_model.dart';

import 'follow_event.dart';
import 'follow_state.dart';

class FollowersPageBloc extends Bloc<FollowEvent, FollowState>{

  final _repository = FollowersPageRepository();

  int _userId;

  FollowersPageBloc({int userId}) {
    this._userId = userId;
  }


  @override
  FollowState get initialState => Loading();

  @override
  Stream<FollowState> mapEventToState(FollowEvent event) async* {
    if(event is FetchFollowers) {
      yield* _mapFetchFollowersToState(event);
    } else if(event is FetchFollowings) {
      yield* _mapFetchFollowingsToState(event);
    }
  }

  Stream<FollowState> _mapFetchFollowersToState(FetchFollowers event) async* {
    try {
      var followerList = List<FollowerModel>();
      final initialFollowerResponse = await _repository.getFollowers(_userId);
      final initialFollowers = initialFollowerResponse.map<SmallFollowerModel>((json) => SmallFollowerModel.fromJson(json)).toList();
      for(SmallFollowerModel f in initialFollowers) {
        final userInfo = await _repository.getUserInfo(f.targetUserId);
        followerList.add(FollowerModel.fromJson(userInfo));
        yield FollowersLoaded(followers: followerList);
      }
    } catch(_) {
      yield Failure();
    }
  }

  Stream<FollowState> _mapFetchFollowingsToState(FetchFollowings event) async* {
    try {
      await _repository.getFollowings(_userId).then((response) async* {
        var followingsList = List<FollowerModel>();
        final initialFollowers = response.map<SmallFollowerModel>((json) => SmallFollowerModel.fromJson(json)).toList();
        for(SmallFollowerModel f in initialFollowers) {
          _repository.getUserInfo(f.targetUserId).then((info) {
            var follower = FollowerModel.fromJson(info);
            followingsList.add(follower);
          });
        }
        yield FollowingsLoaded(followings: followingsList);
      });
    } catch(_) {
      yield Failure();
    }
  }
}
