import 'package:equatable/equatable.dart';

class FollowEvent extends Equatable {
  const FollowEvent();
}

class FetchFollowers extends FollowEvent {}

class FetchFollowings extends FollowEvent {}
