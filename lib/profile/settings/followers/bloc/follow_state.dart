import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:kutupsozluk_app/profile/settings/followers/follower_model.dart';

class FollowState extends Equatable {
  const FollowState();

  @override
  List get props => [];
}

class FollowersLoaded extends FollowState {
  final List<FollowerModel> followers;

  FollowersLoaded({@required this.followers});

  @override
  List get props => followers;
}

class FollowingsLoaded extends FollowState {
  final List<FollowerModel> followings;

  FollowingsLoaded({@required this.followings});

  @override
  List get props => followings;
}

class Loading extends FollowState {}

class Failure extends FollowState {}