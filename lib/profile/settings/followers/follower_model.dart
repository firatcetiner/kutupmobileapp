class FollowerModel {
  int id;
  String nick;
  String registerDate;
  int generation;
  String status;
  String message;
  String image;
  String reputationMessage;

  FollowerModel({this.id, this.nick, this.registerDate, this.generation, this.status, this.message, this.image, this.reputationMessage});

  FollowerModel.fromJson(Map<String, dynamic> json) {
    nick = json['nick'];
    registerDate = json['registerDate'];
    generation = json['generation'];
    status = json['status'];
    message = json['message'];
    image = json['userImagePath'];
    reputationMessage = json['reputationMessage'];
  }
}