import 'dart:io';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/authentication_bloc/authentication_bloc.dart';
import 'package:kutupsozluk_app/authentication_bloc/authentication_event.dart';
import 'package:kutupsozluk_app/custom_transitions.dart';
import 'package:kutupsozluk_app/main.dart';
import 'package:kutupsozluk_app/profile/settings/blockeds/blockeds_page.dart';
import 'package:kutupsozluk_app/profile/settings/followers/bloc/bloc.dart';
import 'package:kutupsozluk_app/profile/settings/followers/bloc/follow_event.dart';
import 'package:kutupsozluk_app/profile/settings/user_info/general_info.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';
import 'package:kutupsozluk_app/theme/custom_themes.dart';

import 'followers/followers_page.dart';

class ProfileSettings extends StatefulWidget {

  ProfileSettings({Key key}) : super(key: key);

  @override
  _ProfileSettingsState createState() => _ProfileSettingsState();
}

class _ProfileSettingsState extends State<ProfileSettings> {

  ScrollController _scrollController;

  @override
  void initState() {
    _scrollController = ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }

  void _changeTheme(BuildContext buildContext, ThemeType key) {
    CustomTheme.instanceOf(buildContext).changeTheme(key);
  }

  Future<void> _showThemeChangeDialog() {
    if(Platform.isIOS) {
      return showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog();
        }
      );
    }
    else if(Platform.isAndroid) {
      return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0)
            ),
            title: Center(child: Text('bir tema seçin')),
            content: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text('karanlık temalar'),
                Wrap(
                  spacing: 10,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        _changeTheme(context, ThemeType.DARK);
                        Navigator.pop(context);
                      },
                      child: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          shape: BoxShape.circle
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        _changeTheme(context, ThemeType.LIGHT_BLUE);
                        Navigator.pop(context);
                      },
                      child: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                            color: Colors.orangeAccent,
                            shape: BoxShape.circle
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        _changeTheme(context, ThemeType.DARKER);
                        Navigator.pop(context);
                      },
                      child: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                            color: Colors.redAccent,
                            shape: BoxShape.circle
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          );
        }
      );
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(42),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.05),
              spreadRadius: 1,
              offset: Offset(0, 2.0),
              blurRadius: 4.0,
            )
          ]),
          child: AppBar(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            centerTitle: true,
            leading: IconButton(
              icon: Icon(EvaIcons.arrowIosBackOutline, size: 25),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            title: Text('ayarlar'),
          ),
        ),
      ),
      body: ListView(
        padding: EdgeInsets.only(top: 5),
        shrinkWrap: true,
        controller: _scrollController,
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 5, left: 10, right: 10),
            child: Container(
              decoration: BoxDecoration(
                boxShadow: [BoxShadow(
                  color: Colors.black.withOpacity(0.05),
                  spreadRadius: 0.5,
                  blurRadius: 2.5
                )]
              ),
              child: Card(
                child: FlatButton(
                  padding: EdgeInsets.all(15),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => GeneralInfo())
                    );
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(EvaIcons.personOutline),
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Text('bilgiler'),
                          ),
                        ],
                      ),
                      Icon(EvaIcons.arrowIosForwardOutline)
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 5, left: 10, right: 10),
            child:  Container(
              decoration: BoxDecoration(
                boxShadow: [BoxShadow(
                  color: Colors.black.withOpacity(0.05),
                  spreadRadius: 0.5,
                  blurRadius: 2.5
                )]
              ),
              child: Card(
                child: FlatButton(
                  padding: EdgeInsets.all(15),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)
                  ),
                  onPressed: () {
                    print('Go settings');
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(EvaIcons.settings2Outline),
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Text('ayarlar'),
                          ),
                        ],
                      ),
                      Icon(EvaIcons.arrowIosForwardOutline)
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 5, left: 10, right: 10),
            child: Container(
              decoration: BoxDecoration(
                boxShadow: [BoxShadow(
                  color: Colors.black.withOpacity(0.05),
                  spreadRadius: 0.5,
                  blurRadius: 2.5
                )]
              ),
              child: Card(
                child: FlatButton(
                  padding: EdgeInsets.all(15),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  onPressed: () {
                    print('go kenar');
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(EvaIcons.briefcaseOutline),
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Text('kenar'),
                          ),
                        ],
                      ),
                      Icon(EvaIcons.arrowIosForwardOutline)
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 5, left: 10, right: 10),
            child: Container(
              decoration: BoxDecoration(
                  boxShadow: [BoxShadow(
                      color: Colors.black.withOpacity(0.05),
                      spreadRadius: 0.5,
                      blurRadius: 2.5
                  )]
              ),
              child: Card(
                child: FlatButton(
                  padding: EdgeInsets.all(15),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  onPressed: () {
                    Navigator.of(context).push(
                        PageTransition(
                            type: PageTransitionType.rightToLeftWithFade,
                            child: BlocProvider.value(
                              value: FollowersPageBloc(userId: int.parse(prefs.getString('uid')))..add(FetchFollowers()),
                              child: FollowersPage(),
                            )
                        )
                    );
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(EvaIcons.personAddOutline),
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Text('takip'),
                          ),
                        ],
                      ),
                      Icon(EvaIcons.arrowIosForwardOutline)
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 5, left: 10, right: 10),
            child: Container(
              decoration: BoxDecoration(
                  boxShadow: [BoxShadow(
                      color: Colors.black.withOpacity(0.05),
                      spreadRadius: 0.5,
                      blurRadius: 2.5
                  )]
              ),
              child: Card(
                child: FlatButton(
                  padding: EdgeInsets.all(15),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  onPressed: () {
                    Navigator.of(context).push(
                        PageTransition(
                            type: PageTransitionType.rightToLeftWithFade,
                            child: BlockedsPage()
                        )
                    );
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(EvaIcons.personDeleteOutline),
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Text('engel'),
                          ),
                        ],
                      ),
                      Icon(EvaIcons.arrowIosForwardOutline)
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 5, left: 10, right: 10),
            child: Container(
              decoration: BoxDecoration(
                  boxShadow: [BoxShadow(
                      color: Colors.black.withOpacity(0.05),
                      spreadRadius: 0.5,
                      blurRadius: 2.5
                  )]
              ),
              child: Card(
                child: FlatButton(
                  padding: EdgeInsets.all(15),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  onPressed: () {
                    print('change account');
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Stack(
                            children: <Widget>[
                              Icon(EvaIcons.personOutline),
                              Padding(
                                padding: const EdgeInsets.only(left: 12, bottom: 3),
                                child: Align(alignment: Alignment.topRight, child: Icon(EvaIcons.settings2Outline, size: 8)),
                              )
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Text('hesap değişikliği'),
                          ),
                        ],
                      ),
                      Icon(EvaIcons.arrowIosForwardOutline)
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 5, left: 10, right: 10),
            child: Container(
              decoration: BoxDecoration(
                  boxShadow: [BoxShadow(
                      color: Colors.black.withOpacity(0.05),
                      spreadRadius: 0.5,
                      blurRadius: 2.5
                  )]
              ),
              child: Card(
                child: FlatButton(
                  padding: EdgeInsets.all(15),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  onPressed: () {
                    print('eksi');
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(EvaIcons.dropletOutline),
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Text('ekşiden aktar'),
                          ),
                        ],
                      ),
                      Icon(EvaIcons.arrowIosForwardOutline)
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 5, left: 10, right: 10),
            child: FlatButton(
              padding: EdgeInsets.all(15),
              color: CustomTheme.of(context).cardColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
              ),
              onPressed: () {
                print('change account');
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Icon(EvaIcons.radioOutline),
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Text('radyo'),
                      ),
                    ],
                  ),
                  Icon(EvaIcons.arrowIosForwardOutline)
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 5, left: 10, right: 10),
            child: FlatButton(
              padding: EdgeInsets.all(15),
              color: CustomTheme.of(context).cardColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
              ),
              onPressed: () => BlocProvider.of<AuthenticationBloc>(context).add(LoggedOut()),
              child: Row(
                children: <Widget>[
                  Icon(EvaIcons.logOutOutline),
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Text('çıkış yap'),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}