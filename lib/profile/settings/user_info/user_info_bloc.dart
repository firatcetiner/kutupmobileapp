import 'package:kutupsozluk_app/profile/settings/user_info/user_info_repository.dart';

class UserInfoBloc {
  final _repository = UserInfoRepository();

  Future<dynamic> updateUserInfo({String oldPassword, String newPassword, String name, String city, String email, String message, String music}) async {
    return await _repository.updatePassword(oldPassword: oldPassword, newPassword: newPassword).then((_) async {
      return await _repository.updateGeneralInfo(name: name, city: city, email: email, message: message, music: music);
    });
  }

}