import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:kutupsozluk_app/profile/settings/user_info/user_info_bloc.dart';

class GeneralInfo extends StatefulWidget {

  GeneralInfo({Key key}) : super(key: key);
  @override
  _GeneralInfoState createState() => _GeneralInfoState();
}

class _GeneralInfoState extends State<GeneralInfo> {

  UserInfoBloc _bloc;
  ScrollController _scrollController;
  bool _isPasswordObscure = true;
  TextEditingController _nameController;
  TextEditingController _cityController;
  TextEditingController _oldPasswordController;
  TextEditingController _newPasswordController;
  TextEditingController _repeatNewPasswordController;
  TextEditingController _oldEmailController;
  TextEditingController _emailController;
  TextEditingController _repeatEmailController;

  @override
  void initState() {
    super.initState();
    _bloc = UserInfoBloc();
    _scrollController = ScrollController();
    _nameController = TextEditingController();
    _cityController = TextEditingController();
    _oldPasswordController = TextEditingController();
    _newPasswordController = TextEditingController();
    _repeatNewPasswordController = TextEditingController();
    _oldEmailController = TextEditingController();
    _emailController = TextEditingController();
    _repeatEmailController = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
    _nameController.dispose();
    _cityController.dispose();
    _oldPasswordController.dispose();
    _newPasswordController.dispose();
    _repeatNewPasswordController.dispose();
    _emailController.dispose();
    _repeatEmailController.dispose();
  }

  Future<dynamic> _updateProfileInfo() async {
    return await _bloc.updateUserInfo(
      oldPassword: _oldPasswordController.text,
      newPassword: _newPasswordController.text,
      name: _nameController.text,
      city: _cityController.text,
      email: _emailController.text,
      message: 'new message!!!!'
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(42),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.05),
              spreadRadius: 1,
              offset: Offset(0, 2.0),
              blurRadius: 4.0,
            )
          ]),
          child: AppBar(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            centerTitle: true,
            leading: IconButton(
              tooltip: 'geri',
              icon: Icon(EvaIcons.arrowIosBackOutline, size: 25),
              onPressed: () => Navigator.pop(context),
            ),
            actions: <Widget>[
              FlatButton(
                padding: EdgeInsets.only(right: 10),
                child: Text('kaydet'),
                onPressed: () {
                  if(_oldPasswordController.text.isNotEmpty && _newPasswordController.text.isEmpty && _repeatNewPasswordController.text.isNotEmpty) {
                    if((_emailController.text.isEmpty && _repeatEmailController.text.isEmpty && _oldEmailController.text.isEmpty)) {
                      print('nope');
                    }
                  }
                  else
                    _updateProfileInfo().then((res) {
                      if(res) Navigator.pop(context);
                      else print(res);
                    });
                },
              )
            ],
            title: Text('bilgiler'),
          ),
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        controller: _scrollController,
        padding: EdgeInsets.only(top: 5, bottom: 5, right: 5, left: 5),
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              boxShadow: [BoxShadow(
                color: Colors.black.withOpacity(0.05),
                spreadRadius: 0.5,
                blurRadius: 2.5
              )]
            ),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                          color: Colors.blue,
                          shape: BoxShape.circle
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Container(
              decoration: BoxDecoration(
                boxShadow: [BoxShadow(
                  color: Colors.black.withOpacity(0.05),
                  spreadRadius: 0.5,
                  blurRadius: 2.5
                )]
              ),
              child: Card(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Text('genel', style: TextStyle(fontSize: 15)),
                    ),
                    Divider(height: 20, thickness: 1.0),
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                      child: Row(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(EvaIcons.personOutline, size: 20),
                              Padding(
                                padding: const EdgeInsets.only(left: 10.0),
                                child: Text('isim'),
                              ),
                            ],
                          ),
                          Expanded(
                            child: TextFormField(
                              controller: _nameController,
                              textAlign: TextAlign.right,
                              decoration: InputDecoration.collapsed(
                                  hintStyle: TextStyle(fontSize: 14),
                                  hintText: 'isim'
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Divider(height: 20, indent: 40),
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                      child: Row(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(Icons.ac_unit, size: 20, color: Colors.transparent),
                              Padding(
                                padding: const EdgeInsets.only(left: 10.0),
                                child: Text('şehir'),
                              ),
                            ],
                          ),
                          Expanded(
                            child: TextFormField(
                              controller: _cityController,
                              textAlign: TextAlign.right,
                              decoration: InputDecoration.collapsed(
                                  hintStyle: TextStyle(fontSize: 14),
                                  hintText: 'şehriniz'
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Divider(height: 20, indent: 40),
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(Icons.ac_unit, size: 20, color: Colors.transparent),
                              Padding(
                                padding: const EdgeInsets.only(left: 10.0),
                                child: Text('doğum tarihi'),
                              ),
                            ],
                          ),
                          Align(alignment: Alignment.centerRight, child: Text('19/05/1996'))
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Container(
              decoration: BoxDecoration(
                boxShadow: [BoxShadow(
                  color: Colors.black.withOpacity(0.05),
                  spreadRadius: 0.5,
                  blurRadius: 2.5
                )]
              ),
              child: Card(
                child: Stack(
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 15.0),
                          child: Text('şifre & mail', style: TextStyle(fontSize: 15)),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Divider(height: 20, thickness: 1.0),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                          child: Row(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Icon(EvaIcons.lockOutline, size: 20),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Text('mevcut şifre'),
                                  ),
                                ],
                              ),
                              Expanded(
                                child: TextFormField(
                                  controller: _oldPasswordController,
                                  obscureText: _isPasswordObscure,
                                  textAlign: TextAlign.right,
                                  decoration: InputDecoration.collapsed(
                                      hintStyle: TextStyle(fontSize: 14),
                                      hintText: 'mevcut şifre'
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Divider(height: 20, indent: 40),
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                          child: Row(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Icon(Icons.ac_unit, size: 20, color: Colors.transparent),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Text('yeni şifre'),
                                  ),
                                ],
                              ),
                              Expanded(
                                child: TextFormField(
                                  controller: _newPasswordController,
                                  obscureText: _isPasswordObscure,
                                  textAlign: TextAlign.right,
                                  decoration: InputDecoration.collapsed(
                                      hintStyle: TextStyle(fontSize: 14),
                                      hintText: 'yeni şifre'
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Divider(height: 20, indent: 40),
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                          child: Row(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Icon(Icons.ac_unit, size: 20, color: Colors.transparent),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Text('tekrar yeni şifre'),
                                  ),
                                ],
                              ),
                              Expanded(
                                child: TextFormField(
                                  controller: _repeatNewPasswordController,
                                  obscureText: _isPasswordObscure,
                                  textAlign: TextAlign.right,
                                  decoration: InputDecoration.collapsed(
                                      hintStyle: TextStyle(fontSize: 14),
                                      hintText: 'tekrar yeni şifre'
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Divider(height: 20, thickness: 0.6),
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                          child: Row(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Icon(EvaIcons.emailOutline, size: 20),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Text('mevcut mail'),
                                  ),
                                ],
                              ),
                              Expanded(
                                child: TextFormField(
                                  controller: _emailController,
                                  textAlign: TextAlign.right,
                                  decoration: InputDecoration.collapsed(
                                      hintStyle: TextStyle(fontSize: 14),
                                      hintText: 'mevcut mail'
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Divider(height: 20, indent: 40),
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                          child: Row(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Icon(EvaIcons.emailOutline, size: 20, color: Colors.transparent),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Text('yeni mail'),
                                  ),
                                ],
                              ),
                              Expanded(
                                child: TextFormField(
                                  controller: _repeatEmailController,
                                  textAlign: TextAlign.right,
                                  decoration: InputDecoration.collapsed(
                                      hintStyle: TextStyle(fontSize: 15),
                                      hintText: 'yeni mail'
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Divider(height: 20, indent: 40),
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
                          child: Row(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Icon(EvaIcons.emailOutline, size: 20, color: Colors.transparent),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Text('tekrar yeni mail'),
                                  ),
                                ],
                              ),
                              Expanded(
                                child: TextFormField(
                                  textAlign: TextAlign.right,
                                  decoration: InputDecoration.collapsed(
                                      hintStyle: TextStyle(fontSize: 15),
                                      hintText: 'tekrar yeni mail'
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    Align(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        onPressed: () {
                          setState(() {
                            _isPasswordObscure = !_isPasswordObscure;
                          });
                        },
                        icon: Icon(EvaIcons.eyeOutline),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}