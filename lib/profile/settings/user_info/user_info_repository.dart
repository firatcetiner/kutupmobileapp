import 'package:kutupsozluk_app/main.dart';
import 'package:dio/dio.dart' show Options;
import 'package:dio/dio.dart' show DioError;
import 'package:dio/dio.dart' show DioErrorType;

class UserInfoRepository {

  Future<dynamic> updateGeneralInfo(
      {String name, String city, String email, String message,
        String music}) async {
    var url = '$baseUrl/api/user/update/general-infos';
    var token = await prefs.get('token');

    var response = await client.put(
      url,
      data: {
        'name': '$name',
        'city': '$city',
        'email': '$email',
        'message': '$message',
        'music': '$music'
      },
      options: Options(
        headers: {
          'accept': 'application/json',
          'Content-Type': 'application/json-patch+json',
          'Authorization': 'Bearer $token'
        },
      ),
    ).catchError((error) => print(error));
    if(response.statusCode == 200 && response.statusCode == 304)
      return true;
    else
      return response.data['error'].toString();
  }

  Future<dynamic> getUserInfo(int userId) async {
    return await client.get('$baseUrl/api/user/$userId/info').then((response) {
      if(response.statusCode == 200 || response.statusCode == 304)
        return response.data['model'];
      else
        throw DioError(response: response, type: DioErrorType.RESPONSE);
    }).catchError((error) => throw error);
  }

  Future<dynamic> updatePassword({String oldPassword, String newPassword}) async {
    var url = '$baseUrl/api/user/update/password';
    var token = await prefs.get('token');
    var body = {
      'oldPassword': oldPassword,
      'newPassword': newPassword,
    };
    return await client.put(
        url,
        data: body,
        options: Options(
          headers: {
            'accept': 'application/json',
            'Content-Type': 'application/json-patch+json',
            'Authorization': 'Bearer $token'
          },
        )
    ).then((response) {
      if(response.statusCode == 200 && response.statusCode == 304)
        return true;
      else
        return response.data['error'].toString();
    }).catchError((error) => print(error));
  }
}