import 'package:kutupsozluk_app/profile/settings/blockeds/repository.dart';
import 'package:kutupsozluk_app/profile/settings/followers/follower_model.dart';
import 'package:kutupsozluk_app/profile/settings/followers/models/small_follower_model.dart';
import 'package:rxdart/rxdart.dart';

class BlockedsBloc {

  final BlockedsRepository _repository = BlockedsRepository();

  PublishSubject<List<FollowerModel>> _blockedsController = PublishSubject<List<FollowerModel>>();

  Stream<List<FollowerModel>> get blockedList => _blockedsController.stream;

  List<FollowerModel> allFollowers = List<FollowerModel>();

  int _userId;

  BlockedsBloc({int userId}) {
    this._userId = userId;
    getBlockedsAndAdd();
  }

  Future<void> getBlockedsAndAdd() async {
    allFollowers.clear();
    return await _repository.getBlockeds(_userId).then((response) {
      var initialFollowers = response.map<SmallFollowerModel>((json) => SmallFollowerModel.fromJson(json)).toList();
      for(SmallFollowerModel f in initialFollowers) {
        _repository.getUserInfo(f.targetUserId).then((info) {
          var follower = FollowerModel.fromJson(info);
          follower.id = f.targetUserId;
          allFollowers.add(follower);
        });
      }
      _blockedsController.sink.add(allFollowers);
    }).catchError((error) => _blockedsController.addError(error));
  }

  Future<bool> removeBlocked(int userId) async {
    return await _repository.removeBlocked(userId);
  }


  void dispose() {
    _blockedsController.close();
  }
}
