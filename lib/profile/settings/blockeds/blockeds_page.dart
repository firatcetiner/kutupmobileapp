import 'package:cached_network_image/cached_network_image.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kutupsozluk_app/main.dart';
import 'package:kutupsozluk_app/profile/settings/followers/follower_model.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';

import 'bloc.dart';

class BlockedsPage extends StatefulWidget {

  @override
  _BlockedsPageState createState() => _BlockedsPageState();

}

class _BlockedsPageState extends State<BlockedsPage> with SingleTickerProviderStateMixin, AutomaticKeepAliveClientMixin {

  BlockedsBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = BlockedsBloc(userId: int.parse(prefs.getString('uid')));
  }

  @override
  void dispose() {
    super.dispose();
    _bloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final _color = CustomTheme.of(context).indicatorColor;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(48),
        child: Container(
          color: CustomTheme.of(context).appBarTheme.color,
          child: Column(
            children: <Widget>[
              IconButton(
                icon: Icon(EvaIcons.arrowIosBackOutline),
                onPressed: () => Navigator.pop(context),
              ),
              Divider(height: 0, thickness: 1)
            ],
          ),
        ),
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          await _bloc.getBlockedsAndAdd();
        },
        child: StreamBuilder<List<FollowerModel>>(
            stream: _bloc.blockedList,
            builder: (context, snapshot) {
              if(snapshot.hasError) {
                print(snapshot.error);
                return Text('error');
              }
              if(!snapshot.hasData) return Text('waiting');
              var items = snapshot.data;
              return ListView.separated(
                separatorBuilder: (context, index) => Divider(height: 0, thickness: 1.0),
                physics: AlwaysScrollableScrollPhysics(),
                itemCount: snapshot.data.length,
                itemBuilder: (context, index) {
                  var data = items[index];
                  return Dismissible(
                    direction: DismissDirection.startToEnd,
                    background: Container(
                      alignment: Alignment.centerLeft,
                      color: Colors.green,
                      child: Text('engeli kaldır'),
                    ),
                    key: Key(items[index].nick),

                    onDismissed: (direction) async {
                      await _bloc.removeBlocked(items[index].id).then((res) {
                        if(res)
                          setState(() {
                            items.removeAt(index);
                          });
                      });
                    },
                    child: Container(
                      color: CustomTheme.of(context).cardColor,
                      padding: EdgeInsets.all(10),
                      child: Stack(
                        children: <Widget>[
                          Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  CachedNetworkImage(
                                    width: 50,
                                    height: 50,
                                    imageUrl: data.image,
                                    placeholder: (context, url) {
                                      return Container(
                                        width: 50,
                                        height: 50,
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                        ),
                                        child: Text(data.nick[0].toUpperCase()),
                                      );
                                    },
                                    imageBuilder: (context, image) {
                                      return Container(
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          image: DecorationImage(image: image)
                                        ),
                                      );
                                    },
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(data.nick, style: TextStyle(color: _color, fontWeight: FontWeight.w500)),
                                        Text('${data.generation}. nesil yazar (${data.registerDate.split(" ")[0]})'),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              data.message.isNotEmpty ? Text(data.message) : Container(height: 0)
                            ],
                          ),
                          Icon(EvaIcons.personRemoveOutline)
                        ],
                      ),
                    ),
                  );
                },
              );
            }
        ),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

}