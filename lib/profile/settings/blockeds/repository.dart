import 'package:kutupsozluk_app/main.dart';
import 'package:dio/dio.dart' show DioError;
import 'package:dio/dio.dart' show DioErrorType;
import 'package:dio/dio.dart' show Options;

class BlockedsRepository {

  /* get blocked users userId and the unique_name */
  Future<dynamic> getBlockeds(int userId) async {
    return await client.get('$baseUrl/api/user/$userId/blockeds').then((response) {
      if(response.statusCode == 200 || response.statusCode== 304)
        return response.data['model'];
      else
        throw DioError(response: response, type: DioErrorType.RESPONSE);
    }).catchError((error) => throw error);
  }

  Future<dynamic> getUserInfo(int userId) async {
    return await client.get('$baseUrl/api/user/$userId/info').then((response) {
      if(response.statusCode == 200 || response.statusCode == 304)
        return response.data['model'];
      else
        throw DioError(response: response, type: DioErrorType.RESPONSE);
    }).catchError((error) => throw error);
  }

  Future<bool> removeBlocked(int userId) async {
    var token = prefs.getString('token');
    return await client.delete(
      '$baseUrl/api/user/remove-blocked/$userId',
      options: Options(
        headers: {
          'accept': 'application/json',
          'Authorization': 'Bearer $token'
        }
      )
    ).then((response) {
      if(response.statusCode == 200 || response.statusCode == 304)
        return true;
      else
        return false;
    }).catchError((error) => print(error));
  }
}