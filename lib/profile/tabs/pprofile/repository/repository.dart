/* get top rated entries of user with userId and the voteType */
import 'package:kutupsozluk_app/data/model/entry_model.dart';
import 'package:kutupsozluk_app/main.dart';
import 'package:dio/dio.dart' show DioError, DioErrorType;
import 'package:kutupsozluk_app/profile/settings/followers/models/user_info_model.dart';

Future<List<Entry>> getTopRatedEntries(List<int> props) async {
  return await client.get('$baseUrl/api/user/${props[0]}/top-rated?voteType=${props[1]}').then((response) {
    if(response.statusCode == 200 || response.statusCode == 304)
      return response.data['model'].map<Entry>((json) => Entry.fromJson(json)).toList();
    else
      throw DioError(response: response, type: DioErrorType.RESPONSE);
  }).catchError((error) => throw error);
}

Future<UserInfoModel> getUserInfo(int userId) async {
  return await client.get('$baseUrl/api/user/$userId/info').then((response) {
    if(response.statusCode == 200 || response.statusCode == 304)
      return UserInfoModel.fromJson(response.data['model']);
    else
      throw DioError(response: response, type: DioErrorType.RESPONSE);
  }).catchError((error) => throw error);
}