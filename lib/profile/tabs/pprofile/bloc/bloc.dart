import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/profile/tabs/pprofile/bloc/event.dart';
import 'package:kutupsozluk_app/profile/tabs/pprofile/bloc/state.dart';
import 'package:kutupsozluk_app/profile/tabs/pprofile/repository/repository.dart';
import 'package:meta/meta.dart';

class ProfileMainBloc extends Bloc<ProfileMainEvent, ProfileMainState> {

  final int userId;

  ProfileMainBloc({@required this.userId});

  @override
  ProfileMainState get initialState => Loading();

  @override
  Stream<ProfileMainState> mapEventToState(ProfileMainEvent event) async* {
    if(event is FetchInfo) {
      yield* _mapFetchToState(event);
    }
  }

  Stream<ProfileMainState> _mapFetchToState(FetchInfo event) async* {
    try {
      final props1 = [userId, 1];
      final mostLikedEntries = await compute(getTopRatedEntries, props1);
      final props2 = [userId, 2];
      final mostUnLikedEntries = await compute(getTopRatedEntries, props2);
      final userInfoModel = await compute(getUserInfo, userId);
      yield Loaded(mostLikedEntries: mostLikedEntries, mostUnLikedEntries: mostUnLikedEntries, userInfo: userInfoModel);
    } catch(_) {
      yield Failure();
    }
  }

}