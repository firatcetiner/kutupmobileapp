import 'package:equatable/equatable.dart';
import 'package:kutupsozluk_app/data/model/entry_model.dart';
import 'package:kutupsozluk_app/profile/settings/followers/models/user_info_model.dart';
import 'package:meta/meta.dart';

class ProfileMainState extends Equatable {
  @override
  List get props => [];
}

class Loaded extends ProfileMainState {
  final List<Entry> mostLikedEntries;
  final List<Entry> mostUnLikedEntries;
  final UserInfoModel userInfo;

  Loaded({@required this.mostLikedEntries, @required this.mostUnLikedEntries, @required this.userInfo});

  @override
  List get props => [mostLikedEntries, mostUnLikedEntries, userInfo];

}

class Failure extends ProfileMainState {}

class Loading extends ProfileMainState {}