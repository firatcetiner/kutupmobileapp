import 'package:equatable/equatable.dart';

class ProfileMainEvent extends Equatable {
  const ProfileMainEvent();
  @override
  List get props => [];
}

class FetchInfo extends ProfileMainEvent {}
