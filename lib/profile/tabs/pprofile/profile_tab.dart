import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kutupsozluk_app/profile/tabs/pprofile/bloc/bloc.dart';
import 'package:kutupsozluk_app/profile/tabs/pprofile/bloc/state.dart';
import 'package:kutupsozluk_app/style/custom_colors.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';
class ProfileTab extends StatefulWidget {
  final int _userId;

  ProfileTab({Key key, @required int userId})
      : assert(userId != null),
        _userId = userId,
        super(key: key);

  _ProfileTabState createState() => _ProfileTabState();

}

class _ProfileTabState extends State<ProfileTab> with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {

  int get _userId => widget._userId;
  ProfileMainBloc _bloc;
  AnimationController _rotateController;

  @override
  void initState() {
    super.initState();
    _rotateController = AnimationController(vsync: this, duration: Duration(seconds: 5));
    _rotateController.repeat();
    _bloc = BlocProvider.of<ProfileMainBloc>(context);
  }
  @override
  void dispose() {
    _rotateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<ProfileMainBloc, ProfileMainState>(
      builder: (context, state) {
        if(state is Loaded) {
          return ListView(
            shrinkWrap: true,
            padding: EdgeInsets.symmetric(vertical: 5),
            children: <Widget>[
              Container(
                height: 300,
                margin: EdgeInsets.symmetric(horizontal: 5),
                decoration: BoxDecoration(
                  boxShadow: [BoxShadow(
                    color: Colors.black12.withOpacity(0.05),
                    spreadRadius: 1,
                    blurRadius: 5
                  )]
                ),
                child: Card(
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: Text('istatistikleri'),
                      ),
                      Divider(thickness: 1, height: 0),
                      Expanded(
                        child: SingleChildScrollView(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 5),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            height: 40,
                                            width: 40,
                                            decoration: BoxDecoration(
                                              //color: CustomColors.loginGradientStart.withOpacity(0.3),
                                              shape: BoxShape.circle,
                                            ),
                                            child: Icon(FontAwesomeIcons.snowflake, color: CustomColors.loginGradientStart),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(left: 15),
                                            child: Text('karlı oy'),
                                          )
                                        ],
                                      ),
                                      Text(state.userInfo.plus.toString())
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            height: 40,
                                            width: 40,
                                            decoration: BoxDecoration(
                                              //: Colors.red.withOpacity(0.3),
                                              shape: BoxShape.circle,
                                            ),
                                            child: Icon(FontAwesomeIcons.fire, color: Colors.red),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(left: 15),
                                            child: Text('ateşli oy'),
                                          )
                                        ],
                                      ),
                                      Text(state.userInfo.minus.toString())
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            height: 40,
                                            width: 40,
                                            decoration: BoxDecoration(
                                              //color: Colors.green.withOpacity(0.3),
                                              shape: BoxShape.circle,
                                            ),
                                            child: Icon(EvaIcons.thermometer, color: Colors.green),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(left: 15),
                                            child: Text('ılık oy'),
                                          )
                                        ],
                                      ),
                                      Text(state.userInfo.warm.toString())
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            height: 40,
                                            width: 40,
                                            decoration: BoxDecoration(
                                              //color: Colors.red.withOpacity(0.3),
                                              shape: BoxShape.circle,
                                            ),
                                            child: Icon(EvaIcons.heart, color: Colors.red),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(left: 15),
                                            child: Text('favoriler'),
                                          )
                                        ],
                                      ),
                                      Text(state.userInfo.favourite.toString())
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            height: 40,
                                            width: 40,
                                            decoration: BoxDecoration(
                                              //color: Colors.grey[800],
                                              shape: BoxShape.circle,
                                            ),
                                            child: Icon(EvaIcons.personAddOutline),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(left: 15),
                                            child: Text('takipçi'),
                                          )
                                        ],
                                      ),
                                      Text(state.userInfo.followerCount.toString())
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            height: 40,
                                            width: 40,
                                            decoration: BoxDecoration(
                                              //color: Colors.grey[800],
                                              shape: BoxShape.circle,
                                            ),
                                            child: Icon(EvaIcons.personAddOutline),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(left: 15),
                                            child: Text('takipçi'),
                                          )
                                        ],
                                      ),
                                      Text(state.userInfo.followerCount.toString())
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            height: 40,
                                            width: 40,
                                            decoration: BoxDecoration(
                                              color: Colors.grey[300],
                                              shape: BoxShape.circle,
                                            ),
                                            child: Icon(EvaIcons.personAddOutline),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(left: 15),
                                            child: Text('takipçi'),
                                          )
                                        ],
                                      ),
                                      Text(state.userInfo.followerCount.toString())
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          );
        }
        return AnimatedBuilder(
          animation: _rotateController,
          child: SizedBox(
            width: 30,
            height: 30,
            child: Image.asset('images/snowflake-solid.png', color: CustomTheme.of(context).indicatorColor),
          ),
          builder: (context, child) {
            return Transform.rotate(
              angle: _rotateController.value * 360,
              child: child,
            );
          },
        );
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}