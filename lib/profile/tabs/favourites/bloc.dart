import 'package:kutupsozluk_app/data/model/entry_model.dart';
import 'package:kutupsozluk_app/profile/tabs/favourites/repository.dart';
import 'package:rxdart/rxdart.dart';

class FEBloc {
  final ceController = PublishSubject<List<Entry>>();
  final FERepository _repository = FERepository();

  List<Entry> allEntries = List<Entry>();

  Stream<List<Entry>> get entryStream => ceController.stream;

  int _userid;
  int _currentPage = 1;
  bool loading = false;

  FEBloc(int userid) {
    this._userid = userid;
    getFavoriteEntries();
  }

  Future<void> getFavoriteEntries() async {
    return await _repository.fetchFavouriteEntries(_userid, 1).then((response) {
      allEntries = response.map<Entry>((json) => Entry.fromJson(json)).toList();
      ceController.sink.add(allEntries);
    }).catchError((error) => ceController.addError(error));
  }

  Future<void> getNewFavoriteEntriesAndAdd() async {
    loading = true;
    _currentPage++;
    return await _repository.fetchFavouriteEntries(_userid, _currentPage).then((response) {
      final entries = response.map<Entry>((json) => Entry.fromJson(json)).toList();
      entries.forEach((title) {
        allEntries.add(title);
      });
      ceController.sink.add(allEntries);
    }).catchError((error) => ceController.addError(error)).whenComplete(() => loading = false);
  }

  void dispose() {
    ceController.close();
    allEntries.clear();
  }
}