import 'package:dio/dio.dart' show DioErrorType;
import 'package:dio/dio.dart' show DioError;
import 'package:kutupsozluk_app/main.dart';

class FERepository {

    /* get favorite entries for user with userId, with page number. */
  Future<dynamic> fetchFavouriteEntries(int userId, int page) async {
    final response = await client.get('$baseUrl/api/user/$userId/favourite-entries?page=$page');
    if(response.statusCode == 200 || response.statusCode == 304)
      return response.data['model'];
    else
      throw DioError(response: response, type: DioErrorType.RESPONSE);
  }
}