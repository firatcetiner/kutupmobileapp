import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kutupsozluk_app/data/model/entry_model.dart';
import 'package:kutupsozluk_app/profile/entry_card/profile_entry_card.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';
import 'bloc.dart';

class FavoriteEntriesTab extends StatefulWidget {
  final int _userId;

  FavoriteEntriesTab({Key key, @required int userId})
      :  assert(userId != null),
        _userId = userId,
        super(key: key);

  _FavoriteEntriesTabState createState() => _FavoriteEntriesTabState();

}

class _FavoriteEntriesTabState extends State<FavoriteEntriesTab> with AutomaticKeepAliveClientMixin {
  int get _userId => widget._userId;
  FEBloc _bloc;
  ScrollController _scrollController;

  @override
  void initState() {
    _scrollController = ScrollController();
    _scrollController.addListener(_fetchNext);
    _bloc = FEBloc(_userId);
    super.initState();
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  void _fetchNext() {
    if ((_scrollController.position.pixels == _scrollController.position.maxScrollExtent)
        && _bloc.loading == false) {
      _bloc.getNewFavoriteEntriesAndAdd();
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return RefreshIndicator(
      color: Colors.white,
      backgroundColor: CustomTheme.of(context).indicatorColor,
      onRefresh: () async => await _bloc.getNewFavoriteEntriesAndAdd(),
      child: StreamBuilder<List<Entry>>(
        stream: _bloc.ceController,
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(
                child: SizedBox(width: 20,
                  height: 20,
                  child: CircularProgressIndicator(strokeWidth: 2.0)),
              );
            default: if (snapshot.hasError) return Text('entryler getirilemedi.');
            return ListView.separated(
              padding: EdgeInsets.only(top: 10),
              separatorBuilder: (context, index) => SizedBox(height: 10),
              itemCount: snapshot.data.length + 1,
              itemBuilder: (context, index) {
                if(index == snapshot.data.length) 
                  return Center(
                    child: Padding(
                      padding: EdgeInsets.all(8),
                      child: SizedBox(height: 10, width: 10, child: CircularProgressIndicator(strokeWidth: 2.0))
                    ),
                  );
                return EntryCard(entry: snapshot.data[index]);
              },
            );
          }
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}