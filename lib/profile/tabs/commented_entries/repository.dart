import 'package:kutupsozluk_app/main.dart';
import 'package:dio/dio.dart' show DioError;
import 'package:dio/dio.dart' show DioErrorType;


class CERepository {
  Future<dynamic> fetchCommentedEntries(int userId, int page) async {
    final response = await client.get('$baseUrl/api/user/$userId/include-comment-entries?page=$page');
    if(response.statusCode == 200 || response.statusCode == 304)
      return response.data['model'];
    else
      throw DioError(response: response, type: DioErrorType.RESPONSE);
  }
}