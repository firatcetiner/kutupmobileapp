import 'package:kutupsozluk_app/data/model/entry_model.dart';
import 'package:kutupsozluk_app/profile/tabs/commented_entries/repository.dart';
import 'package:rxdart/rxdart.dart';

class CEBloc {
  final ceController = PublishSubject<List<Entry>>();
  final CERepository _repository = CERepository();

  List<Entry> allEntries = List<Entry>();

  Stream<List<Entry>> get entryStream => ceController.stream;

  int _userid;
  int _currentPage = 1;
  bool loading = false;

  CEBloc(int userid) {
    this._userid = userid;
    getCommentedEntries();
  }

  Future<void> getCommentedEntries() async {
    return await _repository.fetchCommentedEntries(_userid, 1).then((response) {
      allEntries = response.map<Entry>((json) => Entry.fromJson(json)).toList();
      ceController.sink.add(allEntries);
    }).catchError((error) => ceController.sink.addError(error));
  }

  Future<void> getNewCommentedEntriesAndAdd() async {
    loading = true;
    _currentPage++;
    return await _repository.fetchCommentedEntries(_userid, _currentPage).then((response) {
      final entries = response.map<Entry>((json) => Entry.fromJson(json)).toList();
      entries.forEach((title) {
        allEntries.add(title);
      });
      ceController.sink.add(allEntries);
    }).catchError((error) => ceController.addError(error)).whenComplete(() => loading = false);
  }

  void dispose() {
    ceController.close();
    allEntries.clear();
  }
}