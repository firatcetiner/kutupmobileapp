import 'package:dio/dio.dart' show DioErrorType;
import 'package:dio/dio.dart' show DioError;
import 'package:dio/dio.dart' show Options;
import 'package:kutupsozluk_app/main.dart';

class LERepository {
    /* get last entries of user with userId, with page number. */
  Future<dynamic> fetchLastEntries(int userId, int page) async {
    final response =  await client.get(
      '$baseUrl/api/user/$userId/last-entries?page=$page',
      options: Options(
        headers: {
          'accept': 'application/json'
        }
      )
    );
    if(response.statusCode == 200 || response.statusCode == 304)
      return response.data['model'];
    else
      throw DioError(response: response, type: DioErrorType.RESPONSE);
  }
}