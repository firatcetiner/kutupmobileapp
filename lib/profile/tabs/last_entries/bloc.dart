import 'package:kutupsozluk_app/data/model/entry_model.dart';
import 'package:kutupsozluk_app/profile/tabs/last_entries/repository.dart';
import 'package:rxdart/rxdart.dart';

class LEBloc {
  final ceController = PublishSubject<List<Entry>>();
  final LERepository _repository = LERepository();

  List<Entry> allEntries = List<Entry>();

  Stream<List<Entry>> get entryStream => ceController.stream;

  int _userid;
  int _currentPage = 1;
  bool loading = false;

  LEBloc(int userid) {
    this._userid = userid;
    getLastEntries();
  }

  Future<void> getLastEntries() async {
    return await _repository.fetchLastEntries(_userid, 1).then((response) {
      allEntries = response.map<Entry>((json) => Entry.fromJson(json)).toList();
      ceController.sink.add(allEntries);
    }).catchError((error) => ceController.addError(error));
  }

  Future<void> getNewLastEntriesAndAdd() async {
    loading = true;
    _currentPage++;
    return await _repository.fetchLastEntries(_userid, _currentPage).then((response) {
      final entries = response.map<Entry>((json) => Entry.fromJson(json)).toList();
      entries.forEach((title) {
        allEntries.add(title);
      });
      ceController.sink.add(allEntries);
    }).catchError((error) => ceController.addError(error)).whenComplete(() => loading = false);
  }

  void dispose() {
    ceController.close();
    allEntries.clear();
  }
}