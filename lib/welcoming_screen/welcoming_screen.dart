import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/login/login.dart';
import 'package:kutupsozluk_app/profile/user_repository.dart';
import 'package:kutupsozluk_app/register/register.dart';
import 'package:kutupsozluk_app/teddy/teddy_controller.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';
import 'package:kutupsozluk_app/welcoming_screen/bloc/welcome_screen_bloc.dart';
import 'package:kutupsozluk_app/welcoming_screen/bloc/welcome_screen_state.dart';
import 'bloc/welcome_screen_event.dart';

AnimationController animationController;

class WelcomingScreen extends StatefulWidget {

  final UserRepository userRepository;

  const WelcomingScreen({Key key, @required this.userRepository}) : super(key: key);
  @override
  _WelcomingScreenState createState() => _WelcomingScreenState();
}

class _WelcomingScreenState extends State<WelcomingScreen> with TickerProviderStateMixin {

  UserRepository get _userRepository => widget.userRepository;
  TeddyController _teddyController;


  WelcomingScreenBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of<WelcomingScreenBloc>(context);
    _teddyController = TeddyController();
    animationController = AnimationController(
      vsync: this,
      value: 1,
      upperBound: 1,
      lowerBound: 0,
      duration: Duration(milliseconds: 300),
      reverseDuration: Duration(milliseconds: 300)
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomTheme.of(context).indicatorColor.withOpacity(0.2),
      body: BlocBuilder<WelcomingScreenBloc, WelcomingScreenState>(
        builder: (context, state) {
          return AnimatedBuilder(
            animation: animationController,
            child: AnimatedPadding(
              duration: Duration(milliseconds: 300),
              padding: EdgeInsets.only(top: state.props[0]),
              child: Column(
                children: <Widget>[
                  Container(
                      height: 150,
                      padding: const EdgeInsets.only(left: 30.0, right:30.0),
                      child: FlareActor(
                        "images/Teddy.flr",
                        shouldClip: false,
                        alignment: Alignment.topCenter,
                        fit: BoxFit.contain,
                        controller: _teddyController,
                      )
                  ),
                  Expanded(
                    child: AnimatedContainer(
                        duration: Duration(milliseconds: 300),
                        alignment: Alignment.center,
                        //margin: EdgeInsets.only(top: state.props[0]),
                        decoration: BoxDecoration(
                            color: CustomTheme.of(context).cardColor,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(state.props[1]),
                                topRight: Radius.circular(state.props[1])
                            )
                        ),
                        child: BlocBuilder<WelcomingScreenBloc, WelcomingScreenState>(
                          builder: (context, state) {
                            if(state is LoginOpened) {
                              return LoginScreen(userRepository: _userRepository, bloc: _bloc, teddyController: _teddyController);
                            }
                            if(state is RegisterOpened) {
                              return RegisterScreen(userRepository: _userRepository, bloc: _bloc);
                            }
                            return Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                SingleChildScrollView(
                                  physics: NeverScrollableScrollPhysics(),
                                  child: Column(
                                    children: <Widget>[
                                      GestureDetector(
                                        onTap: () {
                                          animationController.reverse().whenComplete(() {
                                            _bloc..add(OpenRegister(margin: 220, borderRadius: 30));
                                            animationController.forward();
                                          });
                                        },
                                        child: Container(
                                          height: 55,
                                          alignment: Alignment.center,
                                          margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                                          decoration: BoxDecoration(
                                              color: CustomTheme.of(context).indicatorColor,
                                              borderRadius: BorderRadius.circular(40)
                                          ),
                                          child: Text('kayıt ol', style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 22)),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          animationController.reverse().whenComplete(() {
                                            _bloc..add(OpenLogin(margin: 120, borderRadius: 30));
                                            animationController.forward();
                                          });
                                        },
                                        child: Container(
                                          height: 55,
                                          alignment: Alignment.center,
                                          margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(40),
                                            border: Border.all(width: 2, color: CustomTheme.of(context).textTheme.title.color)
                                          ),
                                          child: Text('giriş', style: TextStyle(color: CustomTheme.of(context).textTheme.title.color, fontWeight: FontWeight.w500, fontSize: 22)),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            );
                          },
                        )
                    ),
                  ),
                ],
              ),
            ),
            builder: (context, child) {
              return SlideTransition(
                position: Tween<Offset>(
                    begin: Offset(0, 1),
                    end: Offset(0,0)
                ).animate(CurvedAnimation(
                    curve: Curves.decelerate,
                    parent: animationController
                )),
                child: child,
              );
            },
          );
        },
      )
    );
  }
}
