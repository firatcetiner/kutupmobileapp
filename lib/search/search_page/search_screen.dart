import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/search/search_page/bloc.dart';
import 'package:kutupsozluk_app/search/search_page/search_page.dart';

class SearchScreen extends StatefulWidget {

  @override
  _SearchScreenState createState() => _SearchScreenState();
}


class _SearchScreenState extends State<SearchScreen> with AutomaticKeepAliveClientMixin<SearchScreen> {

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocProvider.value(
      value: SearchBloc(),
      child: SearchPage(),
    );
  }

  @override
  bool get wantKeepAlive => true;
}