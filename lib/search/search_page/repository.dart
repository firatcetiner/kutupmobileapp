import 'package:kutupsozluk_app/main.dart';
import 'package:dio/dio.dart' show DioError;
import 'package:dio/dio.dart' show DioErrorType;


class SearchRepository {
  Future<dynamic> fetchTitlesByWord(String word) async {
    final response = await client.get('$baseUrl/api/title/contains?word=$word');
    if(response.statusCode == 200) {
      return response.data['model'];
    } else {
      throw DioError(response: response, type: DioErrorType.RESPONSE);
    }
  }
}
