import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/custom_transitions.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/title_view/entry_list_page.dart';
import 'package:kutupsozluk_app/search/category/category_screen.dart';
import 'package:kutupsozluk_app/search/new_title/new_title_page.dart';
import 'package:kutupsozluk_app/search/search_page/bloc.dart';
import 'package:kutupsozluk_app/search/search_page/search_event.dart';
import 'package:kutupsozluk_app/search/search_page/search_state.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';


// ignore: must_be_immutable
class SearchPage extends StatelessWidget {
  TextEditingController _controller = TextEditingController();
  ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.05),
              spreadRadius: 1,
              offset: Offset(0, 2.0),
              blurRadius: 4.0,
            )
          ]),
          child: AppBar(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            titleSpacing: 0,
            automaticallyImplyLeading: false,
            title: Container(
              height: 62,
              child: Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: CustomTheme.of(context).indicatorColor.withOpacity(0.08),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: TextFormField(
                  onChanged: (value) {
                    if(value.isNotEmpty)
                      BlocProvider.of<SearchBloc>(context)..add(SearchTitles(word: value));
                    else BlocProvider.of<SearchBloc>(context)..add(Idle());
                  },
                  textCapitalization: TextCapitalization.none,
                  controller: _controller,
                  decoration: InputDecoration(
                      alignLabelWithHint: true,
                      contentPadding: EdgeInsets.only(top: 9),
                      suffixIcon: IconButton(
                        padding: EdgeInsets.only(right: 10.0),
                        icon: Icon(EvaIcons.funnelOutline),
                        onPressed: () {
                          print('advanced search!');
                        },
                      ),
                      fillColor: CustomTheme.of(context).indicatorColor,
                      focusColor: CustomTheme.of(context).indicatorColor,
                      border: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.transparent)
                      ),
                      enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.transparent)
                      ),
                      focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.transparent)),
                      prefixIcon: Icon(EvaIcons.searchOutline),
                      hintText: 'başlık, #entry veya @yazar'
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
      body: BlocBuilder<SearchBloc, SearchState>(
        builder: (context, state) {
          if(state is Empty) {
            return Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                  child: Text('kategoriler', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600)),
                ),
                Expanded(
                  child: ListView(
                    padding: EdgeInsets.zero,
                    shrinkWrap: true,
                    children: <Widget>[
                      FlatButton(
                        padding: EdgeInsets.only(top: 20),
                        onPressed: () {
                          Navigator.push(
                              context,
                              PageTransition(
                                  duration: Duration(milliseconds: 300),
                                  type: PageTransitionType.downToUp,
                                  curve: Curves.decelerate,
                                  child: CategoryScreen(title: 'gündem-tespit', categoryId: 'k14',)
                              )
                          );
                        },
                        child: Text('#gündem-tespit'),
                      ),
                      FlatButton(
                          padding: EdgeInsets.only(top: 30),
                          onPressed: () {
                            Navigator.push(
                                context,
                                PageTransition(
                                    duration: Duration(milliseconds: 300),
                                    type: PageTransitionType.downToUp,
                                    curve: Curves.decelerate,
                                    child: CategoryScreen(title: 'anket', categoryId: 'k20')
                                )
                            );
                          },
                          child: Text('#anket')
                      ),
                      FlatButton(
                          padding: EdgeInsets.only(top: 30),
                          onPressed: () {
                            Navigator.push(
                                context,
                                PageTransition(
                                    duration: Duration(milliseconds: 300),
                                    type: PageTransitionType.downToUp,
                                    curve: Curves.decelerate,
                                    child: CategoryScreen(title: 'tv-sinema-magazin', categoryId: 'k10')
                                )
                            );
                          },
                          child: Text('#tv-sinema-magazin')
                      ),
                      FlatButton(
                          padding: EdgeInsets.only(top: 30),
                          onPressed: () {
                            Navigator.push(
                                context,
                                PageTransition(
                                    duration: Duration(milliseconds: 300),
                                    type: PageTransitionType.downToUp,
                                    curve: Curves.decelerate,
                                    child: CategoryScreen(title: 'gündem-tespit', categoryId: 'k21',)
                                )
                            );
                          },
                          child: Text('#siyaset-ekonomi-hukuk')
                      ),
                      FlatButton(
                          padding: EdgeInsets.only(top: 30),
                          onPressed: () {
                            Navigator.push(
                                context,
                                PageTransition(
                                    duration: Duration(milliseconds: 300),
                                    type: PageTransitionType.downToUp,
                                    curve: Curves.decelerate,
                                    child: CategoryScreen(title: 'müzik', categoryId: 'k13',)
                                )
                            );
                          },
                          child: Text('#müzik')
                      ),
                      FlatButton(
                          padding: EdgeInsets.only(top: 30),
                          onPressed: () {
                            Navigator.push(
                                context,
                                PageTransition(
                                    duration: Duration(milliseconds: 300),
                                    type: PageTransitionType.downToUp,
                                    curve: Curves.decelerate,
                                    child: CategoryScreen(title: 'spor', categoryId: 'k12')
                                )
                            );
                          },
                          child: Text('#spor')
                      ),
                      FlatButton(
                          padding: EdgeInsets.only(top: 30),
                          onPressed: () {
                            Navigator.push(
                                context,
                                PageTransition(
                                    duration: Duration(milliseconds: 300),
                                    type: PageTransitionType.downToUp,
                                    curve: Curves.decelerate,
                                    child: CategoryScreen(title: 'sağlık', categoryId: 'k8')
                                )
                            );
                          },
                          child: Text('#sağlık')
                      ),
                      FlatButton(
                          padding: EdgeInsets.only(top: 30),
                          onPressed: () {
                            Navigator.push(
                                context,
                                PageTransition(
                                    duration: Duration(milliseconds: 300),
                                    type: PageTransitionType.downToUp,
                                    curve: Curves.decelerate,
                                    child: CategoryScreen(title: 'oyun-sanal günya', categoryId: 'k25')
                                )
                            );
                          },
                          child: Text('#oyun-sanal dünya')
                      ),
                      FlatButton(
                          padding: EdgeInsets.only(top: 30),
                          onPressed: () {
                            Navigator.push(
                                context,
                                PageTransition(
                                    duration: Duration(milliseconds: 300),
                                    type: PageTransitionType.downToUp,
                                    curve: Curves.decelerate,
                                    child: CategoryScreen(title: 'bilim-teknoloji', categoryId: 'k22')
                                )
                            );
                          },
                          child: Text('#bilim-teknoloji')
                      ),
                      FlatButton(
                          padding: EdgeInsets.only(top: 30),
                          onPressed: () {
                            Navigator.push(
                                context,
                                PageTransition(
                                    duration: Duration(milliseconds: 300),
                                    type: PageTransitionType.downToUp,
                                    curve: Curves.decelerate,
                                    child: CategoryScreen(title: 'yemek', categoryId: 'k26')
                                )
                            );
                          },
                          child: Text('#yemek')
                      ),
                      FlatButton(
                          padding: EdgeInsets.only(top: 30),
                          onPressed: () {
                            Navigator.push(
                                context,
                                PageTransition(
                                    duration: Duration(milliseconds: 300),
                                    type: PageTransitionType.downToUp,
                                    curve: Curves.decelerate,
                                    child: CategoryScreen(title: 'tarih-coğrafya', categoryId: 'k28')
                                )
                            );
                          },
                          child: Text('#tarih-coğrafya')
                      ),
                      FlatButton(
                          padding: EdgeInsets.only(top: 30),
                          onPressed: () {
                            Navigator.push(
                                context,
                                PageTransition(
                                    duration: Duration(milliseconds: 300),
                                    type: PageTransitionType.downToUp,
                                    curve: Curves.decelerate,
                                    child: CategoryScreen(title: 'gündem-tespit', categoryId: 'k24')
                                )
                            );                         },
                          child: Text('#edebiyat-sanat')
                      ),
                      FlatButton(
                          padding: EdgeInsets.only(top: 30),
                          onPressed: () {
                            Navigator.push(
                                context,
                                PageTransition(
                                    duration: Duration(milliseconds: 300),
                                    type: PageTransitionType.downToUp,
                                    curve: Curves.decelerate,
                                    child: CategoryScreen(title: 'felsefe-sosyoloji-psikoloji', categoryId: 'k29')
                                )
                            );
                          },
                          child: Text('#felsefe-sosyoloji-psikoloji')
                      ),
                      FlatButton(
                          padding: EdgeInsets.all(30),
                          onPressed: () {
                            Navigator.push(
                                context,
                                PageTransition(
                                    duration: Duration(milliseconds: 300),
                                    type: PageTransitionType.downToUp,
                                    curve: Curves.decelerate,
                                    child: CategoryScreen(title: 'ilişkiler-hayat', categoryId: 'k27')
                                )
                            );
                          },
                          child: Text('#ilişkiler-hayat')
                      )
                    ],
                  ),
                )
              ],
            );
          }
          if(state is Loaded) {
            if(state.titles.isEmpty) {
              return Text('empty titles');
            }
            return ListView(
              padding: EdgeInsets.only(top: 10),
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 40),
                  child: FlatButton(
                    padding: EdgeInsets.all(10),
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    color: CustomTheme.of(context).cardColor,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    onPressed: () {
                      Navigator.of(context).push(
                          PageTransition(
                              type: PageTransitionType.rightToLeftWithFade,
                              child: NewTitle(titleText: _controller.text)
                          )
                      );
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(EvaIcons.searchOutline),
                        Expanded(
                          child: Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Row(
                                children: <Widget>[
                                  Text(
                                    '"${_controller.text}"',
                                    style: TextStyle(fontWeight: FontWeight.w600),
                                  ),
                                  Text(' için arama sonuçları')
                                ],
                              )
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                ListView.separated(
                  physics: NeverScrollableScrollPhysics(),
                  controller: _scrollController,
                  padding: EdgeInsets.only(top: 10),
                  shrinkWrap: true,
                  itemCount: state.titles.length,
                  separatorBuilder: (context, index) => SizedBox(height: 5),
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: FlatButton(
                          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                          color: CustomTheme.of(context).cardColor,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)
                          ),
                          padding: EdgeInsets.all(10),
                          onPressed: () {
                            Navigator.of(context).push(
                                MaterialPageRoute(builder: (context) => TitleView(titleId: state.titles[index].id, titleText: state.titles[index].text))
                            );
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: Padding(
                                  padding: EdgeInsets.only(right: 10.0),
                                  child: Material(
                                    color: Colors.transparent,
                                    child: Text(
                                      state.titles[index].text.toLowerCase(),
                                      style: TextStyle(fontSize: 14),
                                      maxLines: 3,
                                      overflow: TextOverflow.clip,
                                    ),
                                  ),
                                ),
                              ),
                              state.titles[index].entryCount > 0 ? Text(state.titles[index].entryCount.toString()) : Text(''),
                            ],
                          )
                      ),
                    );
                  },
                )
              ],
            );
          }
          return Text('loading');
        },
      )
    );
  }
}