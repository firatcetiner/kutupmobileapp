import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class SearchEvent extends Equatable {
  const SearchEvent();

  @override
  List<Object> get props => [];
}

class SearchTitles extends SearchEvent {
  final String word;

  const SearchTitles({@required this.word});

  @override
  String toString() => 'Search Titles with keyword: $word';
}

class Idle extends SearchEvent {}