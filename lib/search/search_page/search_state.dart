import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:kutupsozluk_app/data/model/title_model.dart';

abstract class SearchState extends Equatable {
  const SearchState();

  @override
  List<Object> get props => [];
}

class Loaded extends SearchState {
  final List<TitleModel> titles;

  Loaded({@required this.titles});

  @override
  String toString() => 'Loaded { titles: ${titles.length} }';
}

class Empty extends SearchState {}

class Failure extends SearchState {}

class Loading extends SearchState {
}