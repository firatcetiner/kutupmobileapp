import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/data/model/title_model.dart';
import 'repository.dart';
import 'search_event.dart';
import 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState>{
  final SearchRepository _repository = SearchRepository();


  @override
  SearchState get initialState => Empty();


  @override
  Stream<SearchState> mapEventToState(SearchEvent event) async* {
    if (event is SearchTitles) {
      yield* _mapFetchToState(event);
    } else yield Empty();
  }

  Stream<SearchState> _mapFetchToState(SearchTitles event) async* {
    try {
      yield Loading();
      final response = await _repository.fetchTitlesByWord(event.word);
      final titles = response.map<TitleModel>((json) => TitleModel.fromJson(json)).toList();
      yield Loaded(titles: titles);
    } catch (_) {
      yield Failure();
    }
  }
}