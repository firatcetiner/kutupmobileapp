import 'package:kutupsozluk_app/search/new_title/new_title_repository.dart';

class NewTitleBloc {
  NewTitleRepository _repository = NewTitleRepository();

  Future<bool> addNewTitleWithEntry(String titleText, String entryText) async {
    return await _repository.addTitle(titleText, entryText);
  }
}