import 'package:flutter/material.dart';
import 'package:kutupsozluk_app/custom_transitions.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/title_view/entry_list_page.dart';
import 'package:kutupsozluk_app/style/custom_colors.dart' as Theme;

import 'new_title_bloc.dart';

class TitleDetail extends StatefulWidget {

  TitleDetail({@required this.titleText, @required this.categories}) :
      assert(titleText != null), assert(categories != null);

  final String titleText;
  final List<String> categories;

  @override
  _TitleDetailState createState() => _TitleDetailState();
}

class _TitleDetailState extends State<TitleDetail> {
  String get _titleText => widget.titleText;
  TextEditingController _textController;
  NewTitleBloc _bloc;

  @override
  void initState() {
    _bloc = NewTitleBloc();
    _textController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _textController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        elevation: 0.5,
        centerTitle: false,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Text('iptal', style: TextStyle(color: Colors.black87, fontSize: 15)),
            ),
            Row(
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Text('ukde', style: TextStyle(color: Colors.black38, fontSize: 15)),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      _bloc.addNewTitleWithEntry(_titleText, _textController.text).then((val) {
                        if(val) {
                          Navigator.popUntil(context, ModalRoute.withName(Navigator.defaultRouteName));
                          Navigator.of(context).push(
                            PageTransition(
                              type: PageTransitionType.rightToLeftWithFade,
                              child: TitleView(titleText: _titleText, titleId: 0)
                            )
                          );
                        } else {
                          Navigator.pop(context);
                        }
                      });
                    },
                    child: Text('gönder', style: TextStyle(color: Theme.CustomColors.loginGradientStart, fontSize: 15)),
                  ),
                ),
              ],
            ),
          ],
        ),
        automaticallyImplyLeading: false,
      ),
      body: Column(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.only(left: 15.0, top: 10.0, bottom: 10.0),
              child: Text(_titleText, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15.0, top: 10.0, bottom: 10.0, right: 10.0),
            child: TextField(
              controller: _textController,
              maxLines: null,
              style: TextStyle(color: Colors.grey[600]),
              decoration: InputDecoration.collapsed(
                hintText: 'entry girin'
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15.0, top: 10.0, bottom: 10.0, right: 10.0),
            child: Row(
              children: <Widget>[],
            ),
          ),
        ],
      ),
    );
  }
}