import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kutupsozluk_app/search/new_title/title_detail_page.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';

class NewTitle extends StatefulWidget {
  NewTitle({Key key, @required this.titleText}) : super(key: key);

  @override
  _NewTitleState createState() => _NewTitleState();
  final String titleText;
}

class _NewTitleState extends State<NewTitle> {

  final List<String> _list=<String>[
    'gündem-tespit',
    'anket',
    'tv-sinema-magazin',
    'siyaset-ekonomi-hukuk',
    'müzik',
    'spor',
    'sağlık',
    'oyun-sanal dünya',
    'bilim-teknoloji',
    'yemek',
    'tarih-coğrafya',
    'felsefe-sosyoloji-psikoloji',
    'ilişkiler-hayat'
  ];

  List<String> _filters = <String>[];
  Iterable<Widget> get actorWidgets sync*{
    for(String actor in _list){
      yield Padding(
        padding: const EdgeInsets.only(left: 4.0),
        child: ChoiceChip(
          labelStyle: TextStyle(color: Colors.black87),
          backgroundColor: Colors.white24,
          elevation: 1.0,
          pressElevation: 2.0,
          selectedColor: CustomTheme.of(context).indicatorColor,
          label: Text(actor),
          selected: _filters.contains(actor),
          onSelected: (value){
            setState(() {
              if(_filters.length < 2 && _filters.length >= 0 && !_filters.contains(actor))
                _filters.add(actor);
              else if(_filters.contains(actor))
                _filters.remove(actor);
            });
            print(_filters);
          },
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.5,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: Icon(EvaIcons.arrowIosBackOutline, size: 25),
        ),
        centerTitle: true,
        title: Text(widget.titleText),
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Text('yeni bir başlık açmak üzeresin. bir tanım yapabilir, örnek verebilir veya başlığı ukde olarak paylaşabilirsin.',
              style: TextStyle(color: Colors.grey[600])),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Text('kategoriler',
              style: TextStyle(color: Colors.grey[800], fontSize: 20)),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15.0),
            child: Wrap(
              children: actorWidgets.toList(),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: MaterialButton(
              padding: EdgeInsets.all(10),
              elevation: 1.0,
              highlightElevation: 1.0,
              focusElevation: 1.0,
              onPressed: () {
                print('yeni başlık');
                Navigator.of(context).push(
                  MaterialPageRoute(
                    fullscreenDialog: false,
                    builder: (context) => TitleDetail(titleText: widget.titleText, categories: _filters)
                  )
                );
              },
              color: CustomTheme.of(context).indicatorColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Text('bu başlık hakkında bilgi ver', style: TextStyle(color: Colors.white)),
            ),
          )
        ],
      ),
    );
  }
}