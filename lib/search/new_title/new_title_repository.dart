import 'package:kutupsozluk_app/main.dart';
import 'package:dio/dio.dart' show Options;
import 'package:dio/dio.dart' show DioError;
import 'package:dio/dio.dart' show DioErrorType;


class NewTitleRepository {

  Future<bool> addTitle(String titleText, String entryText) async {
    var url = '$baseUrl/api/title/add';
    var token = await prefs.get('token');
    final response =  await client.post(
      url,
      data: {
        'titleText': titleText,
        'entryText': entryText,
      },
      options: Options(
        headers: {
          'accept': 'application/json',
          'Content-Type': 'application/json-patch+json',
          'Authorization': 'Bearer $token'
        },
      )
    );
    if(response.statusCode == 200 || response.statusCode == 304)
      return true;
    else
      throw DioError(response: response, type: DioErrorType.RESPONSE);
  }
}