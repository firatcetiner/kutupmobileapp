import 'dart:async';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/events_and_states/event.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/events_and_states/state.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/title_view/entry_list_screen.dart';
import 'package:kutupsozluk_app/search/category/bloc/bloc.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';


// ignore: must_be_immutable
class CategoryPage extends StatelessWidget {
  CategoryPage({Key key, @required this.title, @required this.categoryId})
    : assert(title != null),
      assert(categoryId != null),
      super(key: key);

  final String title;
  final String categoryId;
  ScrollController _scrollController;
  Completer<void> _refreshCompleter = Completer<void>();
  final _refreshController = RefreshController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(42),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.05),
              spreadRadius: 1,
              offset: Offset(0, 2.0),
              blurRadius: 4.0,
            )
          ]),
          child: AppBar(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            leading: IconButton(
              onPressed: () => Navigator.pop(context),
              icon: Icon(EvaIcons.arrowIosBackOutline)
            ),
            centerTitle: true,
            title: Text(title.toLowerCase())
          ),
        ),
      ),
      body: BlocListener<CategoryBloc, TitleState>(
        listener: (context, state) {
          if(state is Loaded) {
            _refreshCompleter?.complete();
            _refreshCompleter = Completer();
          }
        },
        child: BlocBuilder<CategoryBloc, TitleState>(
            builder: (context, state) {
              if (state is TitleError) {
                return Center(
                  child: Text('Oops something went wrong!'),
                );
              }
              if(state is Loaded) {
                if(state.titles.isEmpty) {
                  return Text('başlık yok');
                }
                return SmartRefresher(
                  header: CustomHeader(
                    builder: (context, status) {
                      return Container(
                        height: 55.0,
                        child: Center(
                          child:SizedBox(
                            width: 15,
                            height: 15,
                            child: CircularProgressIndicator(strokeWidth: 2),
                          )
                        ),
                      );
                    },
                  ),
                  controller: _refreshController,
                  scrollController: _scrollController,
                  onRefresh: () {
                    BlocProvider.of<CategoryBloc>(context)..add(Fetch(page: 1));
                    return _refreshCompleter.future.whenComplete(() => _refreshController.refreshCompleted());
                  },
                  enablePullUp: true,
                  onLoading: () {
                    state.update(loading: true);
                    return Future.value(BlocProvider.of<CategoryBloc>(context)..add(FetchNext(page: state.nextPage))).whenComplete(() {
                      state.update(loading: false);
                    }).whenComplete(() => _refreshController.loadComplete());
                  },
                  child: ListView.separated(
                    shrinkWrap: true,
                    controller: _scrollController,
                    padding: EdgeInsets.only(top: 5),
                    separatorBuilder: (context, index) => SizedBox(height: 5),
                    itemCount: state.titles.length,
                    itemBuilder: (context, index) {
                      return Container(
                        margin: EdgeInsets.symmetric(horizontal: 5),
                        decoration: BoxDecoration(
                          boxShadow: [BoxShadow(
                            color: Colors.black12.withOpacity(0.1),
                            spreadRadius: 0.5,
                            blurRadius: 2.5
                          )]
                        ),
                        child: FlatButton(
                            color: CustomTheme.of(context).cardColor,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                            padding: EdgeInsets.zero,
                            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                            onPressed: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  fullscreenDialog: true,
                                  builder: (context) => EntryListScreen(titleId: state.titles[index].id, titleText: state.titles[index].text)
                                )
                              );
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(10),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Text(
                                      state.titles[index].text.toLowerCase(),
                                      overflow: TextOverflow.clip,
                                    ),
                                  ),
                                  state.titles[index].entryCount == 0 ? Container(height: 0) :
                                  Text(state.titles[index].entryCount.toString())
                                ],
                              ),
                            ),
                        ),
                      );
                    },
                  ),
                );
              }
              return Center(child: Text('loading'));
            }
        ),
      ),
    );
  }
}

