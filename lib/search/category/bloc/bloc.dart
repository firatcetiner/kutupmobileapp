import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/data/model/title_model.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/events_and_states/event.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/events_and_states/state.dart';

import '../repository/repository.dart';

class CategoryBloc extends Bloc<TitleEvent, TitleState> {
  final CategoryRepository _repository = CategoryRepository();

  String _categoryId;

  CategoryBloc(String id) {
    this._categoryId = id;
  }

  @override
  TitleState get initialState => Loading();

  @override
  Stream<TitleState> mapEventToState(TitleEvent event) async* {
    if (event is Fetch) {
      yield* _mapFetchToState(event);
    } else if(event is FetchNext) {
      yield* _mapFetchNextToState(event);
    }
  }

  Stream<TitleState> _mapFetchNextToState(FetchNext event) async* {
    final currentState = state;
    if(currentState is Loaded) {
      try {
        final response = await _repository.fetchTitlesByCategoryId(_categoryId, currentState.nextPage);
        final titles = response.map<TitleModel>((json) => TitleModel.fromJson(json)).toList();
        yield Loaded(isLoading: false, titles: currentState.titles + titles, hasReachedMax: titles != null ? false : true, nextPage: currentState.nextPage + 1);
      } catch(_) {
        yield TitleError();
      }
    }
  }

  Stream<TitleState> _mapFetchToState(Fetch event) async* {
    try {
      final response = await _repository.fetchTitlesByCategoryId(_categoryId, 1);
      final items = response.map<TitleModel>((json) => TitleModel.fromJson(json)).toList();
      yield Loaded(isLoading: false, titles: items, hasReachedMax: false, nextPage: 2);
    } catch (_) {
      yield TitleError();
    }
  }
}