import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/home/homescreen/pages/events_and_states/event.dart';
import 'package:kutupsozluk_app/search/category/bloc/bloc.dart';
import 'package:kutupsozluk_app/search/category/category_page.dart';

class CategoryScreen extends StatefulWidget {

  CategoryScreen({Key key, @required this.title, @required this.categoryId})
      : assert(title != null),
        assert(categoryId != null),
        super(key: key);

  final String title;
  final String categoryId;
  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> with AutomaticKeepAliveClientMixin {

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocProvider<CategoryBloc>(
      create: (context) {
        return CategoryBloc(widget.categoryId)..add(Fetch(page: 1));
      },
      child: CategoryPage(title: widget.title, categoryId: widget.categoryId),
    );
  }

  @override
  bool get wantKeepAlive => true;
}