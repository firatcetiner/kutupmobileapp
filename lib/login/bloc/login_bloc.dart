import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:kutupsozluk_app/main.dart';
import 'package:kutupsozluk_app/profile/settings/followers/models/user_info_model.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';
import 'package:kutupsozluk_app/login/login.dart';
import 'package:kutupsozluk_app/profile/user_repository.dart';
import 'package:kutupsozluk_app/validators.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  UserRepository _userRepository;

  LoginBloc({
    @required UserRepository userRepository,
  })  : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  LoginState get initialState => LoginState.empty();

  @override
  Stream<LoginState> transformEvents(
    Stream<LoginEvent> events,
    Stream<LoginState> Function(LoginEvent event) next,
  ) {
    final nonDebounceStream = events.where((event) {
      return (event is! EmailChanged && event is! PasswordChanged);
    });
    final debounceStream = events.where((event) {
      return (event is EmailChanged || event is PasswordChanged);
    }).debounceTime(Duration(milliseconds: 50));
    return super.transformEvents(
      nonDebounceStream.mergeWith([debounceStream]),
      next,
    );
  }

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is PasswordChanged) {
      yield* _mapPasswordChangedToState(event.password);
    } else if (event is LoginWithCredentialsPressed) {
      yield* _mapLoginWithCredentialsPressedToState(
        nick: event.nick,
        password: event.password,
      );
    }
  }

  Stream<LoginState> _mapEmailChangedToState(String nick) async* {
    yield state.update(
      isEmailValid: Validators.isValidEmail(nick),
    );
  }

  Stream<LoginState> _mapPasswordChangedToState(String password) async* {
    yield state.update(
      isPasswordValid: Validators.isValidPassword(password),
    );
  }


  Stream<LoginState> _mapLoginWithCredentialsPressedToState({
    String nick,
    String password,
  }) async* {
    yield LoginState.loading();
    try {
      final signedIn = await _userRepository.signIn(nick, password);
      final uid = prefs.getString('uid');
      final response = await getUserInfo(int.parse(uid));
      final userInfo = UserInfoModel.fromJson(response);
      prefs.setString('avatar', userInfo.image);
      if(signedIn) yield LoginState.success();
      else yield LoginState.failure();
    } catch(_) {
      yield LoginState.failure();
    }
  }
}
