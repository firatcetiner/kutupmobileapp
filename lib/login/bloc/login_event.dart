import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
abstract class LoginEvent extends Equatable {
  LoginEvent([List props = const []]) : super(props);
}

class EmailChanged extends LoginEvent {
  final String nick;

  EmailChanged({@required this.nick}) : super([nick]);

  @override
  String toString() => 'EmailChanged { nick :$nick }';
}

class PasswordChanged extends LoginEvent {
  final String password;

  PasswordChanged({@required this.password}) : super([password]);

  @override
  String toString() => 'PasswordChanged { password: $password }';
}

class Submitted extends LoginEvent {
  final String nick;
  final String password;

  Submitted({@required this.nick, @required this.password})
      : super([nick, password]);

  @override
  String toString() {
    return 'Submitted { nick: $nick, password: $password }';
  }
}

class LoginWithCredentialsPressed extends LoginEvent {
  final String nick;
  final String password;

  LoginWithCredentialsPressed({@required this.nick, @required this.password})
      : super([nick, password]);

  @override
  String toString() {
    return 'LoginWithCredentialsPressed { nick: $nick, password: $password }';
  }
}
