import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/profile/user_repository.dart';
import 'package:kutupsozluk_app/login/login.dart';
import 'package:kutupsozluk_app/teddy/teddy_controller.dart';
import 'package:kutupsozluk_app/welcoming_screen/bloc/welcome_screen_bloc.dart';
import 'package:kutupsozluk_app/welcoming_screen/bloc/welcome_screen_event.dart';
import 'package:kutupsozluk_app/welcoming_screen/welcoming_screen.dart';

class LoginScreen extends StatelessWidget {
  final UserRepository _userRepository;
  final WelcomingScreenBloc _bloc;
  final TeddyController _teddyController;

  LoginScreen({Key key, @required UserRepository userRepository,
    @required WelcomingScreenBloc bloc, @required TeddyController teddyController})
    : assert(userRepository != null),
      assert(bloc != null),
      assert(teddyController != null),
      _teddyController = teddyController,
      _bloc = bloc,
      _userRepository = userRepository,
      super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        FocusScope.of(context).unfocus();
        animationController.reverse().whenComplete(() {
          _bloc..add(OpenWelcome(margin: 300, borderRadius: 30));
          animationController.forward();
        });
        return null;
      },
      child: BlocProvider<LoginBloc>(
        create: (context) => LoginBloc(userRepository: _userRepository),
        child: LoginForm(bloc: _bloc, teddyController: _teddyController),
      ),
    );
  }
}
