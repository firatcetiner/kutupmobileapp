import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kutupsozluk_app/profile/user_repository.dart';
import 'package:kutupsozluk_app/register/register.dart';

class CreateAccountButton extends StatelessWidget {
  final UserRepository _userRepository;

  final _fontFamily = 'RobotoMono';

  CreateAccountButton({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => RegisterScreen(userRepository: _userRepository)),
        );
      },
      child: Container(
        height: 55.0,
        decoration: BoxDecoration(
          color: Colors.orangeAccent,
          borderRadius: BorderRadius.circular(5.0)
        ),
        child: Center(
          child: Text(
            'Kayıt Ol',
            style: TextStyle(fontFamily: _fontFamily, color: Colors.white, fontSize: 25.0)
          ),
        ),
      ),
    );
  }
}
