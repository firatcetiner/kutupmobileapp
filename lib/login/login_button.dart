import 'package:flutter/material.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';

class LoginButton extends StatelessWidget {
  final VoidCallback _onPressed;

  LoginButton({Key key, VoidCallback onPressed})
      : _onPressed = onPressed,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 55.0,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.0)
        ),
        child: MaterialButton(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          elevation: 0.0,
          highlightElevation: 2.0,
          disabledColor: Colors.grey,
          color: CustomTheme.of(context).indicatorColor,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          onPressed: _onPressed,
          child: Center(child: Text('giriş', style: TextStyle(color: Colors.white, fontSize: 25.0))),
        ),
      ),
    );
  }
}
