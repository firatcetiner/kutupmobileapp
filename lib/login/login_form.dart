import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/authentication_bloc/bloc.dart';
import 'package:kutupsozluk_app/login/login.dart';
import 'package:kutupsozluk_app/teddy/teddy_controller.dart';
import 'package:kutupsozluk_app/teddy/tracking_text_input.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';
import 'package:kutupsozluk_app/welcoming_screen/bloc/welcome_screen_bloc.dart';
import 'package:kutupsozluk_app/welcoming_screen/bloc/welcome_screen_event.dart';
import 'package:kutupsozluk_app/welcoming_screen/welcoming_screen.dart';

class LoginForm extends StatefulWidget {
  final WelcomingScreenBloc _bloc;
  final TeddyController _teddyController;

  LoginForm({Key key, @required WelcomingScreenBloc bloc, @required TeddyController teddyController})
      : assert(bloc != null),
        assert(teddyController != null),
        _teddyController = teddyController,
        _bloc = bloc,
        super(key: key);

  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  LoginBloc _loginBloc;
  WelcomingScreenBloc get _welcomingScreenBloc => widget._bloc;
  TeddyController get _teddyController => widget._teddyController;


  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  bool isLoginButtonEnabled(LoginState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state.isFailure) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text('Login Failure'), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
        if (state.isSubmitting) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Logging In...'),
                    CircularProgressIndicator(),
                  ],
                ),
              ),
            );
        }
        if (state.isSuccess) {
          BlocProvider.of<AuthenticationBloc>(context).add(LoggedIn());
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(
        builder: (context, state) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: GestureDetector(
                  onPanDown: (details) {

                  },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(EvaIcons.arrowIosBack, size: 30),
                        onPressed: () {
                          FocusScope.of(context).unfocus();
                          animationController.reverse().whenComplete(() {
                            _welcomingScreenBloc..add(OpenWelcome(margin: 300, borderRadius: 30));
                            animationController.forward();
                          });
                        },
                      ),
                      Text('giriş', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500)),
                      IconButton(
                        icon: Icon(EvaIcons.arrowIosBackOutline, size: 30, color: Colors.transparent),
                        onPressed: null,
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    Container(
                      alignment: Alignment.centerLeft,
                      height: 55,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        border: Border.all(width: 2, color: Colors.grey)
                      ),
                      margin: EdgeInsets.symmetric(horizontal: 15),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: TrackingTextInput(
                          onTap: () {
                            _welcomingScreenBloc..add(OpenLogin(margin: 0, borderRadius: 30));
                          },
                          textEditingController: _emailController,
                          hint: 'email veya nick',
                          onCaretMoved: (offset) {
                            _teddyController.lookAt(offset);
                          },
                        )
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      height: 55,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        border: Border.all(width: 2, color: Colors.grey)
                      ),
                      margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: TrackingTextInput(
                          isObscured: true,
                          hint: 'şifre',
                          textEditingController: _passwordController,
                          onTap: () {
                            _welcomingScreenBloc..add(OpenLogin(margin: 0, borderRadius: 30));
                          },
                          onCaretMoved: (offset) {
                            _teddyController.lookAt(offset);
                          },
                        )
                      ),
                    ),
                    GestureDetector(
                      onTap: isLoginButtonEnabled(state)
                          ? _onFormSubmitted
                          : null,
                      child: Container(
                        height: 55,
                        alignment: Alignment.center,
                        margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                        decoration: BoxDecoration(
                          color: CustomTheme.of(context).indicatorColor,
                          borderRadius: BorderRadius.circular(40),
                          border: Border.all(width: 2, color: CustomTheme.of(context).indicatorColor)
                        ),
                        child: Text('giriş yap', style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 22)),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        animationController.reverse().whenComplete(() {
                          _welcomingScreenBloc..add(OpenRegister(margin: 220, borderRadius: 30));
                          animationController.forward();
                        });
                      },
                      child: Container(
                        height: 55,
                        alignment: Alignment.center,
                        margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                          border: Border.all(width: 2, color: CustomTheme.of(context).textTheme.title.color)
                        ),
                        child: Text('yeni hesap oluştur', style: TextStyle(color: CustomTheme.of(context).textTheme.title.color, fontWeight: FontWeight.w500, fontSize: 22)
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void _onEmailChanged() {
    _loginBloc.add(
      EmailChanged(nick: _emailController.text),
    );
  }

  void _onPasswordChanged() {
    _loginBloc.add(
      PasswordChanged(password: _passwordController.text),
    );
  }

  void _onFormSubmitted() {
    FocusScope.of(context).unfocus();
    _loginBloc.add(
      LoginWithCredentialsPressed(
        nick: _emailController.text,
        password: _passwordController.text,
      ),
    );
  }
}
