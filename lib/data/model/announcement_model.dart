class AnnouncementModel {
  String subject;
  String announcement;
  String nick;
  String date;

  AnnouncementModel({
    this.subject,
    this.announcement,
    this.nick,
    this.date
  });

  AnnouncementModel.fromJson(Map<String, dynamic> json) {
    subject = json['subject'];
    announcement = json['announcement'];
    nick = json['nick'];
    date = json['date'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['subject'] = this.subject;
    data['announcement'] = this.announcement;
    data['nick'] = this.nick;
    data['date'] = this.date;
    return data;
  }
}
