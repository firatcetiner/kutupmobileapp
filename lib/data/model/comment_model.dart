class Comment {
  int id;
  int entryId;
  String commentText;
  String date;
  String nick;
  int plus;
  int favorite;
  Comment({
    this.id,
    this.entryId,
    this.commentText,
    this.date,
    this.nick,
    this.plus,
    this.favorite});

  Comment.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    entryId = json['entryId'];
    commentText = json['commentText'];
    date = json['date'];
    nick = json['nick'];
    plus = json['plus'];
    favorite = json['favorite'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['entryId'] = this.entryId;
    data['commentText'] = this.commentText;
    data['date'] = this.date;
    data['nick'] = this.nick;
    data['plus'] = this.plus;
    data['favorite'] = this.favorite;
    return data;
  }

}
