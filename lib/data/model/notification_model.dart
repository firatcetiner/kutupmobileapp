class NotificationModel {
  int id;
  String subject;
  String message;
  String who;
  int count;
  int isRead;

  NotificationModel({
    this.id,
    this.subject,
    this.message,
    this.isRead,
    this.count,
    this.who
  });

  NotificationModel.fromJson(Map<String, dynamic> json) {
    subject = json['subject'].toString();
    switch(subject) {
      case 'basliktakip': 
        id = int.parse(json['message'].split('--')[1]);
        message = 'takip ettiğin ${json['message'].split('--')[0]} başlığına yeni entry girildi.';
        break;
      case 'profil':
        who = json['message'].split('--')[0].toString();
        message = '$who profilinizi görüntüledi.';
        break;
      case 'favori':
        id = int.parse(json['message'].split('--')[0]);
        who = json['message'].split('--')[1].toString();
        message = '$who entrynizi favoriledi.';
        break;
      case 'arti':
        id =  int.parse(json['message'].split('--')[0].toString());
        count = int.parse(json['message'].split('--')[1].toString());
        message = '$count. karlı oyunu aldı.';
        break;
      case 'eksi':
        id = int.parse(json['message'].split('--')[0]);
        count = int.parse(json['message'].split('--')[1]);
        message = '$count. ateşli oyunu aldı.';
        break;
      case 'ilik':
        id = int.parse(json['message'].split('--')[0]);
        count = int.parse(json['message'].split('--')[1]);
        message = '$count. ilik oyunu aldı.';
        break;
      case 'yorumarti':
        id = int.parse(json['message'].split('--')[0]);
        who = json['message'].split('--')[1].toString();
        message = 'yorumun karlı oy aldı.';
        break;
      case 'yorumeksi':
        id = int.parse(json['message']);
        message = 'yorumun ateşli oy aldı.';
        break;
      default: break;
    }
    isRead = int.parse(json['isRead'].toString());
  }


}
