import 'comment_model.dart';

class Entry {
  int id;
  int titleId;
  int userId;
  String titleText;
  String entryText;
  String date;
  String lastUpdateDate;
  String nick;
  int commentCount;
  int plus;
  int warm;
  int minus;
  int favorite;
  int voteType;
  int isFavourite;
  String userAvatar;
  List<Comment> comments;

  Entry({
    this.id,
    this.titleId,
    this.userId,
    this.titleText,
    this.entryText,
    this.date,
    this.lastUpdateDate,
    this.nick,
    this.commentCount,
    this.plus,
    this.warm,
    this.minus,
    this.favorite,
    this.comments,
    this.voteType,
    this.isFavourite,
    this.userAvatar
  });

  Entry.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    titleId = json['titleId'];
    userId = json['userId'];
    titleText = json['titleText'];
    entryText = json['entryText'];
    date = json['date'];
    lastUpdateDate = json['lastUpdateDate'];
    nick = json['nick'];
    commentCount = json['commentCount'];
    plus = json['plus'] as int;
    warm = json['warm'] as int;
    minus = json['minus'] as int;
    favorite = json['favorite'] as int;
    voteType = json['voteType'] as int;
    isFavourite = json['isFavourite'] as int;
    userAvatar = json['userAvatar'] as String;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['userId'] = this.userId;
    data['titleId'] = this.titleId;
    data['titleText'] = this.titleText;
    data['entryText'] = this.entryText;
    data['date'] = this.date;
    data['lastUpdateDate'] = this.lastUpdateDate;
    data['nick'] = this.nick;
    data['commentCount'] = this.commentCount;
    data['plus'] = this.plus;
    data['warm'] = this.warm;
    data['minus'] = this.minus;
    data['favorite'] = this.favorite;
    data['comments'] = this.comments;
    data['isFavourite'] = this.isFavourite;
    data['votaType'] = this.voteType;
    data['userAvatar'] = this.userAvatar;
    return data;
  }
}
