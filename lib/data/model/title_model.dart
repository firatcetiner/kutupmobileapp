class TitleModel {
  int id;
  String text;
  String category;
  String author;
  int hit;
  int entryCount;

  TitleModel({
    this.id,
    this.text,
    this.category,
    this.author,
    this.hit,
    this.entryCount});

  TitleModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    text = json['text'];
    category = json['category'];
    author = json['author'];
    hit = json['hit'];
    entryCount = json['entryCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['text'] = this.text;
    data['category'] = this.category;
    data['author'] = this.author;
    data['hit'] = this.hit;
    data['entryCount'] = this.entryCount;
    return data;
  }
}
