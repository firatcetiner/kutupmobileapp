class QuestionModel {
  int id;
  String title;
  String text;
  String nick;
  int answerCount;
  String date;
  int isAnonymous;

  QuestionModel({
    this.id,
    this.title,
    this.text,
    this.date,
    this.nick,
    this.answerCount,
    this.isAnonymous
  });

  QuestionModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    text = json['text'];
    date = json['date'];
    nick = json['nick'];
    answerCount = json['answerCount'];
    isAnonymous = json['isAnonymous'];
  }

  Map<String, dynamic> toJson() {
    final data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['text'] = this.text;
    data['date'] = this.date;
    data['nick'] = this.nick;
    data['answerCount'] = this.answerCount;
    data['isAnonymous'] = this.isAnonymous;
    return data;
  }

}
