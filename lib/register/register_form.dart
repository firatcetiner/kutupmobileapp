import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/authentication_bloc/bloc.dart';
import 'package:kutupsozluk_app/register/register.dart';
import 'package:kutupsozluk_app/teddy/teddy_controller.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';
import 'package:kutupsozluk_app/welcoming_screen/bloc/welcome_screen_bloc.dart';

class RegisterForm extends StatefulWidget {
  final WelcomingScreenBloc _bloc;
  final TeddyController _teddyController;

  RegisterForm({Key key, @required WelcomingScreenBloc bloc, @required TeddyController teddyController})
    : assert(bloc != null),
      assert(teddyController != null),
      _teddyController = teddyController,
      _bloc = bloc,
      super(key: key);

  State<RegisterForm> createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _birthDateController = TextEditingController();
  final TextEditingController _nickController = TextEditingController();

  String _gender;
  String _birthDate;

  RegisterBloc _registerBloc;

  bool isPasswordObscure = true;
  bool isConfirmPasswordObscure = true;
  void onObscurePressed() {
    setState(() {
      isPasswordObscure = !isPasswordObscure;
    });
  }


  void onConfirmPasswordObscurePressed() {
    setState(() {
      isConfirmPasswordObscure = !isConfirmPasswordObscure;
    });
  }

  bool get isPopulated =>
      _nickController.text.isNotEmpty && _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty
      && _confirmPasswordController.text.isNotEmpty && _nameController.text.isNotEmpty
      && _birthDate != null && _gender != null;

  bool isRegisterButtonEnabled(RegisterState state) {
    return isPopulated && !state.isSubmitting;
  }

  Future<Null> _selectDate(BuildContext context) async {
    String month;
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateTime(2019, 12, 12),
      firstDate: DateTime(1923, 10),
      lastDate: DateTime(2101)
    );
    if (picked != null) {
      setState(() {
        month = picked.month.toString();
        if(month.length == 1) {
          month = ('0' + picked.month.toString());
        }
        _birthDate = picked.day.toString() + '/' + month + '/' + picked.year.toString();
      });
    }
    print('bd is $_birthDate');
  }

  @override
  void initState() {
    super.initState();
    _registerBloc = BlocProvider.of<RegisterBloc>(context);
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
    _confirmPasswordController.addListener(_onConfirmPasswordChanged);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterBloc, RegisterState>(      
      listener: (context, state) {
        if (state.isSubmitting) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                backgroundColor: Colors.blueAccent,
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('kayıt olunuyor...'),
                    Container(width: 20.0, height: 20.0, child: CircularProgressIndicator(
                      strokeWidth: 2.0, backgroundColor: Colors.white)),
                  ],
                ),
              ),
            );
        }
        if (state.isSuccess) {
          BlocProvider.of<AuthenticationBloc>(context).add(LoggedIn());
          Navigator.of(context).pop();
        }
        if (state.isFailure) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Container(
                  height: 20.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('böyle bir kullanıcı zaten var'),
                      Icon(Icons.error),
                    ],
                  ),
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
      },
      child: BlocBuilder<RegisterBloc, RegisterState>(
        builder: (context, state) {
          return Padding(
            padding: EdgeInsets.only(left: 30.0, right: 30.0),
            child: Form(
              child: ListView(
                children: <Widget>[
                  SizedBox(height: 20.0),
                  Container(
                    decoration: BoxDecoration(
                      color: CustomTheme.of(context).cardColor,
                      borderRadius: BorderRadius.circular(10)
                    ),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                          child: TextFormField(
                            style: TextStyle(color: Colors.grey[700]),
                            controller: _nickController,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              icon: Icon(EvaIcons.personOutline),
                              hintStyle: TextStyle(color: Colors.grey[500]),
                              hintText: 'nickiniz',
                              border: UnderlineInputBorder(borderSide: BorderSide(width: 0.5, style: BorderStyle.none), borderRadius: BorderRadius.circular(0.5)),
                              labelText: 'nick'
                            ),
                            autocorrect: false,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 20, left: 20, right: 20),
                          child: TextFormField(
                            style: TextStyle(color: Colors.grey[700]),
                            controller: _passwordController,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                icon: Icon(EvaIcons.lockOutline),
                                suffixIcon: GestureDetector(
                                  onTap: onObscurePressed,
                                  child: Icon(Icons.remove_red_eye,
                                    color: isPasswordObscure ? Colors.grey : Colors.orangeAccent.withOpacity(0.6)),
                                ),
                                hintText: 'şifreniz',
                                labelText: 'şifre',
                                helperText: 'şifreniz en az 8 karakterden oluşmalı,\nharf ve sayı içermelidir',
                                errorStyle: TextStyle(color: Colors.red.withOpacity(0.6)),
                                hintStyle: TextStyle(color: Colors.grey[500]),
                                border: UnderlineInputBorder(borderSide: BorderSide(width: 0.5, style: BorderStyle.none), borderRadius: BorderRadius.circular(0.5)),
                            ),
                            obscureText: isPasswordObscure,
                            autovalidate: true,
                            autocorrect: false,
                            validator: (_) {
                              return !state.isPasswordValid ? 'Geçersiz Şifre' : null;
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, top: 20, bottom: 30, right: 20),
                          child: TextFormField(
                            style: TextStyle(color: Colors.grey[700]),
                            controller: _confirmPasswordController,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              border: UnderlineInputBorder(borderSide: BorderSide(width: 0.5, style: BorderStyle.none), borderRadius: BorderRadius.circular(0.5)),
                              icon: Icon(EvaIcons.lockOutline),
                              suffixIcon: GestureDetector(
                                onTap: onConfirmPasswordObscurePressed,
                                child: Icon(Icons.remove_red_eye,
                                    color: isConfirmPasswordObscure ? Colors.grey : Colors.orangeAccent.withOpacity(0.6)),
                              ),
                              hintText: 'şifreniz',
                              labelText: 'şifre tekrar',
                              errorStyle: TextStyle(color: Colors.red.withOpacity(0.6)),
                              hintStyle: TextStyle(color: Colors.grey[500]),
                            ),
                            obscureText: isConfirmPasswordObscure,
                            autovalidate: true,
                            autocorrect: false,
                            validator: (_) {
                              if(!state.isPasswordValid && (_passwordController.text != _confirmPasswordController.text))
                                return 'Şifreler Uyuşmuyor';
                              else if((_passwordController.text != _confirmPasswordController.text)) return 'Şifreler Uyuşmuyor';
                              else if(!state.isPasswordValid) return 'Geçersiz Şifre';
                              return null;
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20.0),
                  Container(
                    decoration: BoxDecoration(
                      color: CustomTheme.of(context).cardColor,
                      borderRadius: BorderRadius.circular(10)
                    ),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 20.0, top: 20, right: 20),
                          child: TextFormField(
                            style: TextStyle(color: Colors.grey[700]),
                            keyboardType: TextInputType.text,
                            controller: _nameController,
                            decoration: InputDecoration(
                              icon: Icon(EvaIcons.personOutline),
                              labelText: 'isim',
                              hintText: 'isminiz',
                              hintStyle: TextStyle(color: Colors.grey[500]),
                              border: UnderlineInputBorder(borderSide: BorderSide(width: 0.5, style: BorderStyle.none), borderRadius: BorderRadius.circular(0.5)),
                            ),
                            autocorrect: false,
                            autovalidate: true,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(20),
                          child: TextFormField(
                              style: TextStyle(color: Colors.grey[700]),
                              controller: _emailController,
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                  icon: Icon(EvaIcons.emailOutline),
                                  hintText: 'email adresiniz',
                                  labelText: 'email adresi',
                                  errorStyle: TextStyle(color: Colors.red.withOpacity(0.6)),
                                  hintStyle: TextStyle(color: Colors.grey[500]),
                                  border: UnderlineInputBorder(borderSide: BorderSide(width: 0.5, style: BorderStyle.none), borderRadius: BorderRadius.circular(0.5)),
                              ),
                              autocorrect: false,
                              autovalidate: true,
                              validator: (_) {
                                return !state.isEmailValid ? 'Geçersiz Eposta' : null;
                              }
                          ),
                        ),
                        GestureDetector(
                          onTap: () => _selectDate(context),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 30),
                            child: Center(
                              child: Row(
                                children: <Widget>[
                                  Icon(EvaIcons.calendarOutline, size: 22),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 20.0),
                                    child: Text('$_birthDate', style: TextStyle(color: Colors.grey[700], fontSize: 15.0, fontWeight: FontWeight.w500))
                                  ),
                                ],
                              ),
                            )
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Container(
                    padding: EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      color: CustomTheme.of(context).cardColor,
                      borderRadius: BorderRadius.circular(10)
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Text('cinsiyet: '),
                        Row(
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  _gender = 'erkek';
                                });
                              },
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                      color: _gender == 'erkek' ? CustomTheme.of(context).indicatorColor : Colors.transparent,
                                      shape: BoxShape.circle,
                                      border: Border.all(width: 0.8)
                                    ),
                                    child: Text('e')
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Text('erkek'),
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    _gender = 'kadın';
                                  });
                                },
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.all(10),
                                      decoration: BoxDecoration(
                                        color: _gender == 'kadın' ? CustomTheme.of(context).indicatorColor : Colors.transparent,
                                        shape: BoxShape.circle,
                                        border: Border.all(width: 0.8)
                                      ),
                                      child: Text('k')
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 10.0),
                                      child: Text('kadın'),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: RegisterButton(
                      onPressed: isRegisterButtonEnabled(state)
                          ? _onFormSubmitted
                          : null,
                    ),
                  ),
                ]
              ),
            ),
          );
        },
      ),
    );
  }
  

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void _onEmailChanged() {
    _registerBloc.add(
      EmailChanged(email: _emailController.text),
    );
  }

  void _onPasswordChanged() {
    _registerBloc.add(
      PasswordChanged(password: _passwordController.text),
    );
  }
  void _onConfirmPasswordChanged() {
    _registerBloc.add(
      PasswordChanged(password: _confirmPasswordController.text),
    );
  }

  void _onFormSubmitted() {
    _registerBloc.add(
      Submitted(
        nick: _nickController.text,
        password: _passwordController.text,
        displayName: _nameController.text,
        birthDate: _birthDate,
        email: _emailController.text,
        gender: _gender
      ),
    );
  }
}
