import 'package:flutter/material.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';

class RegisterButton extends StatelessWidget {
  final VoidCallback _onPressed;

  RegisterButton({Key key, VoidCallback onPressed})
      : _onPressed = onPressed,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 55.0,
      child: Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0)),
        child: MaterialButton(
          child: Center(child: Text('kayıt ol', style: TextStyle(color: Colors.white, fontSize: 25.0))),
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          elevation: 0.0,
          color: CustomTheme.of(context).indicatorColor,
          disabledColor: Colors.grey,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
          onPressed: _onPressed,
        ),
      )
    );
  }
}
