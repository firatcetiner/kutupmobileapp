import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kutupsozluk_app/profile/user_repository.dart';
import 'package:kutupsozluk_app/register/register.dart';
import 'package:kutupsozluk_app/teddy/teddy_controller.dart';
import 'package:kutupsozluk_app/theme/custom_theme.dart';
import 'package:kutupsozluk_app/welcoming_screen/bloc/welcome_screen_bloc.dart';

class RegisterScreen extends StatelessWidget {
  final UserRepository _userRepository;
  final WelcomingScreenBloc _bloc;
  final TeddyController _teddyController;

  RegisterScreen({Key key, @required UserRepository userRepository, @required WelcomingScreenBloc bloc, @required TeddyController teddyController})
      : assert(userRepository != null),
        assert(bloc != null),
        assert(teddyController != null),
        _teddyController = teddyController,
        _bloc = bloc,
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(42),
          child: Container(
            color: CustomTheme.of(context).appBarTheme.color,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(EvaIcons.arrowIosBackOutline),
                      onPressed: () => Navigator.pop(context),
                    ),
                    Text('yeni kayıt oluştur'),
                  ],
                ),
                Divider(height: 0, thickness: 1)
              ],
            ),
          ),
        ),
        body: Stack(
          children: <Widget>[
            Center(
              child: BlocProvider<RegisterBloc>(
                create: (context) => RegisterBloc(userRepository: _userRepository),
                child: RegisterForm(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
